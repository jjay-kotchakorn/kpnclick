<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
global $db;
$news_id = $_GET["news_id"];
$newstype_id = $_GET["newstype"];
$newstype = datatype(" and a.active='T'", "newstype", true);
$newsstatus = datatype(" and a.active='T' and a.newstype_id=$newstype_id", "newsstatus", true);
$company_id = $_GET["company_id"];
$company = datatype(" and a.active='T'", "company", true);
$q = "select name, code from company where company_id=$company_id";
$company_data = $db->rows($q);
$select = $company_data["name"];
$q = "select name from newstype where newstype_id=$newstype_id";
$type_name = $db->data($q);
$q = "select  name, assign_to_id from news where news_id=$news_id";
$r = $db->rows($q);
$str = "";

if($r){
	$str = $r["name"];
	$assign_to_id = $r["assign_to_id"];
}else{
	$str = "Add New ".$type_name;
	$code = $company_data["code"];
}
?>
<link rel="stylesheet" type="text/css" href="js/bootstrap.summernote/dist/summernote.css" />
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
				  <div class="col-md-12">			      
					<div class="block-flat">
					  <div class="header">							
					  	<ol class="breadcrumb">
							<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">Home</a></li>
							<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>&type=info&company_id=<?php echo $company_id ?>');"><?php echo $select; ?></a></li>
							<li class="active"><?php echo $str; ?></li>
						</ol>
					  </div>
					  <div class="content">
						  <form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="update-news.php">
						  <input type="hidden" name="news_id" id="news_id" value="<?php echo $news_id; ?>">
						  <input type="hidden" name="newstype_id" id="newstype_id" value="<?php echo $newstype_id; ?>">
						  <input type="hidden" name="company_id" id="company_id" value="<?php echo $company_id; ?>">
						    <input type="hidden" name="tmpimg" />
  							<input type="hidden" name="delimg" />
						  <div class="col-sm-12">
							  <div class="form-group row">
								  	<label class="col-sm-1 control-label">Symbol</label>
								  	<div class="col-sm-2">
								  		<input class="form-control" id="code" name="code"  value="<?php echo $code; ?>" required="" placeholder="Code" type="text">
								  	</div>	
								  	<label class="col-sm-2 control-label"><?php echo $type_name; ?> Status<span class="red">*</span></label>
								  	<div class="col-sm-2">
								  		<select name="newsstatus_id" id="newsstatus_id" class="form-control required">
								  			<option value="">---- Select ----</option>
								  			<?php foreach ($newsstatus as $key => $value) {
								  				$id = $value['newsstatus_id'];
								  				$name = $value['name'];
								  				echo  "<option value='$id'>$name</option>";
								  			} ?>

								  		</select>
								  	</div>
								  	<?php if($newstype_id==2): ?>
								  	<label class="col-sm-2 control-label">Quotation No.</label>
								  	<div class="col-sm-3">
								  		<input class="form-control" id="quotation_no" name="quotation_no" required="" placeholder="Quotation No." type="text">
								  	</div>
								  	<?php endif; ?>		
								</div>				                
								<div class="form-group row">	                			                
								<label class="col-sm-1 control-label">Title</label>
								<div class="col-sm-7">
								  <input class="form-control" name="name" id="name" required="" placeholder="title" type="text">
								</div>	
								<label class="col-sm-1 control-label" >Assign To<span class="red">*</span></label>
								<div class="col-sm-3" id="load_box">
									<select name="assign_to_id" id="assign_to_id" class="select2">
										<option value="">---- เลือก ----</option>
										<?php 
										$q = "select * from emp where active='T'";
										$emp = $db->get($q);
										foreach ($emp as $key => $value) {
											$id = $value['emp_id'];
											$s = "";
											if($assign_to_id==$id){
												$s = "selected";
											}
											$name = $value['prefix'].$value["fname"]." ".$value["lname"];
											echo  "<option value='$id' {$s}>$name</option>";
										} ?>

									</select>
								</div> 
								</div>
								<div class="form-group row">		                			                

									<label class="col-sm-2 control-label">Target Date</label>
									<div class="col-sm-2">
										<input class="form-control" name="target_date" id="target_date" placeholder="Target Date" type="text">
									</div> 
	                			                
									<label class="col-sm-2 control-label">Created Date</label>
									<div class="col-sm-2">
										<input class="form-control" readonly="readonly" name="created_date" id="created_date" placeholder="Created Date" type="text">
									</div>		                			                
									<label class="col-sm-2 control-label">Last Update</label>
									<div class="col-sm-2">
										<input class="form-control" readonly="readonly" name="rectime" id="rectime" placeholder="Last Update" type="text">
									</div>
							  </div>				                
							  <div class="form-group row">
							  <label class="col-sm-1 control-label">Remark</label>
							  <div class="clear"></div><br>
							  	<div class="col-sm-12">
							  		<textarea id="detail" class="summernote" name="detail" placeholder="Description"></textarea>
							  	</div>
							  </div>
							  <div class="form-group row">
							  	<div class="col-sm-12">
							  	<label class="control-label">FILE UPLOAD</label>
							  	<div class="clear"></div><br>
									<div id="file_upload_list">
										
									</div>							  		
							  	</div>
							  </div>

						  </div>
						  <div class="clear"></div>
						  <div class="form-group row" style="padding-left:10px;">
								<div class="col-sm-12">
									<button type="button" class="btn btn-primary" onClick="ckSave()">Save changes</button>
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">Cancel</button>
									<button type="button" <?php echo ($news_id) ? '' : 'style="visibility:hidden;"'; ?> class="btn btn-primary pull-right" onClick="get_upload(<?php echo $news_id; ?>)">Upload</button>
								</div>
						  </div>
						</form>
						
					  </div>
					</div>
					
				  </div>
				</div>

		</div>
	</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>

<script type="text/javascript" src="js/bootstrap.summernote/dist/summernote.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
   $("#frmMain").validate();
   var news_id = "<?php echo $_GET["news_id"]?>";
   if(news_id) viewInfo(news_id);
 	var nowDate = new Date();
	var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate()-7, 0, 0, 0, 0);	
   $("#target_date").datepicker({language:'th-th',format:'dd-mm-yyyy',startDate: today });
   $('#detail').summernote({
            height: 200,
            onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            }
        }); 
        function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);//You can append as many data as you want. Check mozilla docs for this
            $.ajax({
                data: data,
                type: "POST",
                url: 'data/upload-img.php',
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                	var url = "newsImg/"+url;
                    editor.insertImage(welEditable, url);
                }
            });
        }

        
 });
 function viewInfo(news_id){
   if(typeof news_id=="undefined") return;
   getnewsInfo(news_id);
}

function getnewsInfo(id){
	if(typeof id=="undefined") return;
	var url = "data/newslist.php";
	var param = "news_id="+id+"&single=T";
	dataUrl(url, param,"#frmMain");
	reload_upload();
}
function get_upload(id){
	if(typeof id=="undefined") return;
	var url = "news-upload.php?news_id="+id;
	popUp(url, 700, 450);
}

function reView(){
	reload_upload();
}

function delete_file(id){
	if(typeof id=="undefined") return;
		var t = confirm("ลบไฟล์นี้ ? ");
		if(!t) return;
		var url = "update-upload.php";
		var param = "news_upload_id="+id;
		$.ajax( {
			"type": "POST",
			"async": false, 
			"url": url,
			"data": param, 
			"success": function(data){
			   reload_upload();
		 	}
		});
}
function reload_upload(){
		var url = "data/uploadlist.php";
		var param = "news_id=<?php echo $news_id; ?>";
		$.ajax( {
			"type": "POST",
			"async": false, 
			"url": url,
			"data": param, 
			"success": function(data){
			   $("#file_upload_list").html(data);
		 	}
		});
}

function ckSave(id){
  onCkForm("#frmMain");
 var tmp = $('#detail').code();
 $('#detail').val(tmp);
  $("#frmMain").submit();
}	
</script>