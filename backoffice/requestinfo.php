<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/request.php";
include_once "./share/member.php";
global $db, $RIGHTTYPEID, $MEMBERID;

$error = $_SESSION["error"]["msg"];
unset($_SESSION["error"]["msg"]);
$success = $_SESSION["success"]["msg"];
unset($_SESSION["success"]["msg"]);

$request_id = $_GET["request_id"];

$requesttype = datatype(" and a.active='T'", "requesttype", true);
$q = "select  title, room_id, requeststatus_id, requeststate_id from request where request_id=$request_id";
$r = $db->rows($q);
$str = "";
$select = "";
$requeststatus_id = "";
$requeststate_id = "";
$approveT = "";
$approveF = "";
$show_return = "visibility:hidden";
$con_room = "";
if($r){
	$str = $r["title"];
	$select = $r["room_id"];
	$requeststatus_id = $r["requeststatus_id"];
	$requeststate_id = $r["requeststate_id"];
	$con_rom = " and a.active='T'";
}else{
	$str = "เพิ่มการขอใช้บริการ";
	$con_rom = " and a.active='T'";
}
$room = datatype($con_rom, "room", true);
if($requeststatus_id==1){
	$approveT = "อนุมัติการยืม";
	$approveF = "-";
}
if($requeststatus_id==2){
	$show_return = "";
	$approveT = "อนุมัติการคืน";
	$approveF = "ยกเลิกอนุมัติการยืม";
}
if($requeststatus_id==3){
	$show_return = "";
	$approveT = "-";
	$approveF = "ยกเลิกอนุมัติการคืน";
}
if($MEMBERID>0){
	$info_member = view_member("", $MEMBERID);
	if($info_member) $info_member = $info_member[0];
	$membername = $info_member["prefix"].$info_member["fname"]." ".$info_member["lname"];
}

?>
<link rel="stylesheet" type="text/css" href="js/bootstrap.summernote/dist/summernote.css" />
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
					<div class="col-md-12">           
					<div class="block-flat">
						<div class="header">              
						<ol class="breadcrumb">
							<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
							<li class="active"><?php echo $str; ?></li>
							<li class="pull-right"><?php if($request_id){ ?><a href="#" onClick="print_report('<?php echo $request_id ?>');">พิมพ์เอกสาร</a> <?php } ?></li>
						</ol>
						</div>
						<div class="content">
							<form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="update-request.php">
							<input type="hidden" name="request_id" id="request_id" value="<?php echo $request_id; ?>">
							    <input type="hidden"  id="del" name="del" value="F">
    							<input type="hidden"  id="approve" name="approve">
    							<input type="hidden"  id="requeststate_id" name="requeststate_id">
							<div class="col-sm-12">
								<div class="form-group row">
									<label class="col-sm-1 control-label" style="width:115px;">เลขที่</label>
									<div class="col-sm-2" style="width:150px;">
										<input class="form-control" id="docno" name="docno" readonly="true" placeholder="เลขที่เอกสาร" type="text">
									</div>
									<label class="col-sm-1 control-label">วันที่</label>
									<div class="col-sm-2">
										<input class="form-control" name="docdate" id="docdate" readonly="true"  placeholder="วันที่เอกสาร" type="text">
									</div> 
									<label class="col-sm-2 control-label">ชื่อ-นามสกุล <span class="red">*</span></label>
									<div class="col-sm-4">
										<input type="text" class="form-control" value="<?php echo $info_member["code"] ?>" id="membercode" name="membercode" readonly="true" style="width:80px; display:inline-block;">
										&nbsp;
										<input type="button" style="width:32px; <?php echo ($RIGHTTYPEID==1 || $RIGHTTYPEID==2) ? '' : 'display:none;'; ?>" class="btn" value=".." onClick="selectMember();">
										&nbsp;
										<input type="text" class="form-control" id="membername" name="membername" readonly="true" value="<?php echo $membername; ?>" style="width:236px; display:inline-block;">
										<input name="member_id" type="hidden" id="member_id" value="<?php echo $MEMBERID; ?>" />
									</div>                
								</div> 
								<div class="form-group row">
									<label class="col-sm-2 control-label" >ประเภทสมาชิก<span class="red">*</span></label>
									<div class="col-sm-2" >
										<input class="form-control" value="<?php echo $info_member["membertype_name"] ?>" readonly="true" id="membertype_name" name="membertype_name" required="" placeholder="ประเภทสมาชิก" type="text">
									</div>
  									<label class="col-sm-2 control-label" >ภาควิชา / หน่วยงาน<span class="red">*</span></label>
									<div class="col-sm-3">
										<input class="form-control" value="<?php echo $info_member["department_name"] ?>" readonly="true" id="department_name" name="department_name" required="" placeholder="ภาควิชา / หน่วยงาน" type="text">
									</div>  
  									<label class="col-sm-1 control-label" >เบอร์โทร<span class="red">*</span></label>
									<div class="col-sm-2">
										<input class="form-control" id="dept_phone" name="dept_phone" required="" placeholder="เบอร์โทร ภาควิชา / หน่วยงาน" type="text">
									</div>  
								</div>                        
								<div class="form-group row"> 
                                     
									<label class="col-sm-3 control-label" >จุดประสงค์เพื่อจัด (ชื่อกิจกรรม / โครงการ) <span class="red" style="display:inline-block; position:absolute;"> &nbsp;*</span></label>
									<div class="col-sm-9">
										<input class="form-control" name="title" id="title" required="" placeholder="จุดประสงค์เพื่อจัด (ชื่อกิจกรรม / โครงการ)" type="text">
									</div> 
								</div>                        
								<div class="form-group row">
									<h4><i class="fa  fa-clock-o"></i> &nbsp;มีความประสงค์ขอใช้บริการ</h4> 
								</div>
								<div class="form-group row">
									<label class="col-sm-2 control-label" style="text-align:left;"><input type="checkbox" name="request_item" id="request_item" value="F">&nbsp;ยืมอุปกรณ์โสตฯ </label>
									<label class="col-sm-2 control-label" ><input type="checkbox" name="request_camera" id="request_camera" value="F">&nbsp;ถ่ายภาพนิ่งดิจิตอล </label>
									<label class="col-sm-2 control-label" ><input type="checkbox" name="request_video" id="request_video" value="F">&nbsp;ถ่ายทำวีดิทัศน์</label>
									<label class="col-sm-2 control-label" ><input type="checkbox" name="request_audio" id="request_audio" value="F">&nbsp;บันทึกเสียง</label>
									<label class="col-sm-2 control-label" ><input type="checkbox" name="request_editing_video" id="request_editing_video" value="F">&nbsp;ตัดต่อวีดีโอ </label>									
								</div>
								<div class="form-group row">                                        
                                  
	                                       
									<label class="col-sm-1 control-label">วันที่ <span class="red">*</span></label>
									<div class="col-sm-3">
										<div class="" style="display:inline-block;width:150px">
											<input class="form-control required" name="date_start" id="date_start"  placeholder="วันที่" type="text">
										</div>
										<div class="" style="display:inline-block;width:100px">
											<input class="form-control required time" name="date_start_time" id="date_start_time"  placeholder="" type="text">
										</div>
									</div>                                          
									<label class="col-sm-1 control-label">ถึงวันที่</label>
									<div class="col-sm-3">
										<div class="" style="display:inline-block;width:150px">
											<input class="form-control required" name="date_stop" id="date_stop"  placeholder="วันที่" type="text">
										</div>
										<div class="" style="display:inline-block;width:100px">
											<input class="form-control required time" name="date_stop_time" id="date_stop_time"  placeholder="" type="text">
										</div>
									</div>                                           
									<label class="col-sm-1 control-label" style="<?php echo $show_return; ?> ">วันที่คืน</label>
									<div class="col-sm-3" style="<?php echo $show_return; ?>">
										<div class="" style="display:inline-block;width:150px">
											<input class="form-control" name="date_return" id="date_return"  placeholder="วันที่" type="text">
										</div>
										<div class="" style="display:inline-block;width:100px">
											<input class="form-control time" name="date_return_time" id="date_return_time"  placeholder="" type="text">
										</div>
									</div>  									
								</div>
								<div class="form-group row">
								
									<label class="col-sm-1 control-label" >สถานที่<span class="red">*</span></label>
									<div class="col-sm-3" id="load_box">
										<select name="room_id" id="room_id" class="select2" onchange="addrequest_detail();">
											<option value="">---- เลือก ----</option>
											<?php foreach ($room as $key => $value) {
												$id = $value['room_id'];
												$s = "";
												if($select==$id){
													$s = "selected";
												}
												$name = $value['name'];
												echo  "<option value='$id' {$s}>$name</option>";
											} ?>

										</select>
									</div>  

								</div>

								<div class="form-group row" >
									
									<table class="table table-striped" id="tbList">
										<thead>
											<tr class="alert alert-success" style="font-weight:bold;">
												<td width="10%"><input   type="checkbox" onClick="ckListAll(this)"  >&nbsp;ลำดับ</td>
												<td width="20%" class="center">รหัส</td>
												<td width="30%" class="center">รายการ</td>
												<td width="10%" class="center">จำนวน</td>
												<td width="30%" class="center">ยี่ห้อ/รุ่น</td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><input name="ckRequestDetail[]" type="checkbox" checked id="ckRequestDetail" value="T">
													<span id="runNo"></span>
													<input name="item_id[]" type="hidden" id="item_id">
													<input name="request_detail_id[]" type="hidden" id="request_detail_id">
												</td>												
												<td  class="center" id="code"></td>
												<td  class="center" id="name"></td>
												<td  class="center"><input class="form-control" id="amount" name="amount[]"  placeholder="จำนวน" type="text" value="1"></td>
												<td  class="center" id="itemtype_name"></td>													
											</tr>
										</tbody>
									</table>
								</div> 
								<div class="form-group row">
								  <h5><i class="fa  fa-clock-o"></i> &nbsp;รายละเอียดเพิ่มเติมอื่นๆ ถ้ามี(โปรดระบุ)</h5>
								  <div class="clear"></div>
								  	<div class="col-sm-12 row">
								  		<textarea id="detail" class="summernote" name="detail" placeholder="รายละเอียดเพิ่มเติม"></textarea>
								  	</div>
								    </div>                       

								<div class="form-group row"> 
									<label class="col-sm-2 control-label">เก็บค่าบริการจาก <span class="red">*</span></label>
									<div class="col-sm-7">
										<label class="col-sm-1 control-label"><input type="radio" name="servicecharge_id" value="1" checked></label>
										<div class="col-sm-11">งานส่วนกลางของคณะฯ</div>	<div class="clear"></div>									
										<label class="col-sm-1 control-label"><input type="radio" name="servicecharge_id" value="2" ></label>
										<div class="col-sm-11">จ่ายเงินสด (โดยเจ้าของนำเงินเข้าเลขที่บัญชีเลขที่ 016-3-00325-6 ของคณะวิทยาศาสตร์ ม.มหิดล)</div>
										<div class="clear"></div>										
										<label class="col-sm-1 control-label"><input type="radio" name="servicecharge_id" value="3" ></label>
										<div class="col-sm-11 "><div style="display:inline-block; float:left; margin-right:15px;">งานภาควิชา / หน่วยงาน</div> <input class="form-control " name="servicecharge_detail" id="servicecharge_detail"  placeholder="ระบุ" type="text" style="width:200px;"></div>
									</div>
									<label class="col-sm-1 control-label">สถานะ </label>
									<div class="col-sm-2">
										<input type="text" class="form-control" id="requeststatus_name" name="requeststatus_name" readonly="true">
									</div>                                         
								</div>	
								</div>
							</div>
							<div class="clear"></div>
							<div class="form-group row" style="padding-left:10px;">
								<div class="col-sm-12">
									<button type="button" id="btSave" class="btn btn-primary" onClick="ckSave()">บันทึก</button>
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">กลับสู่หน้าหลัก</button>
									<span style="float:right;<?php echo ($RIGHTTYPEID==1 || $RIGHTTYPEID==2) ? "" : "visibility:hidden"; ?>">
									&nbsp;<button id="btAppT" style="margin-right:5px;" type="button" class="btn btn-success" onClick="ckApp('T');"><?php echo $approveT; ?></button>
									&nbsp;<button id="btAppF" style="margin-right:5px;" type="button" class="btn btn-warning" onClick="ckApp('F');"><?php echo $approveF; ?></button>
									&nbsp;<button id="btDel" style="margin-right:5px;" type="button" class="btn btn-danger" onClick="ckDel();">ยกเลิกรายการ</button>
									</span>
								</div>
							</div>
						</form>
						</div>
					</div>
					
					</div>
				</div>

		</div>
	</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>
<script type="text/javascript" src="js/bootstrap.summernote/dist/summernote.min.js"></script>
<script type="text/javascript">
	var trMenuList = $("#tbList tbody tr:eq(0)").clone();
	delRow("#tbList");
	$(document).ready(function() {
	var request_id = "<?php echo $_GET["request_id"]?>";
	var requeststatus_id = "<?php echo $requeststatus_id; ?>";

	if(request_id>0) ckStatus(requeststatus_id);
	if (!requeststatus_id)
	$("#btDel").hide();
	if (!requeststatus_id)
	$("#btAppT").hide();
	if (!requeststatus_id)
	$("#btAppF").hide();
	var nowDate = new Date();
	var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);		
	 $("#frmMain").validate();
	  $("#date_start_time").mask("99:99");
	  $("#date_stop_time").mask("99:99");
	  $("#date_return_time").mask("99:99");

	 if(request_id) viewInfo(request_id);
	 $('#detail').summernote({height: 80});
	 $("#date_start").datepicker({language:'th-th',format:'dd-mm-yyyy',startDate: today });
	 $("#date_return").datepicker({language:'th-th',format:'dd-mm-yyyy',startDate: today });
	 $("#date_stop").datepicker({
	 	language:'th-th',
	 	format:'dd-mm-yyyy',
	 	startDate: today 
	 });
     var error = "<?php echo $error ?>";
     if(error!=""){
		$.gritter.removeAll({
	        after_close: function(){
	          $.gritter.add({
	          	position: 'center',
		        title: 'Error',
		        text: error,
		        class_name: 'danger'
		      });
	        }
	     });
     }
     var success = "<?php echo $success ?>";
     if(success!=""){
		$.gritter.removeAll({
	        after_close: function(){
	          $.gritter.add({
	          	position: 'center',
		        title: 'success',
		        text: success,
		        class_name: 'success'
		      });
	        }
	     });
     }

     $("#date_start, #date_stop").blur(function(event) {
     	var from=$("#date_start").datepicker('getDate');
		var to =$("#date_stop").datepicker('getDate');
		var ckto = strTrim($("#date_stop").val());
		var ckf = strTrim($("#date_start").val());
		var msg = "";
		if(ckto=="" || ckf=="") return false;		
		if(to<from){
			msgError("วันเวลาไม่ถูกต้อง");
			$("#date_stop").val("");
		}
     });

     $("#date_start, #date_stop").blur(function(event) {
     	var from=$("#date_start").datepicker('getDate');
		var to =$("#date_stop").datepicker('getDate');
		var ckto = strTrim($("#date_stop").val());
		var ckf = strTrim($("#date_start").val());
		var msg = "";
		if(ckf=="") return false;		
		if(from!=""){
			ckRoom();
		}
     });
	$("#request_item").click(function(event) {
		if($(this).is(":checked")){

		}else{
			delRow("#tbList");
		}
		/* Act on the event */
	});


 });
function ckRoom(){
	var room_id = $("#room_id").val();
    var url = "data/checkroom.php";
	var param = "date_start="+$("#date_start").val();
	param += "&room_id="+room_id;
	$.ajax( {
		/*"dataType":'json', */
		"type": "POST", 
		"url": url,
		"data": param, 
		"success": function(data){	
			$("#load_box").html(data);
			$(".select2").select2({
				width: '100%'
			});				 
		}
	});
}

function viewInfo(request_id){
	 if(typeof request_id=="undefined") return;
	 get_request_info(request_id);
	 addrequest_detail(request_id);
}

function get_request_info(id){
	if(typeof id=="undefined") return;
	var url = "data/requestlist.php";
	var param = "request_id="+id+"&single=T";
	dataUrl(url, param,"#frmMain");
	
}


function selectMember(){
   var url = "member map";
   requestDetailData(url,"ret_member_select");	
}

function ret_member_select(id){
	for(var i in id){
		var ck = "";
		var data = id[i];
        $("#member_id").val(data["member_id"]);
        $("#membercode").val(data["code"]);
        $("#membername").val(data["prefix"]+' '+data["fname"]+' '+data["lname"]);
        $("#membertype_name").val(data["membertype_name"]);
        $("#department_name").val(data["department_name"]);
	}
}
function addrequest_detail(id){
	var request_item = $("#request_item").is(":checked");
	if(!request_item) return;
	if(typeof id=="undefined") var id = "default";
	delRow("#tbList");
	var room_id = $("#room_id").val();
    var url = "data/request-item-list.php";
	var param = "request_id="+id;
	param += "&room_id="+room_id;
	$.ajax( {
		"dataType":'json', 
		"type": "POST", 
		"url": url,
		"data": param, 
		"success": function(data){	
			 $.each(data, function(index, array){
			 	if(!array.amount)
			 		array.amount = 1;
             	addTrLine("#tbList", trMenuList, array, "runNo");    
			 });				 
		}
	});
}

function ckSave(id){
	onCkForm("#frmMain");
	var tmp = $('#detail').code();
	$('#detail').val(tmp);
	$("#frmMain").submit();
} 

function ckDel() {
    var t = confirm("ยืนยันการยกเลิกการยืม");
    if (t) {
        $("#del").val("T");
        $("#frmMain").submit();
    }
}

function ckApp(type) {
    if (typeof type == "undefined")
        return;
    if (type == 'T') {
        var msg = "ยืนยันการอนุมัติรายการ";
    } else {
        var msg = "ยืนยันการยกเลิกรายการ";
    }
    var t = confirm(msg);
    if (t) {
        if (type == "T")
            $("#approve").val("T");
        else
            $("#approve").val("F");
        $("#frmMain").submit();
    }
}
function ckStatus(requestStatusId) {
    if (typeof requestStatusId == "undefined")
        return;
    if (requestStatusId == 1) {
        $("#btDel").show();
        $("#btAppT").show();
        $("#btAppF").hide();
        $("#btSave").show();
    } else if (requestStatusId == 2) {
        $("#btDel").hide();
        $("#btAppT").show();
        $("#btAppF").show();
        $("#btSave").hide();
    }else if (requestStatusId == 3) {
        $("#btDel").hide();
        $("#btAppT").hide();
        $("#btAppF").hide();
        $("#btSave").hide();
    } else {
        $("#btDel").hide();
        $("#btAppT").hide();
        $("#btAppF").hide();
        $("#btSave").hide();
    }
}
function ckListAll(tag){

	$("#tbList tbody tr :checkbox").each(function() {
		if ($(tag).is(":checked")) {
			$(this).prop('checked',true);
		} else {
			$(this).prop('checked', false);
		}
	});
}

function print_report(id){
	if(typeof id=="undefined") return;
   var url = "request-report.php?request_id="+id;
   popUp(url, 820, 500);
}

</script>