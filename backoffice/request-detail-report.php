<?php 
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/menu.php";
?>
<!DOCTYPE html>
<html lang="en">
<?php include ('inc/header.php'); ?>
<body>
	<?php 
	include_once "./share/request.php";
	include_once "./share/item.php";
	global $db;
	$request_id = $_GET["request_id"];
	?>
	<table class="table table-striped" id="tbList" style="width:850px">
		<thead>
			<tr class="alert alert-success" style="font-weight:bold;">
				<td width="10%">ลำดับ</td>
				<td width="20%" class="center">รหัส</td>
				<td width="30%" class="center">รายการ</td>
				<td width="10%" class="center">จำนวน</td>
				<td width="30%" class="center">ยี่ห้อ/รุ่น</td>
			</tr>
		</thead>
		<tbody>
			<?php 
			$con = " and a.request_id={$request_id}";
			$r = get_request_detail($con);
			$i = 1;
			foreach($r as $k=>$v){?>
				<tr>
					<td><?php echo $i; ?></td>												
					<td  class="center"><?php echo $v["code"]; ?></td>
					<td  class="center" id="name"><?php echo $v["name"]; ?></td>
					<td  class="center"><?php echo $v["amount"]; ?></td>
					<td  class="center" id="itemtype_name"><?php echo $v["itemtype_name"]; ?></td>													
				</tr>
				<?php 
				$i++;
			} 
			?>
		</tbody>
	</table>
	<?php 		  	
	include_once ('inc/js-script.php');
	include ('inc/footer.php') ?>
</body>
<script>
	$(document).ready(function() {
		var oTable;
		listItem();
	});	
	function listItem(){
		oTable = $("#tbList").dataTable({
			"sDom": 'T<"clear">lfrtip',
			"bFilter": false,
			"oLanguage": {
				"sInfoEmpty": "",
				"sInfoFiltered": "",
				"sSearch": "Search:"
			},
			"oTableTools": {
				"aButtons":  [	
				{
					"sExtends": "xls",
					"sButtonText": "Save for Excel"
				}
				]
			}
		}); 
	}
</script>
</html>
<?php $db->disconnect(); ?>