<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;
$company = datatype(" and a.active='T'", "company", true);
$type = $_GET["type"];
$str =  "Contact Person";
?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
					<div class="page-head">
						<button id="add" class="btn btn-success btn-small pull-right" onclick="addnew()" style="margin-top:10px;"><i class="fa fa-plus"></i> Add New Person</button>
						<h3><i class="fa fa-list"></i> &nbsp; <?php echo $str; ?></h3>
					</div>
						<div class="header">
							<div class="form-group row">             
								<label class="col-sm-1 control-label">Company<span class="red">*</span></label>
								<div class="col-sm-2">
									<select name="company_id" id="company_id" class="select2" onchange="reCall();">
										<option value="">---- เลือก ----</option>
										<?php foreach ($company as $key => $value) {
											$id = $value['company_id'];
											$name = $value['name'];
											echo  "<option value='$id'>$name</option>";
										} ?>

									</select>
								</div>
								<label class="col-sm-1 control-label">Status</label>
								<div class="col-sm-2">
									<select name="active" id="active" class="form-control" onchange="reCall();">
										<option selected="selected" value="T">active</option>
										<option value="F">inactive</option>
									</select>
								</div>                                           
							</div> 
						</div>
					<table id="tbperson" class="table" style="width:100%">
						  <thead>
							  <tr>
								  <th width="8%">No</th>
								  <th width="15%">Code</th>
								  <th width="26%">Person Name</th>
								  <th width="17%">Company</th>
								  <th width="13%">Phone/Mobile</th>
								  <th width="10%">Remark</th>
								  <th width="10%">Manage</th>
							  </tr>
						  </thead>   
						<tbody>
						</tbody>
					</table>
					<div class="clear"></div>
				</div>
			</div>

		</div>
	</div> 
</div>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
$(document).ready(function() {
	var get_type = "<?php echo $_GET["type"]; ?>";
	if(get_type=="childlist" || get_type=="person_order") $("#add").hide();
	var oTable;
	listperson();	
});

function listperson(){
   var get_type = "<?php echo $_GET["type"]; ?>";
   var url = "data/personlist.php";
   oTable = $("#tbperson").dataTable({
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
   	   "sInfoEmpty": "",
   		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons":  ""
		},
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({"name":"section_id","value":$("#section_id").val()});			
			aoData.push({"name":"company_id","value":$("#company_id").val()});			
			aoData.push({"name":"active","value":$("#active").val()});			
			aoData.push({"name":"type","value":get_type});
			if(get_type!="childlist"){
				aoData.push({"name":"childlist","value":$("#childlist").val()});	
			}			
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
		}
   }); 
}

function editInfo(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&person_id="+id+"&type=info";
   redirect(url);
}


function childlist(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&person_id="+id+"&type=childdetail";
   redirect(url);
}

function addnew(){
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=info";
   redirect(url);
}

function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}

function update_order(id){
  var mixed_var = $("input[name=menu_order_"+id+"]").val();
  var tmp = $("input[name=menu_order_"+id+"]").attr("data");
  var whitespace =
    " \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000";
  var ck = (typeof mixed_var === 'number' || (typeof mixed_var === 'string' && whitespace.indexOf(mixed_var.slice(-1)) === -
    1)) && mixed_var !== '' && !isNaN(mixed_var);
  if(ck){
	$.ajax({
		"type": "POST",
		"async": false, 
		"url": "data/person-order-update.php",
		"data": {'person_id': id, 'data_value': mixed_var}, 
		"success": function(data){	
			$.gritter.removeAll({
		        after_close: function(){
		          $.gritter.add({
		          	position: 'center',
			        title: 'Success',
			        text: data,
			        class_name: 'success'
			      });
		        }
		      });
 			reCall();				      							 
		}
	});
  }else{
    $("input[name=menu_order_"+id+"]").val(tmp);
  }

}

</script>