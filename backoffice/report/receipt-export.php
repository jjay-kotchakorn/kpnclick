<?php
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/register.php";
require_once "./Classes/PHPExcel.php";
global $db;
// d($db);

// d($_GET); 
// die();

function update_receipt_status_log($payment_bank_register_list_id="", $emp_id=""){
	if (empty($payment_bank_register_list_id)) return;
	global $db;

	$payment_bank_register_list = get_payment_bank_register_list("", $payment_bank_register_list_id);
	$payment_bank_register_list = $payment_bank_register_list[0];
	$payment_bank_register_list_recby_id = $payment_bank_register_list["recby_id"];
	$register_id = $payment_bank_register_list["register_id"];

	$register_info = get_register("", $register_id);
	$register_info = $register_info[0];
	$register_recby_id = $register_info["recby_id"];
	$receipt_status_id_old = $register_info["receipt_status_id"];

	$args = array();
	$args["table"] = "receipt_status_log";
	$args["payment_bank_register_list_id"] = $payment_bank_register_list_id;
	$args["register_id"] = $register_id;
	$args["receipt_status_id"] = $receipt_status_id_old;
	$args["register_recby_id"] = (int)$register_recby_id;
	$args["payment_bank_register_list_recby_id"] = (int)$payment_bank_register_list_recby_id;
	$args["serialize_payment_bank_register_list"] = serialize($payment_bank_register_list);
	$args["serialize_register"] = serialize($register_info);
	$args["recby_id"] = (int)$emp_id;

	// var_dump($db->set($args, true, true)); die();
	$receipt_status_log_id = $db->set($args);

	if ( !empty($receipt_status_log_id) ) {
		$args = array();
		$args["table"] = "register";
		$args["id"] = $register_id;		
		$args["receipt_status_id"] = 3;
		$args["recby_id"] = (int)$emp_id;

		// var_dump($db->set($args, true, true)); die();
		$db->set($args);

		unset($payment_bank_register_list);
		unset($register_info);
		unset($args);

	}//end if
}//end func


$bank = $_GET["bank"];
$status = $_GET["status"];
$bill_request = $_GET["bill_request"];
$project = $_GET["project"];
$chk_receipt_update = $_GET["chk_receipt_update"];
$emp_id = (int)$_GET["emp_id"];

$date_start = trim($_GET["date_start"]);
$date_stop = trim($_GET["date_stop"]);
$date_start_result = trim($_GET["date_start_result"]);
$date_stop_result = trim($_GET["date_stop_result"]);

$date_start = thai_to_timestamp($date_start);
$date_stop = thai_to_timestamp($date_stop);
$date_start_result = (!empty($date_start_result)) ? thai_to_timestamp($date_start_result) : "";
$date_stop_result = (!empty($date_stop_result)) ? thai_to_timestamp($date_stop_result) : "";

$disp_date = $date_start." ถึง ".$date_stop;

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
// d($objPHPExcel);
$objPHPExcel->getProperties()->setCreator("Nattapon Booncharoen")
							 ->setLastModifiedBy("Nattapon Booncharoen")
							 ->setTitle("receipt-export")
							 ->setSubject("receipt-export")
							 ->setDescription("-")
							 ->setKeywords("receipt-export")
							 ->setCategory("export to excel file");	

// Add some data column						 
$objPHPExcel->getActiveSheet()
			->setCellValue('A1', "รายการบริจาค ช่วงวันที่ {$disp_date}")

            ->setCellValue('A2', 'วันที่ทำรายการ')
            ->setCellValue('B2', 'วันที่หมดเขต')
            ->setCellValue('C2', 'คำนำหน้าชื่อ')
			->setCellValue('D2', 'ชื่อ')
			->setCellValue('E2', 'นามสกุล')

            ->setCellValue('F2', 'โครงการ')
            ->setCellValue('G2', 'จำนวนเงิน')
            ->setCellValue('H2', 'วันที่ชำระ')
            ->setCellValue('I2', 'ช่องทางบริจาค')
            ->setCellValue('J2', 'สถานะบริจาค')

            ->setCellValue('K2', 'หมายเลขโทรศัพท์')
            ->setCellValue('L2', 'ใบเสร็จ')
            ->setCellValue('M2', 'รูปแบบใบเสร็จ')
            ->setCellValue('N2', 'เลขประจำตัวผู้เสียภาษี')
			->setCellValue('O2', 'ที่อยู่')

            ->setCellValue('P2', 'ตำบล/แขวง')
            ->setCellValue('Q2', 'อำเภอ')
            ->setCellValue('R2', 'จังหวัด')
            ->setCellValue('S2', 'รหัสไปรษณีย์');

// set width column
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);

$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);

$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);

$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(15);



// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('sheet1');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->getActiveSheet();
// d($objPHPExcel);

$cond = "";
// $cond .= " AND a.active='T'"; 
if ( !empty($bank) ) {
	$cond .= " AND a.payment_bank_id={$bank}"; 
}
if ( !empty($status) ) {
	$cond .= " AND a.pay_status={$status}"; 
}
if ( !empty($bill_request) ) {
	$cond .= " AND b.bill_request='{$bill_request}'"; 
}
if ( !empty($project) ) {
	$cond .= " AND b.project_id={$project}"; 
}
if ( !empty($date_start) && !empty($date_stop) ) {
	$cond .= " AND a.donation_date BETWEEN '{$date_start} 00:00:00' AND '{$date_stop} 23:59:59'"; 
}
if ( !empty($date_start_result) || !empty($date_stop_result) ) {
	if ( !empty($date_start_result) && !empty($date_stop_result) ) {
		$cond .= " AND a.resp_pay_date BETWEEN '{$date_start_result} 00:00:00' AND '{$date_stop_result} 23:59:59'"; 
	}else if ( !empty($date_start_result) && empty($date_stop_result) ) {
		$cond .= " AND a.resp_pay_date BETWEEN '{$date_start_result} 00:00:00' AND '{$date_start_result} 23:59:59'";
	}else if ( empty($date_start_result) && !empty($date_stop_result) ) {
		$cond .= " AND a.resp_pay_date BETWEEN '{$date_stop_result} 00:00:00' AND '{$date_stop_result} 23:59:59'";
	}//end else if
}//end if


$rs = get_payment_bank_register($cond);
// var_dump($rs);
// d($rs); die();

$row = 2;
$runno = 0;
$ttl_price = 0;
foreach ($rs as $key => $v) {
	$row++;
	$address = "";
	$payment_bank_register_list_id = $v["payment_bank_register_list_id"];
	$bill_request = $v["bill_request"];
	$pay_status = $v["pay_status"];
	$donation_date = explode(" ", $v["donation_date"]);
	$donation_date = $donation_date[0];
	$order_expire_date = explode(" ", $v["order_expire_date"]);
	$order_expire_date = $order_expire_date[0];
	$resp_pay_date = $v["resp_pay_date"];

	$bill_request = ($v["bill_request"] == 'T') ? "ต้องการ" : "ไม่ต้องการ";

	if ( $v["slip_type"] == 'individual' ) {
		$bill_request_type = "ออกในนามบุคคลธรรมดา";
		$address = $v["receipt_no"];
	}else if ( $v["slip_type"] == 'corporation' ) {
		$bill_request_type = "ออกในนามนิติบุคคล";
		$address = $v["receipttype_text"].", เลขที่ ".$v["receipt_no"];
	}//end else if

	$ttl_price = $ttl_price+$v["amount"];

	if ( $chk_receipt_update=='T' && $bill_request=='T' && $pay_status==3 ) {
	// if ( $chk_receipt_update=='T' && $v["bill_request"]=='T' ) {
		// echo "string"; die();
		update_receipt_status_log($payment_bank_register_list_id, $emp_id);
	}//end if

	$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $donation_date);
	$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $order_expire_date);
	$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $v["receipt_title"]);
	$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $v["receipt_fname"]);
	$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $v["receipt_lname"]);


	$objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $v["project_name_th"]);
	$objPHPExcel->getActiveSheet()->setCellValue('G' . $row, number_format($v["amount"], 2, '.', ','));		
	$objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $resp_pay_date);
	$objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $v["payment_bank_name"]);
	$objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $v["pay_status_name"]);


	$objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $v["telephone"]);
	$objPHPExcel->getActiveSheet()->setCellValue('L' . $row, $bill_request);		
	$objPHPExcel->getActiveSheet()->setCellValue('M' . $row, $bill_request_type);
	$objPHPExcel->getActiveSheet()->setCellValue('N' . $row, $v["taxno"]);
	$objPHPExcel->getActiveSheet()->setCellValue('O' . $row, $address);

	$objPHPExcel->getActiveSheet()->setCellValue('P' . $row, $v["district_name"]);
	$objPHPExcel->getActiveSheet()->setCellValue('Q' . $row, $v["amphur_name"]);		
	$objPHPExcel->getActiveSheet()->setCellValue('R' . $row, $v["province_name"]);
	$objPHPExcel->getActiveSheet()->setCellValue('S' . $row, $v["receipt_postcode"]);


	// d($objPHPExcel); die();
	
}//end loop $v
$row++;

$objPHPExcel->getActiveSheet()->setCellValue('F' . $row, 'รวม');
$objPHPExcel->getActiveSheet()->setCellValue('G' . $row, number_format($ttl_price, 2 ,".", ","));

/*
$objPHPExcel->getActiveSheet()
    ->getStyle("N2:N{$row}")
    ->getNumberFormat()
    ->setFormatCode(
        PHPExcel_Style_NumberFormat::FORMAT_TEXT
);
*/

//set FORMAT_NUMBER_COMMA_SEPARATED1
$objPHPExcel->getActiveSheet()
    ->getStyle("G2:G{$row}")
    ->getNumberFormat()
    ->setFormatCode(
        PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
);

//set HORIZONTAL_RIGHT
$objPHPExcel->getActiveSheet()
    ->getStyle("G2:G{$row}")
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
);

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="receipt-export.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

die();

?>