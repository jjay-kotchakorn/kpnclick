  <!-- Fixed navbar -->
<?php
include_once "./lib/lib.php";
include_once "./share/menu.php";
include_once "./share/datatype.php";

$default = get_config("defaultRightTypeId");
if (!$RIGHTTYPEID) {
  $q = "select a.righttype_id, a.code, a.name, a.name_eng, a.active,
			   a.recby_id, a.rectime
	  from rightType a 
	  where a.active='T' and righttype_id=$default
	  order by a.code, a.righttype_id";
  $rs = $db->rows($q);
  $RIGHTTYPEID = $_SESSION["login"]["info"]["righttyep_id"] = $rs["righttyep_id"];
  $RIGTHTYPENAME = $_SESSION["login"]["info"]["righttyep_name"] = $rs["name"];
}
?>
  <div id="head-nav" class="navbar navbar-default navbar-fixed-top">
	<div class="container-fluidx">
	  <div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		  <span class="fa fa-gear"></span>
		</button>
		<a class="navbar-brandx" href="#"></a>
	  </div>
	  <div class="navbar-collapse collapse">
		<ul class="nav navbar-nav">
		<?php
		$curent = $_GET["p"];
		$menu_access = array();
		$array_menus = get_menu();
		foreach ($array_menus as $index => $value) {
			$menu_id = $value["menu_id"];
			if ($RIGHTTYPEID && $menu_id && $ISADMIN != "T") {
				$ck = check_righttype($RIGHTTYPEID, $menu_id);
				if ($ck == false)
					continue;
			}
			$menu_access[$value["action"]] = $value["url"];
			$array_menulist = get_menuList($menu_id);			
			if (is_array($array_menulist) && count($array_menulist)>0) {
				?>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $value["name"];  ?><b class="caret"></b></a>
							<ul class="dropdown-menu">
								<?php
								foreach ($array_menulist as $i => $v) {
									if ($RIGHTTYPEID && $v["menulist_id"] && $ISADMIN != "T") {
										$ck_list = check_righttype($RIGHTTYPEID, "", $v["menulist_id"]);
										if ($ck_list == false)
											continue;										
									}
									$menu_access[$v["action"]] = $v["url"];
									$active = ($curent==$v["action"]) ? "class='active'" : "";
									$click = "javascript: window.open('index.php?p=".$v["action"]."','_self')";
									echo '<li '.$active .'><a href="#" onclick="'.$click.'">'.$v["name"].'</a></li>';
									?>
									<?php
								}
								?>
					</ul>
				</li>    
				<?php
			}else{
				$active = ($curent==$value["action"]) ? "class='active'" : "";
				$click = "javascript: window.open('index.php?p=".$value["action"]."','_self')";
				echo '<li '.$active .'><a href="#" onclick="'.$click.'">'.$value["name"].'</a></li>';
			}
		}
		?>
		</ul>
		<ul class="nav navbar-nav navbar-right user-nav">
		  <li class="profile_menu">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img style="width:30px; height:30px;" src="<?php echo  $EMPIMG; ?>" /> <b class="fa fa-user"></b> <?php echo $USERNAME; ?>&nbsp;( <?php echo $RIGTHTYPENAME; ?> )</a>
		  </li>
		  <li class="button"><a href="update-logout.php"  onclick="return confirm('Confirm Sign Out ?');"> <i class="fa  fa-sign-in"></i></a></li>
		</ul>           

	  </div><!--/.nav-collapse -->
	</div>
  </div>
