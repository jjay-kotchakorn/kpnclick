<?php /*
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">

	<!-- Latest compiled and minified CSS -->
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="js/bootstrap/dist/css/bootstrap.css" rel="stylesheet">

	<!-- Optional theme -->
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"> -->

	<!-- Latest compiled and minified JavaScript -->
	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->

	<title></title>
</head>
<body>
	
</body>
</html>
*/ ?>


<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- <link rel="shortcut icon" href="images/redcross.png"> -->

	<title>บริจาคเงินออนไลน์ สำนักงานจัดหารายได้ สภากาชาดไทย</title>
	<link rel="icon" type="image/png" href="./images/icon-redcross.ico">
	<!-- <link rel="icon" type="image/png" href="http://www2.redcrossfundraising.org/wp-content/uploads/2016/06/favicon.ico"> -->
	<!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:300,200,100' rel='stylesheet' type='text/css'> -->

	<!-- Bootstrap core CSS -->
	<link href="./js/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="./js/jquery.gritter/css/jquery.gritter.css" />

	<link rel="stylesheet" href="./fonts/font-awesome-4/css/font-awesome.min.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="../../assets/js/html5shiv.js"></script>
	  <script src="../../assets/js/respond.min.js"></script>
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="./js/jquery.nanoscroller/nanoscroller.css" />
	<link rel="stylesheet" type="text/css" href="./js/jquery.easypiechart/jquery.easy-pie-chart.css" />
	<link rel="stylesheet" type="text/css" href="./js/bootstrap.switch/bootstrap-switch.css" />
	<link rel="stylesheet" type="text/css" href="./js/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css" />
	<link rel="stylesheet" type="text/css" href="./js/jquery.select2/select2.css" />
	<link rel="stylesheet" type="text/css" href="./js/bootstrap.slider/css/slider.css" />
	<link rel="stylesheet" type="text/css" href="./js/jquery.datatables/bootstrap-adapter/css/datatables.css" />
	<link rel="stylesheet" type="text/css" href="./js/dropzone/css/dropzone.css" />
	<link rel="stylesheet" type="text/css" href="./js/jquery.niftymodals/css/component.css" />
   <!--insert datatable core-->

 	<link rel='stylesheet' type='text/css' href='./js/jquery.fullcalendar/fullcalendar/fullcalendar.css' />
	<link rel='stylesheet' type='text/css' href='./js/jquery.fullcalendar/fullcalendar/fullcalendar.print.css'  media='print' />  
	<link href="./media/css/demo_table.css" rel="stylesheet" type="text/css" />
	<link href="./media/css/TableTools.css" rel="stylesheet" type="text/css" />   
<?php /*	
	<link href="css/style.css" rel="stylesheet" />
*/ ?>
	<link href="./css/datepicker.css" rel="stylesheet" />
	<!-- notification plugin -->
    <script src="./js/lib.js"></script> 
    
    <!-- themes pay-gate -->
    <link rel='stylesheet' type='text/css' href='./css/paygatestlye.css' />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>