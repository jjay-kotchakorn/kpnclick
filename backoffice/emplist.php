<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;

?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
					<div class="page-head">
						<button class="btn btn-success btn-small pull-right" onclick="addnew()" style="margin-top:10px;"><i class="fa fa-plus"></i> เพิ่มผู้ควบคุมการปฏิบัติงาน </button>
						<h3><i class="fa fa-list"></i> &nbsp;ผู้ควบคุมการปฏิบัติงาน</h3>
					</div>
					<table id="tbEmp" class="table" style="width:100%">
						  <col width=3>
						  <col width=70>
						  <col width=30>
						  <col width=20>
						  <col width=10>

						  <col width=10>
						  <col width=30>
						  <thead>
							  <tr>
								  <th width="">ลำดับ</th>
								  <th width="">สังกัด</th>
								  <th width="">ตำแหน่ง</th>
								  <!-- <th width="11%">คำนำหน้าชื่อ</th> -->
								  <th width="">ชื่อ-สกุล</th>
								  <th width="">หมายเลขโทรศัพท์</th>

								  <!-- <th width="">E-mail</th> -->
								  <th width="">Manage</th>
							  </tr>
						  </thead>   
						<tbody>
						</tbody>
					</table>
					<br><br>
				</div>
			</div>

		</div>
	</div> 
</div>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
$(document).ready(function() {
	var oTable;
	$("#frmMain").validate();
	listItem();
});

function listItem(){
   var url = "data/emplist.php";
   oTable = $("#tbEmp").dataTable({
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
   	   "sInfoEmpty": "",
   		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons":  ""
		},
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({"name":"menu_id","value":$("#srchMenuId").val()});
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
		}
   }); 
}

function editInfo(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&emp_id="+id+"&type=info";
   redirect(url);
}
function addnew(){
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=info";
   redirect(url);
}
function link_url(id){
   	var url = "http://www.sec.or.th";
   window.open(url, '_blank');
}
function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}

</script>