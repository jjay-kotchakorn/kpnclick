<?php
include_once "./lib/lib.php";
global $db;
 $aSrch = $aTmpMenu = get_menu();
?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-6">
				<div class="content block-flat ">
					<div class="header" style="border-bottom:none;">							
						<h3><i class="fa fa-list"></i> &nbsp;Submenu</h3>
					</div>
					<div  style="float:right;margin-right:25px;">
						<label class="col-sm-6 control-label" style="text-align:right;">Main Menu</label>
						<div class="col-sm-6">
							<select id="srchMenuId" name="srchMenuId" onChange="reCall()">
								<option select>All</option>
							<?php if(is_array($aSrch)) {
								foreach($aSrch as $key=>$val){
									echo '<option value="',$val["menu_id"],'">',$val["name"],'</option>';			     	

								}						
							}?>
							</select> 
						</div>
					</div>
					<table id="tbMenu" class="table" style="width:100%">
						<thead>
							<tr class="no-border">
								<th width="8%">No</th>
								<th width="9%">Code</th>
								<th width="20%">Entries</th>
								<th width="19%">Entries Eng</th>
								<th width="19%">Parent Menu</th>
								<th width="10%">Status</th>
								<th width="15%">Manage</th>
							</tr>
						</thead>   
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="content block-flat ">
					<div class="header">							
						<h3><i class="fa fa-edit"></i> &nbsp;Submenu Detail <span id="showname"></span></h3>
					</div>					
					<div class="content">
						<form class="form-horizontal" id="frmMain" method="post" action="update-menulist.php">
							<input type="hidden" name="menu_id">
							<input type="hidden" name="menulist_id">
							<fieldset>
								<div class="form-group">
									<label class="col-sm-1 control-label">Main</label>
									<div class="col-sm-5">
										<select id="menu_id" name="menu_id" class="form-control">
										<?php if(is_array($aTmpMenu)) {
										 foreach($aTmpMenu as $k=>$v){
											echo '<option value="',$v["menu_id"],'">',$v["name"],'</option>';			     	

										 }
											
										}

										?>
										</select>
									</div>               			                
								</div>								
								<div class="form-group">
									<label class="col-sm-1 control-label">Code</label>
									<div class="col-sm-3">
										<input name="code" class="form-control" required placeholder=" " type="text">
									</div>			                			                
									<label class="col-sm-2 control-label">Name</label>
									<div class="col-sm-6">
										<input class="form-control" name="name" required placeholder=" " type="text">
									</div>			                			                
								</div>
								<div class="form-group">
									<label class="col-sm-1 control-label">Icon</label>
									<div class="col-sm-3">
										<input class="form-control" name="icon"  placeholder=" " type="text">
									</div>
									<label class="col-sm-2 control-label">Name Eng</label>
									<div class="col-sm-6">
										<input class="form-control" name="name_eng" placeholder=" " type="text">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-1 control-label">File</label>
									<div class="col-sm-3">
										<input class="form-control" name="url" required placeholder=" " type="text">
									</div>
									<label class="col-sm-2 control-label">Class</label>
									<div class="col-sm-5">
										<input class="form-control"  name="use_class" placeholder=" " type="text">
									</div>
								</div>								
								<div class="form-group">
									<label class="col-sm-1 control-label">Action</label>
									<div class="col-sm-3">
										<input class="form-control" name="action" required placeholder=" " type="text">
									</div>
									<label class="col-sm-2 control-label">Status</label>
									<div class="col-sm-5">
										<select name="active" class="form-control" id="active" class="required">
											<option selected="selected" value="T">active</option>
											<option value="F">inActive</option>
										</select>
									</div>
								</div>
								<div class="content">
									<button type="button" class="btn btn-primary" onClick="ckSave()">Save changes</button>
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">Cancel</button>
								</div>
							</fieldset>
						</form>					
					</div>
				</div>
			</div>
		</div>
	</div> 
</div>

<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
$(document).ready(function() {
	var oTable;
	$("#frmMain").validate();
	listItem();
	var menulist_id = "<?php echo $_GET["menulist_id"]?>";
	if(menulist_id) viewInfo(menulist_id);
});

function listItem(){
   var url = "data/menulist.php";
   oTable = $("#tbMenu").dataTable({
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
   	   "sInfoEmpty": "",
   		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons":  ""
		},
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({"name":"menu_id","value":$("#srchMenuId").val()});
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
		}
   }); 
}

function editInfo(id){
	if(typeof id=="undefined") return;
   var url = "data/menulist.php";
	var param = "menulist_id="+id+"&single=T";
	dataUrl(url, param,"#frmMain");
   $('html, body').animate({
      scrollTop: $("#boxContent").offset().top
   }, 600);
}

function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}
function ckSave(id){
   onCkForm("#frmMain");
   $("#frmMain").submit();
}
</script>