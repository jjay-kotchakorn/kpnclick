<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/authen.php";
global $db;
$emp_id = $_SESSION["login"]["info"]["emp_id"];
// d($_SESSION);
// echo $emp_id;

$str = "รายการบริจาค";

$q = "SELECT
		p.project_id,
		p.code_project,
		p.name_th,
		p.name_eng,
		p.`status`,
		p.projecttype_id,
		p.active,
		p.recby_id,
		p.rectime,
		p.remark,
		p.createtime,
		p.dev_code
		FROM
		project AS p 
		WHERE p.active = 'T'
	";
	//echo $sQuery; die();
$project = $db->get($q);

$q = "SELECT
	pb.payment_bank_id,
	pb.bank_id,
	pb.`name`,
	pb.name_eng,
	pb.active,
	pb.recby_id,
	pb.rectime
	FROM payment_bank AS pb
	WHERE pb.active = 'T'
";
//echo $sQuery; die();
$payment_bank = $db->get($q);

$apay = datatype(" and a.active='T'", "pay_status", true);
$arr_pay = array();
foreach ($apay as $key => $value) {
	$arr_pay[$value["pay_status_id"]] = $value["name"];
}
?>


<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
					<div class="page-head">
						<!-- <button id="add" class="btn btn-success btn-small pull-right" onclick="addnew()" style="margin-top:10px;display:none;"><i class="fa fa-plus"></i> เพิ่มการลงทะเบียน</button> -->
						<h3><i class="fa fa-list"></i> &nbsp; <?php echo $str; ?></h3>
					</div>
					<div class="header">
						<div class="form-group row">
							        
<!-- 							        
						</div>
						<div class="form-group row">
 -->
							<label class="col-sm-1 control-label" style=" padding-right:0px;">ช่องทางบริจาค</label>
							<div class="col-sm-2">
								<select name="sBank" id="sBank" class="form-control" onchange="reCall();">
									<option selected="" value="">แสดงทั้งหมด</option>
									<?php 
										foreach ($payment_bank as $key => $c) {
											$payment_bank_id = $c["payment_bank_id"];
											$payment_bank_name = $c["name"];			
										?>
											<option value="<?php echo $payment_bank_id;?>"><?php echo $payment_bank_name;?></option>	
										<?php
										}//end loop $c
										?>
								</select>
							</div>   

							<label class="col-sm-1 control-label" style=" padding-right:0px;">สถานะบริจาค </label>
							<div class="col-sm-2">
								<select name="sStatus" id="sStatus" class="form-control" onchange="reCall();">
									<option value="">แสดงทั้งหมด</option>
									<?php foreach ($apay as $key => $value) {
										$id = $value['pay_status_id'];
										$s = "";
										if($info["pay_status"]==$id){
											$s = "selected";
										}
										$name = $value['name'];
										echo  "<option value='$id' {$s}>$name</option>";
									}//end loop $value 
								?>
								</select>
							</div>
							<label class="col-sm-1 control-label" style="padding-right:0px;">ใบเสร็จ</label>
							<div class="col-sm-2">
								<select name="bill_request" id="bill_request" class="form-control required" onchange="reCall();">
									<option selected="selected" value="">ทั้งหมด</option>
									<option value="T">ต้องการ</option>
									<option value="F">ไม่ต้องการ</option>
								</select>	                			
							</div>
						</div>
						<div class="form-group row">

							<label class="col-sm-1 control-label">โครงการ</label>
							<div class="col-sm-5">
								<select name="sProject" id="sProject" class="form-control" onchange="reCall();">
										<option value="">แสดงทั้งหมด</option>
										<?php 
										foreach ($project as $key => $c) {
											$project_id = $c["project_id"];
											$project_name = $c["name_th"];			
										?>
											<option value="<?php echo $project_id;?>"><?php echo $project_name;?></option>	
										<?php
										}//end loop $c
										?>
								</select>
							</div>

						</div>	
					<hr style="border-bottom: 1px  #eee dotted;width: 95%;">	

					<div class="form-group row">

						<label class="col-sm-2 control-label"  style=" padding-right:0px;">วันที่เริ่มทำรายการบริจาค: </label>

						<label class="col-sm-1 control-label"  style=" padding-right:0px;">วันที่เริ่ม</label>
						<div class="col-sm-1"  style="padding-left:0px; padding-right:0px;">					
							<input class="form-control" name="date_start" value="" id="date_start" onblur="" placeholder="วันที่เริ่ม" type="text">

						</div>  
						<label class="col-sm-1 control-label"  style=" padding-right:0px;">วันที่สิ้นสุด</label>
						<div class="col-sm-1"  style="padding-left:0px; padding-right:0px;">
							<input class="form-control" name="date_stop" id="date_stop" onblur="" placeholder="วันที่สิ้นสุด" type="text">
						</div> 
						<label class="col-sm-1 control-label"></label>
					    <span class="red" style="margin-left: 0px;">** ถ้าต้องการ Export file ต้องเลือกช่วงวันที่เริ่มทำรายการบริจาค</span>                                       
					</div>

					<div class="form-group row">

						<label class="col-sm-2 control-label"  style=" padding-right:0px;"> วันที่ทำรายการสำเร็จ: </label>

						<label class="col-sm-1 control-label"  style=" padding-right:0px;">วันที่เริ่ม</label>
						<div class="col-sm-1"  style="padding-left:0px; padding-right:0px;">
							<input class="form-control" name="date_start_result" value="" id="date_start_result" onblur="" placeholder="วันที่เริ่ม" type="text">
						</div>                                          
						<label class="col-sm-1 control-label"  style=" padding-right:0px;">วันที่สิ้นสุด</label>
						<div class="col-sm-1"  style="padding-left:0px; padding-right:0px;">
							<input class="form-control" name="date_stop_result" id="date_stop_result" onblur="" placeholder="วันที่สิ้นสุด" type="text">
						</div> 

						<label class="col-sm-1 control-label">
							<a href="#" class="btn" onclick="reCall();"><i class="fa fa-search">&nbsp;</i> ค้นหา</a>&nbsp;&nbsp;
							<!-- <button onclick="clearSearch();" id="ok" class="btn btn-success" type="button">Clear</button> -->
						</label>     
						
						<label class="" style='cursor: pointer;font-size: 16px;color: #7141ffdb; padding-right: 15px;'>
							<input type="checkbox" name="chk_receipt_update" id="chk_receipt_update-1" value="T" checked="" onclick="test();"> อัพเดทสถานะใบเสร็จอัตโนมัติหลังจาก Export file
						</label>
	 
	 					
						<a href="#" class="btn btn-primary" onclick="printInfo();"
							style="margin-left: 20px;" 
						>
							<i class="fa fa-file-text">&nbsp;</i> Export</a>

					</div>

				</div>
				<br>

				<table id="tbCourse" class="table" style="width:100%">
					<thead>
						<tr>
							<th style="text-align:center" width="5%">
								<input type="checkbox" id="checkAll" style="float:right;"> ลำดับ
							</th>
							<!-- <th style="text-align:center" width="5%">ID</th> -->
							<th style="text-align:center" width="10%">วันที่ทำรายการ</th>
							<th style="text-align:center" width="10%">วันที่หมดเขต</th>
							<th style="text-align:center" width="15%">ชื่อ-นามสกุล</th>
							<th style="text-align:center" width="20%">โครงการ</th>
							<th style="text-align:center" width="10%">จำนวนเงิน</th>
							<th style="text-align:center" width="10%">วันที่ทำรายการสำเร็จ</th>
							<th style="text-align:center" width="10%">ช่องทางบริจาค</th>
							<th style="text-align:center" width="15%">สถานะบริจาค</th>
							<th style="text-align:center" width="10%">ใบเสร็จ</th>
							<th style="text-align:center" width="10%">สถานะใบเสร็จ</th>
							<!-- <th style="text-align:center" width="10%">Manage</th> -->
						</tr>
					</thead>   
					<tbody>
					</tbody>
				</table>
				<div class="clear"></div>
<!-- 				
<div class="filters">     
	<div class="btn-group" style="">
		<a href="#" class="btn btn-info" onclick="printInfo();"><i class="fa fa-file-text">&nbsp;</i> Export</a>
	</div>

</div>
 -->
					
				<hr>
				<div class="filters"> 
					<div class="form-group row" style="margin-left: 0px;">
						<span style="font-size: 20px;font-weight:  bold;color: #0005e8;margin-right: 10px;">สถานะใบเสร็จ : </span>
						
					<?php 
						$q = "SELECT
								a.receipt_status_id,
								a.`code`,
								a.`name`,
								a.name_eng,
								a.active,
								a.recby_id,
								a.rectime,
								a.remark
							FROM receipt_status AS a
							WHERE a.active='T'
							ORDER BY a.order ASC
						";
						$receipt_status = $db->get($q);

						foreach ($receipt_status as $key => $value) {
							$id = $value['receipt_status_id'];
							$name = $value['name'];
							$checked = "";
							// $checked = ($id==1) ? "checked" : "";

							echo  "								
									<label style='cursor: pointer;font-size: 16px;color: #ff4141; padding-right: 15px;'><input type='radio' name='receipt_status_id' id='receipt_status_id' value='$id' $checked
										onclick='test();'> $name</label> 						
							";
						}//end loop $value 
					?>						
						
						<br>
						<a href="#" class="btn btn-success" onclick="update_receipt();">
							<i class="fa fa-file-text"></i> Update สถานะใบเสร็จ</a>
					</div>

				</div>

			</div>
		</div>

	</div>
</div> 
</div>
<!-- 
<form id="print_form" action="" method="post">
	<input type="hidden" id="register_ids" name="register_id">;		
</form>
 -->
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
	$(document).ready(function() {
		var get_type = "<?php echo $_GET["type"]; ?>";
		if(get_type=="childlist" || get_type=="course_order") $("#add").hide();
		var oTable;
		listItem();	
		$("#date_start").datepicker({language:'th-th',format:'dd-mm-yyyy'});
		$("#date_stop").datepicker({language:'th-th',format:'dd-mm-yyyy'});
		$("#date_start_result").datepicker({language:'th-th',format:'dd-mm-yyyy'});
		$("#date_stop_result").datepicker({language:'th-th',format:'dd-mm-yyyy'});
		// $("#section_id").change(function(event) {
		// 	var id = $(this).val();
		// 	getDropDown('#coursetype_id', id, 'coursetype', 'data/coursetype.php')
		// }); 
	});

	function listItem(){
		var get_type = "<?php echo $_GET["type"]; ?>";
		var url = "data/donationlist.php";
		oTable = $("#tbCourse").dataTable({
			"sDom": 'T<"clear">lfrtip',
			"oLanguage": {
				"sInfoEmpty": "",
				"sInfoFiltered": ""
			},
			"oTableTools": {
				"aButtons":  ""
			},
			"bProcessing": true,
			"bServerSide": true,
			"sAjaxSource": url,
			"bSort": false,
			"sPaginationType": "full_numbers",
			"aaSorting": [[ 0, "desc" ]],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				//aoData.push({"name":"section_id","value":$("#section_id").val()});		
				//aoData.push({"name":"member_id","value":$("#member_id").val()});			
				//aoData.push({"name":"course_detail_id","value":$("#course_detail_id").val()});			
				aoData.push({"name":"bill_request","value":$("#bill_request").val()});
				aoData.push({"name":"sProject","value":$("#sProject").val()});
				aoData.push({"name":"sBank","value":$("#sBank").val()});
				aoData.push({"name":"sStatus","value":$("#sStatus").val()});		
				aoData.push({"name":"active","value":"T"});			
				aoData.push({"name":"type","value":get_type});
				aoData.push({"name":"date_start","value":$("#date_start").val()});			
				aoData.push({"name":"date_stop","value":$("#date_stop").val()});
				aoData.push({"name":"date_start_result","value":$("#date_start_result").val()});
				aoData.push({"name":"date_stop_result","value":$("#date_stop_result").val()});	
				$.ajax( {
					"dataType": 'json', 
					"type": "POST", 
					"url": sSource, 
					"data": aoData, 
					"success": fnCallback
				});
			}
		}); 
		$('#tbCourse_filter input').unbind();
		$('#tbCourse_filter input').bind('keyup', function(e) {
			if(e.keyCode == 13) {
				oTable.fnFilter(this.value);   
			}
		});
	}

	function editInfo(id){
		if(typeof id=="undefined") return;
		var url = "index.php?p=<?php echo $_GET["p"];?>&payment_bank_id="+id+"&type=bankinfo";
		redirect(url);
	}


	function childlist(id){
		if(typeof id=="undefined") return;
		var url = "index.php?p=<?php echo $_GET["p"];?>&register_id="+id+"&type=childdetail";
		redirect(url);
	}

	function addnew(){
		var url = "index.php?p=<?php echo $_GET["p"];?>&type=bankinfo";
		redirect(url);
	}

	function reCall(){
		oTable.fnClearTable( 0 );
		oTable.fnDraw();
	}

	function selectMember(){
		var url = "member map";
		memberpop(url,"ret_member_select");	
	}

	function ret_member_select(id){
		for(var i in id){
			var ck = "";
			var data = id[i];
			$("#member_id").val(data["member_id"]);
			$("#membername").val(data.name_th);
			reCall();
		}
	}

	function addcourse_detail_other(){
		var url = "course_detail map";
		courseDetailData(url,"ret_detail_other");	
	}

	function ret_detail_other(id){
		for(var i in id){
			var ck = "";
			var data = id[i];
			$("#course_detail_id").val(data.course_detail_id);
			console.log(id);
			var str = data.date+" เวลา :  "+data.time+" สถานที่ : "+data.address_detail+" "+data.address+ " จำนวนที่นั่ง : "+data.chair_all;
			$("#course_detail_name").val(str);
			reCall();
		}
	}

	function clearSearch(){
		$("#course_detail_name").val("");
		$("#course_detail_id").val("");
		$("#member_id").val("");
		$("#membername").val("");
		reCall();
	}


	function escapeHtml(unsafe) {
		return unsafe
		.replace(/&/g, "&amp;")
		.replace(/</g, "&lt;")
		.replace(/>/g, "&gt;")
		.replace(/"/g, "&quot;")
		.replace(/'/g, "&#039;");
	}


	function saveInfo(id){
		if(typeof id=="undefined") return;
		if(id=="remark"){
			var remark = $("#remark").val();
			if($.trim(remark)==""){
				alert("กรุณาใส่เหตุผล");
				return;
			}else{
				$('#model-sync').modal('hide');
			}
			var status = $("#remark_status").val();
			var id = $("#remark_id").val();
			if(id=="" || status=="") return;
			$("#remark_header").html('');
			$("#remark_status").val('');
			$("#remark_id").val('');
		}else{
		//var t = confirm("ยืนยัน update  สถานะ การชำระเงิน ? ");
		//if(!t) return false;		
		var status = $("#pay_status"+id).val();
		if(status==7 || status==8){
			var title = "เหตุผลในการ";
			title += (status==8) ? 'ยกเลิก' : 'คืนเงิน';
			$("#remark_header").html(title);
			$("#remark_status").val(status);
			$("#remark_id").val(id);
			$('#model-sync').modal('show');
			return;
		}
		var remark = "";
	}

	var i = 0;
	if(status>0){
		$.ajax({
			"type": "POST",
			"async": false, 
			"url": "data/register-status-update.php",
			"data": {'register_id': id, 'status' : status, 'remark' : remark}, 
			"success": function(data){	
				console.log(data);
				$.gritter.removeAll({
					after_close: function(){
						$.gritter.add({
							position: 'center',
							title: 'Success',
							text: data,
							class_name: 'success'
						});
					}
				});
				reCall();				      							 
			}
		});

		$.ajax({
			"type": "POST",
			"async": false, 
			"url": "check-course-elearning.php",
			"data": {'register_id': id, 'pay_status': status}, 
			"success": function(data){	
				//console.log(data);			      							 
			}
		});
		
	}//end if
}//end func

function printInfo(id){
	var url = "receipt-print.php?register_id="+id;
	window.open(url,'_blank');
}

$("#checkAll").click(function(){
	$("#tbCourse tbody td :checkbox").prop('checked', $(this).prop("checked"));
}); 

function printInfo(){

/*
   	var url = "report/receipt-export.php";
   	$("#register_ids").val(id);
   	$("#print_form").attr("action", url);
   	$("#print_form").submit();
   	return;
   	*/
   	var emp_id = "<?php echo $emp_id; ?>";
   	var chk_receipt_update = $("input:checkbox[name=chk_receipt_update]:checked").val();

   	var bank = $("#sBank").val();
   	var status = $("#sStatus").val();
   	var bill_request = $("#bill_request").val();
   	var project = $("#sProject").val();

   	var date_start = $("#date_start").val();
   	var date_stop = $("#date_stop").val();
   	
   	var date_start_result = $("#date_start_result").val();
   	var date_stop_result = $("#date_stop_result").val();

   	if (date_start=="" && date_stop=="") {
   		swal({
		  type: 'error',
		  title: 'กรุณาเลือกช่วงวันที่เริ่มทำรายการบริจาค',
		  text: 'เพื่อลดภาระการทำงานของ server',
		});
		return;
   	}else{
	   	var param = "bank="+bank+"&status="+status+"&bill_request="+bill_request+"&project="+project;
	   	param = param+"&date_start="+date_start+"&date_stop="+date_stop;
	   	param = param+"&date_start_result="+date_start_result+"&date_stop_result="+date_stop_result;
	   	param = param+"&chk_receipt_update="+chk_receipt_update+"&emp_id="+emp_id;
	 	var url = "report/receipt-export.php?"+param;
		window.open(url,'_blank');
   	}//end else


}//end func

function update_receipt(){
	if(typeof id!="undefined"){

	}else{
	    var id = "";
	    $("#tbCourse tbody tr :checkbox").each(function() {
	        if ($(this).is(":checked")) {
	            var str = $(this).val();
	            id += str+",";
	        }
	    });
	}//end else

	if(id==""){
		// alert("ยังไม่ได้เลือกรายการ ? ");
		swal({
		  type: 'error',
		  title: 'ยังไม่ได้เลือกรายการบริจาค !!',
		});
		return;
	}else{
		// var receipt_status_id = $("#receipt_status_id").val();
		var receipt_status_id = $("input:radio[name=receipt_status_id]:checked").val();
		var url = "data/update-receipt.php";
		var emp_id = "<?php echo $emp_id; ?>";

		$.ajax({ 
		    type: 'POST', 
		    url: url, 
		    data: { id: id
		    	, receipt_status_id: receipt_status_id 
		    	, emp_id: emp_id 
			}, 
		    dataType: 'json',
		    success: function (data) { 
		        // console.log(data);
		        var status = data.status;
		        var title = data.title;
		        var text = data.text;
		        var datetime = data.datetime;
		        var debug = data.debug;
		        if ( status=="error" ) {
		        	swal({
					  type: 'error',
					  title: title,
					  text: text,
					});
		        }else if( status=="success" ){
		        	swal({
					  type: 'success',
					  title: title,
					  text: text,
					});
					reCall();
		        }
		    }//end success
		});

		
/*		
		swal({
		  type: 'success',
		  title: receipt_status,
		});
*/		
	}//end else

}//end func


function test(){
	return;
	// var receipt_status_id = $("#receipt_status_id").val();
	var receipt_status_id = $("input:radio[name=receipt_status_id]:checked").val();
	var chk_receipt_update = $("input:checkbox[name=chk_receipt_update]:checked").val();

	// alert(chk_receipt_update);


	swal({
	  type: 'success',
	  title: chk_receipt_update,
	});
	
}

</script>
