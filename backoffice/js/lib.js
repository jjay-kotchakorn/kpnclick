function pop(url,reCall,width,height){
   if(!url) return;
   if(typeof width=="undefined")
	   width = 700;
   if(typeof height=="undefined")
	   height = 450;
   $.fancybox({
	   'closeBtn' : false,
       'autoSize'	 : true,
	   'width'         	 : width,
	   'height'        	 : height,
	   'autoScale'		 : false,
	   'centerOnScroll'  : true,
	   'transitionIn'	 : 'none',
	   'transitionOut'	 : 'fade',
	   'titlePosition'   : 'inside',
	   'type'		     : 'iframe',
	   'href'		     : url,				
	   'onClosed': function (){
		   if(typeof reCall!="undefined" && reCall!=""){
			   parent.reView();
			}
		   parent.$.fancybox.close();
			   
      }
   });
}
// fancybox Close function if fancy dialog can't close
function fancyE(){
   parent.location.reload(true);
   parent.$.fancybox.close(); 
}
//javascript redirect url
function redirect(url){
	if(typeof url=="undefined" && url=="") return; 
   location = url;  
}
/*
var aColumns = [     
				 { "sName": "unv_id","sWidth":150 },  
				 { "sName": "unv_name",  "sWidth":50 },
		      ];
var dataString = $("#form1").serialize();
getDataTable("scripts-method/index-unv-post.php?id=2&"+dataString, "#x");
*/

function getDataTable(url, id, txtSrch, aColumns, bt){
   if(typeof url=="undefined" || url=="") return;
   if(typeof id=="undefined" || id=="") id = "#example";
   if(typeof bt=="undefined" || bt=="")
	   bt =  [	
					{
						"sExtends": "xls",
						"sButtonText": "Save for Excel"
					}
			   ];
	oTable = $(id).dataTable( {
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
	   "sInfoEmpty": "",
		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons": bt
		},
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		"aoColumns": aColumns,
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
		}
   });
}
function getDataTableNoExport(url, id, txtSrch, aColumns){
   if(typeof url=="undefined" || url=="") return;
   if(typeof id=="undefined" || id=="") id = "#example";
	oTable = $(id).dataTable( {
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
	   "sInfoEmpty": "",
		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons": false
		},
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		"aoColumns": aColumns,
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
		}
   });
}

function checkIDCard(id){
   if(id.length != 13){     
      return false;
   }
   for(i=0, sum=0; i<12; i++)
      sum += parseInt(id.charAt(i))*(13-i);
   sum = (11-(sum%11))%10
   if(sum !=parseInt(id.charAt(12))){      
      return false;
   }
   return true;
}

function dateThai(tag){
	
 	$(tag).datepicker({
		dateFormat: 'dd/mm/yy',
		dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'], 
		monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],
		changeMonth: true,
		changeYear: true ,
		 yearRange:"-100:+10",
		beforeShow:function(){
			if($(this).val()!=""){
				var arrayDate=$(this).val().split("/");		
				arrayDate[2]=parseInt(arrayDate[2])-543;
				if(typeof arrayDate[0]!="undefined" && typeof arrayDate[1]!="undefined" && typeof arrayDate[2]!="undefined")
				   $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);
			}
			setTimeout(function(){
				$.each($(".ui-datepicker-year option"),function(j,k){ 
					var textYear=parseInt($(".ui-datepicker-year option").eq(j).val())+543;
					$(".ui-datepicker-year option").eq(j).text(textYear);
				});				
			},50);

		},
		onChangeMonthYear: function(){
			setTimeout(function(){
				$.each($(".ui-datepicker-year option"),function(j,k){
					var textYear=parseInt($(".ui-datepicker-year option").eq(j).val())+543;
					$(".ui-datepicker-year option").eq(j).text(textYear);
				});				
			},50);		
		},
		onClose:function(){
			if($(this).val()!="" && $(this).val()==dateBefore){			
				var arrayDate=dateBefore.split("/");
				arrayDate[2]=parseInt(arrayDate[2])+543;
				if(typeof arrayDate[0]!="undefined" && typeof arrayDate[1]!="undefined" && typeof arrayDate[2]!="undefined")
				$(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);	
			}		
		},
		onSelect: function(dateText, inst){ 
			dateBefore=$(this).val();
			var arrayDate=dateText.split("/");
			arrayDate[2]=parseInt(arrayDate[2])+543;
			if(typeof arrayDate[0]!="undefined" && typeof arrayDate[1]!="undefined" && typeof arrayDate[2]!="undefined")
			$(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);
		}
	});		
}

function setNet(tag, fixSize, alertMsg){
   if(typeof alertMsg=="undefined") alertMsg = true;
   if(typeof tag.value!="undefined")
      var val = tag.value;
   else
      var val = tag;
   if(typeof fixSize=="undefined") fixSize = 2;
   val = ""+val+"";
   while(val.indexOf(",")!=-1)
      val = val.replace(",", "");
   val = val - 0;
   if( isNaN(val) ){
      if(alertMsg)
         alert("ใส่ตัวเลข");
      return false;
   }
   var sign = "";
   if(val < 0){
      sign = "-";
      val = 0-val;
   }
   val = val.toFixed(fixSize)+"";
   returnVal = "";
   if(val.indexOf(".")>=0){
      var padSize = 3;
   }else{
      var padSize = 0;
      fixSize = -1;
   }
   for(var i=val.length; i>=0; i--){
      returnVal = val.charAt(i)+returnVal;
      if( returnVal.length>(fixSize- -padSize) ){
         if(i!=0 && i!=val.length && ( (val.length-i-fixSize-1 )%3==0 ) ) returnVal = ","+returnVal;
      }
   }
   if(sign=="-"){
      if( returnVal.indexOf("1") == -1

      && returnVal.indexOf("2") == -1

      && returnVal.indexOf("3") == -1

      && returnVal.indexOf("4") == -1

      && returnVal.indexOf("5") == -1

      && returnVal.indexOf("6") == -1

      && returnVal.indexOf("7") == -1

      && returnVal.indexOf("8") == -1

      && returnVal.indexOf("9") == -1 ) sign = "";
   }
   if(typeof tag.value!="undefined")
      tag.value = sign+returnVal;
   else return sign+returnVal;

   return true;

}


function getDropDown(tagNext, val, txtCon, url){
	if(typeof url=="undefined") return;
	$.getJSON(url, {name:txtCon,value:val}, function(data) {
		var select = $(tagNext);
		var options = select.attr('options');
		$('option', select).remove();
		$(select).append('<option value=""> - เลือก - </option>');
		$.each(data, function(index, array) {
			$(select).css("display","inline");
			$(select).append('<option value="' + array[0] + '">' + array[1] + '</option>');
		});
	});	
}

function getLink(param){
	var tmp = location.href;
	var array = tmp.split('&');
	url = array[0];
	location = url+"&"+param; 
}

function clearPage(p){
   var tmp = location.href;
   var array = tmp.split('?');
   if(typeof p!="undefind")
   location = array[0]+"?p="+p;
}

function redirectUrl(url){
	if(typeof url=="undefined") return;
	$(window).attr("location",url);
}

function setVal(tag, data){
   $(tag).children().each(function(){
      var tagName = $(this).attr("id");
      if(typeof tagName=="undefined")
         tagName = $(this).attr("name");
      if(typeof tagName!="undefined"){
         if($(this).is("input, select, textarea")){
            if($(this).is(":checkbox, :radio")){
               if(data[tagName]=="T")
	              $(this).prop('checked',true);
	           else if(data[tagName]=="F")
	              $(this).prop("checked", false);
	           else if(data[tagName]!="" && data[tagName]==$(this).val()){
	           	  $(this).prop("checked", true);
	           }
	        }else{
               $(this).val(data[tagName]);
	        }
         }else{
            if($(this).is("img")){
			   $(this).attr('src', data[tagName]);
            }else{
	           $(this).text(data[tagName]);
		    }
		 }
      }
	  if($(this).length>0){
         setVal(this, data);
	  }
   });
}

function addTrLine(table, trLine, data, runNo){
  	var tagLine  = $(trLine).clone();
	if(typeof runNo!="undefined") data[runNo] = $(table+" tbody>tr").length- -1;
	if(typeof data!="undefined") setVal(tagLine, data);
	$(table+" tbody").append(tagLine);
	return tagLine;
}

function delRow(table){
   $(table+" tbody>tr").remove();
}

function delCkRow(table, all){
   $(table+" tbody tr :checkbox").each(function(){
		if($(this).is(":checked")){
		}else{
         $(this).parent().parent().remove();
		}
  });
  if(typeof all!="undefined") delRow(table);
}

function onCkForm(formId) {
   $(formId+" :checkbox").each(function(){
      if($(this).is(':checked')){
      	$(this).prop('checked',true);
        $(this).val('T');
      }else{
    	 $(this).prop('checked',true);
         $(this).val('F');
      }   
   });
}

function popUp(url,width,height){
   if(!url) return;
   if(typeof width=="undefined")
	   width = 700;
   if(typeof height=="undefined")
	   height = 450;
   $.fancybox({
	   'closeBtn'	: false,
       'autoSize'	: false,
	   'width'         	: width,
	   'height'        	: height,
	   'autoScale'			: false,
	   'centerOnScroll' : true,
	   'transitionIn'		: 'fadeIn',
	   'transitionOut'		: 'fadeOut',
	   'titlePosition'     : 'inside',
	   'type'				: 'iframe',
	   'href'				:url,				
	   'onClosed': function (){
	      //parent.location.reload(true);	
      }
   });
}

function selectData(table, retFunc, ckBox){
	var tmp = table.split(' ');
	var str = "search-datatype.php?table=";
	for(var i in tmp){
		str = str+tmp[i]+"&";
   }
	var url = str.substring(0, str.length-1);
	if(typeof retFunc!="undefined" && retFunc!=""){
	   url = url+"&retFunc="+retFunc;
	}
	if(typeof ckBox!="undefined")
		url = url+"&ckBox=T";
	popUp(url); 
}

function dataUrl(url, param,tag){
	$.ajax( {
		"dataType":'json', 
		"type": "POST",
		"async": false, 
		"url": url,
		"data": param, 
		"success": function(data){	
			 $.each(data, function(index, array){
                 setVal(tag, array);
			 });				 
		}
	});
}

function msgError(msg, p){
	if(typeof p=="undefined") p = "center";
	if(msg!="undefined"){
      $.gritter.add({
        title: msg,
        text: ' ',
         class_name: 'danger',
        position: p
      });
	}
}

function msgSuccess(msg, p){
	if(typeof p=="undefined") p = "center";
	if(msg!="undefined"){
      $.gritter.add({
        title: msg,
        text: ' ',
        class_name: 'success',
        position: p
      });
	}
}
function strTrim(str) {
    return str.replace(/^\s+|\s+$/g, '');
}

function requestDetailData(table, retFunc, ckBox){
	var tmp = table.split(' ');
	var str = "search-member.php?table=";
	for(var i in tmp){
		str = str+tmp[i]+"&";
   }
	var url = str.substring(0, str.length-1);
	if(typeof retFunc!="undefined" && retFunc!=""){
	   url = url+"&retFunc="+retFunc;
	}
	if(typeof ckBox!="undefined")
		url = url+"&ckBox=T";
	popUp(url, 900); 
}
function courseData(table, retFunc, ckBox){
	var tmp = table.split(' ');
	var str = "search-course.php?table=";
	for(var i in tmp){
		str = str+tmp[i]+"&";
   }
	var url = str.substring(0, str.length-1);
	if(typeof retFunc!="undefined" && retFunc!=""){
	   url = url+"&retFunc="+retFunc;
	}
	if(typeof ckBox!="undefined")
		url = url+"&ckBox=T";
	popUp(url, 900); 
}
