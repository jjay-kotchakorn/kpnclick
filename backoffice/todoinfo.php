<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
global $db;
$todo_id = $_GET["todo_id"];
$todostatus = datatype(" and a.active='T'", "todostatus", true);
$company = datatype(" and a.active='T'", "company", true);
$q = "select  name, assign_to_id, company_id from todo where todo_id=$todo_id";
$r = $db->rows($q);
$str = "";

if($r){
	$str = $r["name"];
	$assign_to_id = $r["assign_to_id"];
	$company_id = $r["company_id"];
}else{
	$str = "Add New ".$type_name;
}
?>
<link rel="stylesheet" type="text/css" href="js/bootstrap.summernote/dist/summernote.css" />
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
				  <div class="col-md-12">			      
					<div class="block-flat">
					  <div class="header">							
					  	<ol class="breadcrumb">
							<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">Home</a></li>
							<li class="active"><?php echo $str; ?></li>
						</ol>
					  </div>
					  <div class="content">
						  <form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="update-todo.php">
						  <input type="hidden" name="todo_id" id="todo_id" value="<?php echo $todo_id; ?>">
						    <input type="hidden" name="tmpimg" />
  							<input type="hidden" name="delimg" />
						  <div class="col-sm-12">
							  <div class="form-group row">
								  	<label class="col-sm-1 control-label">Symbol</label>
								  	<div class="col-sm-2">
								  		<input class="form-control" id="code" name="code"  value="<?php echo $code; ?>" required="" placeholder="Code" type="text">
								  	</div>
								<label class="col-sm-1 control-label">Company<span class="red">*</span></label>
								<div class="col-sm-2">
									<select name="company_id" id="company_id" class="select2" onchange="reCall();">
										<option value="">---- Select ----</option>
										<?php foreach ($company as $key => $value) {
											$id = $value['company_id'];
											$name = $value['name'];
											$s = '';
											if($company_id==$id){
												$s = "selected";
											}
											echo  "<option value='$id' {$s}>$name</option>";
										} ?>

									</select>
								</div>	
								  	<label class="col-sm-1 control-label"><?php echo $type_name; ?> Status<span class="red">*</span></label>
								  	<div class="col-sm-2">
								  		<select name="todostatus_id" id="todostatus_id" class="form-control required">
								  			<option value="">---- Select ----</option>
								  			<?php foreach ($todostatus as $key => $value) {
								  				$id = $value['todostatus_id'];
								  				$name = $value['name'];
								  				echo  "<option value='$id'>$name</option>";
								  			} ?>

								  		</select>
								  	</div>
								<label class="col-sm-1 control-label" >Assign To<span class="red">*</span></label>
								<div class="col-sm-2" id="load_box">
									<select name="assign_to_id" id="assign_to_id" class="select2">
										<option value="">---- เลือก ----</option>
										<?php 
										$q = "select * from emp where active='T'";
										$emp = $db->get($q);
										foreach ($emp as $key => $value) {
											$id = $value['emp_id'];
											$s = "";
											if($assign_to_id==$id){
												$s = "selected";
											}
											$name = $value['prefix'].$value["fname"]." ".$value["lname"];
											echo  "<option value='$id' {$s}>$name</option>";
										} ?>

									</select>
								</div> 
								</div>				                
								<div class="form-group row">	                			                
								<label class="col-sm-1 control-label">Title</label>
								<div class="col-sm-11">
								  <input class="form-control" name="name" id="name" required="" placeholder="title" type="text">
								</div>	

								</div>
								<div class="form-group row">		                			                

									<label class="col-sm-1 control-label" style="padding-right:2px;">Target Date</label>
									<div class="col-sm-3">
										<input class="form-control" name="target_date" id="target_date" placeholder="Target Date" type="text">
									</div> 
	                			                
									<label class="col-sm-1 control-label" style="padding-right:2px;">Created Date</label>
									<div class="col-sm-3">
										<input class="form-control" readonly="readonly" name="created_date" id="created_date" placeholder="Created Date" type="text">
									</div>		                			                
									<label class="col-sm-1 control-label" style="padding-right:2px;">Last Update</label>
									<div class="col-sm-3">
										<input class="form-control" readonly="readonly" name="rectime" id="rectime" placeholder="Last Update" type="text">
									</div>
							  </div>				                
							  <div class="form-group row">
							  <label class="col-sm-1 control-label">Remark</label>
							  <div class="clear"></div><br>
							  	<div class="col-sm-12">
							  		<textarea id="detail" class="summernote" name="detail" placeholder="Description"></textarea>
							  	</div>
							  </div>
						  </div>
						  <div class="clear"></div>
						  <div class="form-group row" style="padding-left:10px;">
								<div class="col-sm-12">
									<button type="button" class="btn btn-primary" onClick="ckSave()">Save changes</button>
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">Cancel</button>									
								</div>
						  </div>
						</form>
						
					  </div>
					</div>
					
				  </div>
				</div>

		</div>
	</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>

<script type="text/javascript" src="js/bootstrap.summernote/dist/summernote.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
   $("#frmMain").validate();
   var todo_id = "<?php echo $_GET["todo_id"]?>";
   if(todo_id) viewInfo(todo_id);
 	var nowDate = new Date();
	var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate()-7, 0, 0, 0, 0);	
   $("#target_date").datepicker({language:'th-th',format:'dd-mm-yyyy',startDate: today });
   $('#detail').summernote({
            height: 200,
            onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            }
        }); 
        function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);//You can append as many data as you want. Check mozilla docs for this
            $.ajax({
                data: data,
                type: "POST",
                url: 'data/upload-img.php',
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                	var url = "todoImg/"+url;
                    editor.insertImage(welEditable, url);
                }
            });
        }

        
 });
 function viewInfo(todo_id){
   if(typeof todo_id=="undefined") return;
   gettodoInfo(todo_id);
}

function gettodoInfo(id){
	if(typeof id=="undefined") return;
	var url = "data/todolist.php";
	var param = "todo_id="+id+"&single=T";
	dataUrl(url, param,"#frmMain");
	
}
function get_upload(id){
	if(typeof id=="undefined") return;
	var url = "todo-upload.php?todo_id="+id;
	popUp(url, 700, 450);
}

function reView(){
	reload_upload();
}

function delete_file(id){
	if(typeof id=="undefined") return;
		var t = confirm("ลบไฟล์นี้ ? ");
		if(!t) return;
		var url = "update-upload.php";
		var param = "todo_upload_id="+id;
		$.ajax( {
			"type": "POST",
			"async": false, 
			"url": url,
			"data": param, 
			"success": function(data){
			   reload_upload();
		 	}
		});
}
function reload_upload(){
		var url = "data/uploadlist.php";
		var param = "todo_id=<?php echo $todo_id; ?>";
		$.ajax( {
			"type": "POST",
			"async": false, 
			"url": url,
			"data": param, 
			"success": function(data){
			   $("#file_upload_list").html(data);
		 	}
		});
}

function ckSave(id){
  onCkForm("#frmMain");
 var tmp = $('#detail').code();
 $('#detail').val(tmp);
  $("#frmMain").submit();
}	
</script>