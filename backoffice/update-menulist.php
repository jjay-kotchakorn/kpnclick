<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include_once "./share/member.php";

if($_POST){
	$id = $_POST["menulist_id"];
	$db->begin();
    $args = array();
	if($_POST){
		$args = array();
		$args["table"] = "menulist";
		if($_POST["menulist_id"])
		   $args["id"] = $_POST["menulist_id"];
		$args["code"] = $_POST["code"];
		$args["name"] = $_POST["name"];
		$args["name_eng"] = $_POST["name_eng"];
		$args["url"] = $_POST["url"];
		$args["action"] = $_POST["action"];
		$args["menu_id"] = (int)$_POST["menu_id"];
		$args["useclass"] = $_POST["useclass"];
		$args["active"] = $_POST["active"];
		$args["recby_id"] = (int)$EMPID;
		$args["rectime"] = date("Y-m-d H:i:s");
	}
	$ret = $db->set($args);
	$id = $args["id"] ? $args["id"] : $ret;
	$db->commit();
	unset($_SESSION["menulistinfo"]);
}
$args = array();
$args["menulist_id"] = $id;
redirect_url($args);
?>
