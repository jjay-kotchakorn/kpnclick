<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include('share/class.upload.php');
global $db, $MEMBERID;
if (!empty($_FILES)) {
	// เริ่มต้นใช้งาน class.upload.php ด้วยการสร้าง instant จากคลาส
	$cart_no_file = new upload($_FILES['cart_no_file']) ; // $_FILES['image_name'] ชื่อของช่องที่ให้เลือกไฟล์เพื่ออัปโหลด 
	$receive_file = new upload($_FILES['receive_file']) ; // $_FILES['image_name'] ชื่อของช่องที่ให้เลือกไฟล์เพื่ออัปโหลด 
	
	//  ถ้าหากมีภาพถูกอัปโหลดมาจริง
	if ( $cart_no_file->uploaded ) { 
		// ย่อขนาดภาพให้เล็กลงหน่อย  โดยยึดขนาดภาพตามความกว้าง  ความสูงให้คำณวนอัตโนมัติ
		// ถ้าหากไม่ต้องการย่อขนาดภาพ ก็ลบ 3 บรรทัดด้านล่างทิ้งไปได้เลย
		$cart_no_file->file_src_name_body = "MEMBER-CART-{$MEMBERID}-".date("y-m-d");

		$cart_no_file->process( "member-uploads/" ); // เก็บภาพไว้ในโฟลเดอร์ที่ต้องการ  *** โฟลเดอร์ต้องมี permission 0777

		// ถ้าหากว่าการจัดเก็บรูปภาพไม่มีปัญหา  เก็บชื่อภาพไว้ในตัวแปร เพื่อเอาไปเก็บในฐานข้อมูลต่อไป
		if ( $cart_no_file->processed ) {

			$image_name_cart =  $cart_no_file->file_dst_name; // ชื่อไฟล์หลังกระบวนการเก็บ จะอยู่ที่ file_dst_name
			$cart_no_file->clean(); // คืนค่าหน่วยความจำ

		}// END if ( $upload_image->processed )

	}
	
	//  ถ้าหากมีภาพถูกอัปโหลดมาจริง
	if ( $receive_file->uploaded ) { 
		// ย่อขนาดภาพให้เล็กลงหน่อย  โดยยึดขนาดภาพตามความกว้าง  ความสูงให้คำณวนอัตโนมัติ
		// ถ้าหากไม่ต้องการย่อขนาดภาพ ก็ลบ 3 บรรทัดด้านล่างทิ้งไปได้เลย
		$receive_file->file_src_name_body =  "MEMBER-RECEIVE-{$MEMBERID}-".date("y-m-d");
		//$receive_file->image_resize         = true ; // อนุญาติให้ย่อภาพได้
		//$receive_file->image_x              = 150 ; // กำหนดความกว้างภาพเท่ากับ 400 pixel 
		//$receive_file->image_ratio_y        = true; // ให้คำณวนความสูงอัตโนมัติ

		$receive_file->process( "member-uploads/" ); // เก็บภาพไว้ในโฟลเดอร์ที่ต้องการ  *** โฟลเดอร์ต้องมี permission 0777

		// ถ้าหากว่าการจัดเก็บรูปภาพไม่มีปัญหา  เก็บชื่อภาพไว้ในตัวแปร เพื่อเอาไปเก็บในฐานข้อมูลต่อไป
		if ( $receive_file->processed ) {

			$image_name_receive =  $receive_file->file_dst_name; // ชื่อไฟล์หลังกระบวนการเก็บ จะอยู่ที่ file_dst_name
			$receive_file->clean(); // คืนค่าหน่วยความจำ

		}// END if ( $upload_image->processed )

	}
}
/*print_r($_FILES);
echo "<hr>";
print_r($_POST);
die();*/
global $EMPID;
if($_POST){
    if($_POST["del_receive_file_part"]=="T"){
	   if(file_exists($_POST["receive_file_part_tmp"]))
	      unlink($_POST["receive_file_part_tmp"]);
	   $_POST["receive_file_part_tmp"] = "";
    }
    if($_POST["del_cart_no_file_part"]=="T"){
	   if(file_exists($_POST["cart_no_file_part_tmp"]))
	      unlink($_POST["cart_no_file_part_tmp"]);
	   $_POST["cart_no_file_part_tmp"] = "";
    }
	$args = array();
	$args["table"] = "member";
	if($_POST["member_id"])
	   $args["id"] = $_POST["member_id"];
	$args["code"] = $_POST["code"];
	$args["prefix"] = $_POST["prefix"];
	$args["fname"] = $_POST["fname"];
	$args["lname"] = $_POST["lname"];
	$args["nickname"] = $_POST["nickname"];
	$args["cid"] = $_POST["cid"];
	$args["birthdate"] = ($_POST["birthdate"] ? thai_to_timestamp($_POST["birthdate"]) : "");
	$args["email"] = $_POST["email"];
	$args["address"] = $_POST["address"];
	$args["phone"] = $_POST["phone"];
	$args["line"] = $_POST["line"];
	$args["homephone"] = $_POST["homephone"];
	$args["guide"] = $_POST["guide"];
	$args["bank_no"] = $_POST["bank_no"];
	$args["bank_id"] = (int) $_POST["bank_id"];
	$args["educationlevel_id"] = (int) $_POST["educationlevel_id"];
	$args["university_id"] = (int) $_POST["university_id"];
	$args["active"] = ($_POST["active"]=="F") ? $_POST["active"] : "T";
	$args["recby_id"] = ($EMPID) ? $EMPID : (int) $member_id;
	$args["rectime"] = date("Y-m-d H:i:s");
	$args["receive_file_part"] = $image_name_receive ? "member-uploads/$image_name_receive" : $_POST["receive_file_part_tmp"];
	$args["cart_no_file_part"] = $image_name_cart ? "member-uploads/$image_name_cart" : $_POST["cart_no_file_part_tmp"];
/*	$args["img"] = $image_name ? "memberImg/$image_name" : $_POST["tmpimg"];*/
   $ret = $db->set($args);
   $member_id = $args["id"] ? $args["id"] : $ret;
	if($member_id){
		$args = array();
		$args["table"] = "login";
		$q = "select login_id from login where member_id=$member_id order by login_id desc";
		$login_id = $db->data($q);
		if($login_id)
			$args["id"] = $login_id;
		else
			$args["member_id"] = $member_id;
		$args["username"] = $_POST["username"];
		$args["password"] = $_POST["password"];
		$args["righttype_id"] = 3;
		$args["active"] = ($_POST["activelogin"]) ? $_POST["activelogin"] : "T";
		$args["recby_id"] = (int)$member_id;
	    $args["rectime"] = date("Y-m-d H:i:s");
		$db->set($args);
	}

	if(is_array($_POST["ckbox"])){
		foreach($_POST["ckbox"] as $index=>$v){
			$args = array();
			$args["table"] = "work_time";
			if($_POST["work_time_id"][$index])
				$args["id"] = $_POST["work_time_id"][$index];
			$args["member_id"] = (int)$_POST["member_id"];
			$args["work_time_type"] = (int)$work_time_type;
			$args["time_job"] = $_POST["time_job"][$index];
			$args["remark"] = $_POST["remark"][$index];
			$args["date_job_start"] = ($_POST["date_job_start"][$index] ? thai_to_timestamp($_POST["date_job_start"][$index]) : "");			
			$args["date_job_stop"] = ($_POST["date_job_stop"][$index] ? thai_to_timestamp($_POST["date_job_stop"][$index]) : "");			
			$args["recby_id"] = (int)$EMPID;
			$args["rectime"] = date("Y-m-d H:i:s");
			if(!$args["id"] && $v=="F") continue;
			if($args["id"] && $v=="F"){
				$id = $args["id"];
				$q = "select work_time_id from work_time where work_time_id=$id";
				$testId = $db->data($q);
				if($testId){
					$db->query("delete from work_time where work_time_id=$testId");
					continue;
				}
			}
			$db->set($args);
		}	
	}

}
$args = array();
$args["p"] = "member";
$args["member_id"] = $member_id;
$args["type"] = "info";
redirect_url($args);
?>