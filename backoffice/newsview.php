<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
global $db;
$news_id = $_GET["news_id"];
$newstype = datatype(" and a.active='T'", "newstype", true);
$q = "select  *  from news where news_id=$news_id";
$r = $db->rows($q);
$str = "";
if($r){
	$str = $r["name"];
}else{
	$str = "เพิ่มคลังความรู้";
}

?>
<link rel="stylesheet" type="text/css" href="js/bootstrap.summernote/dist/summernote.css" />
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
				  <div class="col-md-12">			      
					<div class="block-flat">
					  <div class="header">							
					  	<ol class="breadcrumb">
							<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
							<li class="active"><?php echo $str; ?></li>
						</ol>
					  </div>
					  <div class="content">
						  <form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="update-news.php">
						  <input type="hidden" name="news_id" id="news_id" value="<?php echo $news_id; ?>">
						    <input type="hidden" name="tmpimg" />
  							<input type="hidden" name="delimg" />
						  <div class="col-sm-12">
							  <div class="form-group row">
								<label class="col-sm-2  control-label" style="text-align:left;">รหัส : <?php echo $r["code"]; ?></label>

								<label class="col-sm-4 control-label" style="text-align:left;">หมวดหมู่ข่าวสาร : <?php echo $newstype[$r["newstype_id"]]["name"]; ?></label>
								</div>				                
								<div class="form-group row">	                			                
								<label class="col-sm-12 control-label" style="text-align:left;">หัวข้อ : <?php echo $r["name"]; ?></label>
	                			                
							  </div>				                
							  <div class="form-group row">							  							 
							  	<div class="col-sm-12">
							  		<?php echo htmlspecialchars_decode($r["detail"]); ?>
							  	</div>
							  </div>
							  <div class="form-group row">
							  	<div class="col-sm-12">
							  	
											<?php 

											$q = "select a.name, a.url, a.news_upload_id from news_upload a where a.news_id=".$news_id;
										$r = $db->get($q);
										if($r){ 
											$runing = 1;
											?>
											<div class="table-responsive">
												<table class="table no-border hover">
													<thead class="no-border">
														<tr>
															<th style="width:10%;">ลำดับ</th>
															<th style="width:90%;"><strong>ชื่อไฟล์</strong></th>															
														</tr>
													</thead>
													<tbody class="no-border-y">
													<?php foreach ($r as $k => $v) { ?>
													<tr>
														<td ><?php echo $runing; ?></td>
														<td ><a target="_black" href="<?php echo $v["url"].$v["name"]; ?>"><strong><?php echo $v["name"]; ?></strong></a> </td>														
													</tr>				

													<?php
														$runing++;
													 }	?>
													</tbody>
												</table>		
											</div>
										<?php } ?>						
									</div>							  		
							  	</div>
							  </div>

						  </div>
						  <div class="clear"></div>
						  <div class="form-group row" style="padding-left:10px;">
								<div class="col-sm-12">									
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">กลับหน้าหลัก</button>									
								</div>
						  </div>
						</form>
						
					  </div>
					</div>
					
				  </div>
				</div>

		</div>
	</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>
