<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;


$department = datatype(" and a.active='T'", "department", true);
$membertype = datatype(" and a.active='T'", "membertype", true);
$requeststatus = datatype(" and a.active='T'", "requeststatus", true);
$type = $_GET["type"];
$str = "";
$status = "visibility:hidden";
$fix_status = "";
if($type=="request"){
	$str = "ข้อมูลขอใช้บริการ";
	$fix_status = 1;
}elseif($type=="ret"){
	$str = "ข้อมูลการยืม";
	$fix_status = 2;
}else if($type=="history"){
	$str = "ประวัติการยืม-คืนครุภัณฑ์";
	$status = "";
}else{
	$str = "รายงานการยืม-คืนครุภัณฑ์";
	$status = "";
}

?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
					<div class="page-head">						
						<h3><i class="fa fa-list"></i> &nbsp; <?php echo $str; ?></h3>
					</div>
						<div class="header">

							<div class="form-group row">             
								<label class="col-sm-2 control-label">ประเภทสมาชิก<span class="red">*</span></label>
								<div class="col-sm-2">
									<select name="membertype_id" id="membertype_id" class="select2" onchange="reCall();">
										<option value="">---- เลือก ----</option>
										<?php foreach ($membertype as $key => $value) {
											$id = $value['membertype_id'];
											$name = $value['name'];
											echo  "<option value='$id'>$name</option>";
										} ?>

									</select>
								</div>             
								<label class="col-sm-2 control-label">ภาควิชา/หน่วยงาน<span class="red">*</span></label>
								<div class="col-sm-2">
									<select name="department_id" id="department_id" class="select2" onchange="reCall();">
										<option value="">---- เลือก ----</option>
										<?php foreach ($department as $key => $value) {
											$id = $value['department_id'];
											$name = $value['name'];
											echo  "<option value='$id'>$name</option>";
										} ?>

									</select>
								</div>
								<label class="col-sm-1 control-label" style="<?php echo $status; ?>">สถานะ</label>
								<div class="col-sm-2">
									<select name="requeststatus_id" id="requeststatus_id" class="select2" onchange="reCall();" style="<?php echo $status; ?>">
										<option value="">---- เลือก ----</option>
										<?php foreach ($requeststatus as $key => $value) {
											$id = $value['requeststatus_id'];
												$s = "";
												if($fix_status==$id){
													$s = "selected";
												}
												$name = $value['name'];
												echo  "<option value='$id' {$s}>$name</option>";
										} ?>

									</select>
								</div>                                           
							</div>
							<div class="form-group row">
								<label class="col-sm-2 control-label">วันที่เอกสาร  <span class="red">*</span></label>
								<div class="col-sm-2">
									<input class="form-control" name="date_start" id="date_start" onblur="reCall();" placeholder="วันที่" type="text">
								</div>                                          
								<label class="col-sm-2 control-label">ถึงวันที่  <span class="red">*</span></label>
								<div class="col-sm-2">
									<input class="form-control" name="date_stop" id="date_stop" onblur="reCall();" placeholder="ถึงวันที่" type="text">
								</div>
								<label class="col-sm-1 control-label"> <a href="#" class="btn btn-rad btn-info" onClick="reCall();"><i class="fa fa-search"></i></a></label>   
							</div>  
						</div>
						<br>
					<table id="tbRequest" class="table" style="width:100%">
						  <thead>
							  <tr>
								  <th width="5%">ลำดับ</th>
								  <th width="5%">เลขที่เอกสาร</th>
								  <th width="10%">วันที่เอกสาร</th>
								  <th width="14%">ชื่อกิจกรรม / โครงการ</th>
								  <th width="10%">ผู้ขอใช้บริการ</th>
								  <th width="10%">ภาควิชา/หน่วยงาน</th>
								  <th width="10%">สถานที่</th>
								  <th width="10%">รายละเอียด</th>
								  <th width="10%">วันที่/เวลา</th>
								  <th width="8%">สถานะ</th>
								  <th width="8%">ครุภัณฑ์</th>
							  </tr>
						  </thead>   
						<tbody>
						</tbody>
					</table>
					<div class="clear"></div>
				</div>
			</div>

		</div>
	</div> 
</div>

<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
$(document).ready(function() {
	var get_type = "<?php echo $_GET["type"]; ?>";
    $("#date_start").datepicker({language:'th-th',format:'dd-mm-yyyy'});
    $("#date_stop").datepicker({language:'th-th',format:'dd-mm-yyyy'});  
	var oTable;
	listItem();	
});

function listItem(){
   var get_type = "<?php echo $_GET["type"]; ?>";
   var url = "data/request-report.php";
   oTable = $("#tbRequest").dataTable({
	   "sDom": 'T<"clear">lfrtip',
			"oLanguage": {
				"sInfoEmpty": "",
				"sInfoFiltered": "",
				"sSearch": "Search:"
			},
			"oTableTools": {
				"aButtons":  [	
				{
					"sExtends": "xls",
					"sButtonText": "Save for Excel"
				}
				]
			},
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({"name":"membertype_id","value":$("#membertype_id").val()});
			aoData.push({"name":"department_id","value":$("#department_id").val()});
			aoData.push({"name":"active","value":$("#requeststatus_id").val()});		
			aoData.push({"name":"type","value":get_type});
			aoData.push({"name":"date_start","value":$("#date_start").val()});			
			aoData.push({"name":"date_stop","value":$("#date_stop").val()});			
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
		}
   }); 
}

function editInfo(id){
	if(typeof id=="undefined") return;
	var url = "request-detail-report.php?request_id="+id;
	popUp(url, 870, 500);
}

function addnew(){
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=info";
   redirect(url);
}

function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}

function print_report(id){
	if(typeof id=="undefined") return;
   var url = "request-report.php?request_id="+id;
   popUp(url, 820, 500);
}


</script>