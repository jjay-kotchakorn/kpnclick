<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/datatype.php";
global $db;

$name = $db->escape(trim($_POST["name"]));
$code = $db->escape(trim($_POST["code"]));
$active = $_POST["active"];
$table = trim($_POST["table"]);
$id = $_POST["id"];
$othercon = "";
if($_POST["othercon"]){
    $a = explode("-", $_POST["othercon"]);
    $con_id = trim($a[1],",");
    if($con_id){
       $othercon = " and a.{$a[0]} in ($con_id)";
    }   
}

if($_POST["all"]=="T"){
   $con_name = $name ? " and a.name like '%$name%'" : "";
   $con_code = $code ? " and a.code like '%$code%'" : "";
}else{
    $con_name = $name ? " and a.name like '$name%'" : "";
    $con_code = $code ? " and a.code like '$code%'" : "";
}

$con_active = $active ? " and a.active='$active'" : " and a.active='T'";
$con = "$con_name $con_code $con_active $othercon";
$con_id = $id ? " and a.{$table}_id=$id" : "";
if($con_id) $con = "";

$array_data = array();
if($table){
    $q = "select a.{$table}_id as datatype_id, a.{$table}_id, a.code, a.name, a.name_eng, a.active, a.remark
                  ,a.recby_id, a.rectime
                  ,b.prefix+' '+b.fname+' '+b.lname as recby_name 
            from $table a left join emp b on a.recby_id=b.emp_id
            where a.active!='' $con $con_id
            order by a.code, a.{$table}_id
            limit 50";
            //echo $q;
    $r = $db->get($q);
    if(is_array($r)){
      foreach($r as $index=>$v){
        if(!$v["code"]) $v["code"] = "";
        $v["rectime"] = revert_date($v["rectime"], true);
        $array_data[] = $v;
      }
    }
}

echo json_encode($array_data);
?>
