<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include('../share/class.upload.php');
global $db;
if (!empty($_FILES)) {
	$upload_image = new upload($_FILES['file']) ;
	if ( $upload_image->uploaded ) { 
		$upload_image->file_src_name_body = "news-".time();
		$upload_image->image_resize         = true ; 
		$upload_image->image_x              = 640 ; 
		$upload_image->image_ratio_y        = true;

		$upload_image->process( "../newsImg/" ); 

		
		if ( $upload_image->processed ) {

			$image_name =  $upload_image->file_dst_name;
			$upload_image->clean(); 
		}

	}
}

echo $image_name;
?>