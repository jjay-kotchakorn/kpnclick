<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/project.php";
global $db, $RIGHTTYPEID, $EMPID;
$single_info = $_POST["single"];
if($single_info=="T"){
	$aData = array();
	$id = $_POST["project_id"];
	if($id){
	   $r = company_info("", $id);
	   foreach($r as $k=>$v){
		  if(!$v["map_img"])
		     $v["map_img"] = "images/no-avatar-male.jpg";
	      $aData[] = $v;
	   }  
	}
}else{
	$aColumns = array( 'a.project_id','a.code_project','a.name_th','a.name_eng','a.active');
	/* Indexed column (used for fast and accurate table cardinality) */
	$sIndexColumn = "projectId";

	function fnColumnToField( $i ){
		/* Note that column 0 is the details column */
		if ( $i == 0 ||$i == 1 )
			return "a.project_id";
		else if ( $i == 2 )
			return "a.code_project";
		else if ( $i == 3 )
			return "a.name_th";
		else if ( $i == 4 )
			return "a.name_eng";
		else if ( $i == 5 )
			return "a.active";
	}

	$sLimit = "";
	if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
	{
		$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
		$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
	}


	/* Ordering */
	if(isset($_POST['iSortCol_0'])){
		$sOrder = "ORDER BY  ";
		for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
			$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
			".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
		}
		$sOrder = substr_replace( $sOrder, "", -2 );
	}

	/* Filtering */
	$sWhere = "";
	if($_POST['sSearch'] != ""){
		$sWhere = "a.name_th LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
		"a.name_eng LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
		"b.name_type LIKE '%".$db->escape( $_POST['sSearch'] )."%'";
		$sAND = "AND ";
		$WHERE = "WHERE ";
	}
	/* Paging */
	$sQuery = "SELECT
				a.project_id,
				a.code_project,
				a.name_th,
				a.name_eng,
				a.detail,
				a.`status`,
				a.donation_min,
				a.donation_max,
				a.donation_min_receipt,
				a.active,
				b.`name` as name_type,
				a.projecttype_id
			FROM
				project a
			LEFT JOIN projecttype b ON a.projecttype_id = b.projecttype_id
				$WHERE $sWhere
				$sOrder
				$sLimit";
 	// echo $sQuery;die();
	$rResult = $db->get($sQuery);
	$a = array();
	if(is_array($rResult)){
		$runNo = 1;
		foreach ($rResult as $r){
			$manage = "";
			$id = $r["project_id"]; 
			//$manage = '<a class="btn btn-default" title="LINK ไปยังสำนักงาน ก.ล.ต." onClick="link_url('.$id.')"><i class="fa fa-link"></i> LINK</a>';
			$manage .=  get_datatable_icon("edit", $id);
			$active = ($r["active"]=="T") ? "เปิดใช้งาน" : "ปิด";   
			$a[] = array($runNo
				// ,$r['project_id']
				// ,$r['code_project']
				,$r['name_th']
				,$r["name_eng"]
				,$r["donation_min"]
				,$r["donation_max"]
				,$r["donation_min_receipt"]
				//$active
				// ,$r["email"]
				,$manage);
			$runNo++;
		}
	}

	$aData = array();
	$sQuery = "SELECT COUNT(*) as total
	FROM project
	$WHERE $sWhere";

	$rs = $db->data($sQuery);
	$iFilteredTotal = $rs;

	$sQuery = "SELECT COUNT(*) as total
	FROM project";
	$resultTotal = $db->data($sQuery);
	$iTotal = $resultTotal;

	$aData["sEcho"] = intval($_POST['sEcho']);
	$aData["iTotalRecords"] = $iTotal; 
	$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
	$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
