<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/request.php";
include_once "../share/item.php";
global $db;

$request_id = $_POST["request_id"];
$room_id = $_POST["room_id"];
$array_data = array();
if($request_id=="default"){
	$r = get_item(" and a.room_id=$room_id and a.active='T'");
	foreach($r as $k=>$v){
		$v["itemtype_name"] = ($v["itemtype_id"]) ? $v["itemtype_name"] : $v["itemtype_text"];
		$array_data[] = $v;

	} 
}else{
	$con = " and a.request_id={$request_id}";
    $r = get_request_detail($con);
	   foreach($r as $k=>$v){
	   		$v["itemtype_name"] = ($v["itemtype_id"]) ? $v["itemtype_name"] : $v["itemtype_text"];
	      $array_data[] = $v;
	   }      
}

echo json_encode($array_data); 
?>