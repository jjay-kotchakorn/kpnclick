<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/member.php";
global $db;

$id = $_POST["id"];
$con = "";
if($_POST["othercon"]){
    $a = explode("-", $_POST["othercon"]);
    $con_id = trim($a[1],",");
    if($con_id){
       $con .= " and a.{$a[0]} in ($con_id)";
    }   
}
$con .= ($_POST["department_id"]) ? " and a.department_id={$_POST["department_id"]}" : "";
$con .= ($_POST["membertype_id"]) ? " and a.membertype_id={$_POST["membertype_id"]}" : "";
$con .= ($_POST["active"]) ? " and a.active='{$_POST["active"]}'" : "";
$search = $db->escape(trim($_POST["search"]));
if($search){
    $con .=  " and (a.fname like '%$search%' or a.lname like '%$search%' or a.cid like '%$search%')";
}
$array_data = array();
if($con){
    $r = view_member($con, "", false, 50);
	   foreach($r as $k=>$v){
	      $array_data[] = $v;
	   }      
}

echo json_encode($array_data); ?>

