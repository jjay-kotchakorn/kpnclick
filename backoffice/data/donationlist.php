<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/register.php";
global $db, $RIGHTTYPEID, $EMPID;
//d($_POST);

$sProject = $_POST["sProject"];
$sBank = $_POST["sBank"];
$sStatus = $_POST["sStatus"];
$active = $_POST["active"];
$bill_request = $_POST["bill_request"];


$single_info = $_POST["single"];
if($single_info=="T"){
	$aData = array();
	$id = $_POST["payment_bank_register_list_id"];
	if($id){
	   $r = company_info("", $id);
	   foreach($r as $k=>$v){
		  if(!$v["map_img"])
		     $v["map_img"] = "images/no-avatar-male.jpg";
	      $aData[] = $v;
	   }  
	}
}else{
  $aColumns = array( 'payment_bank_register_list_id','code','name','invoice','remark');
/* Indexed column (used for fast and accurate table cardinality) */
//$sIndexColumn = "companyId";

function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if ( $i == 0 ||$i == 1 )
		return "payment_bank_register_list_id";
	else if ( $i == 2 )
		return "code";
	else if ( $i == 3 )
		return "name";
	else if ( $i == 4 )
		return "invoice";
	else if ( $i == 5 )
		return "remark";
}

$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}


/* Ordering */
$sOrder = "ORDER BY  ";
if(isset($_POST['iSortCol_0'])){
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
		$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
                ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
}else{
	$sOrder .= " e.order_expire_date DESC ";
}//end else
 
 /* Filtering */
$sWhere = "";
$WHERE = "WHERE e.active!=''";
if($_POST['sSearch'] != ""){
   $sWhere = "a.receipt_fname LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.receipt_lname LIKE '%".$db->escape( $_POST['sSearch'] )."%'";
	$sAND = "AND ";
	// $WHERE = "WHERE ";
}

/*dateStart*/ 
$dateStart = ($_POST["date_start"]) ? thai_to_timestamp($_POST["date_start"]) :  "";
$dateStop =  ($_POST["date_stop"]) ? thai_to_timestamp($_POST["date_stop"]) : "";
if ($dateStart || $dateStop) {
    if (!$dateStart && $dateStop)
        $dateStart = $dateStop;
    if (!$dateStop && $dateStart)
        $dateStop = $dateStart;
    $t = $dateStart;
    if ($dateStart > $dateStop) {
        $dateStart = $dateStop;
        $dateStop = $t;
    }
}
/*dateStop*/ 

/*dateStartResult*/ 
$dateStartResult = ($_POST["date_start_result"]) ? thai_to_timestamp($_POST["date_start_result"]) :  "";
$dateStopResult =  ($_POST["date_stop_result"]) ? thai_to_timestamp($_POST["date_stop_result"]) : "";
if ($dateStartResult || $dateStopResult) {
    if (!$dateStartResult && $dateStopResult)
        $dateStartResult = $dateStopResult;
    if (!$dateStopResult && $dateStartResult)
        $dateStopResult = $dateStartResult;
    $t = $dateStartResult;
    if ($dateStartResult > $dateStopResult) {
        $dateStartResult = $dateStopResult;
        $dateStopResult = $t;
    }
}
/*dateStopResult*/ 

$sWhere .= ($dateStart && $dateStop) ? " AND (e.donation_date>='$dateStart 00:00:00' AND e.donation_date<='$dateStop 23:59:59')" : "";
$sWhere .= ($dateStartResult && $dateStopResult) ? " AND (e.order_expire_date>='$dateStartResult 00:00:00' AND e.order_expire_date<='$dateStopResult 23:59:59')" : "";



$cond = "";
if ( !empty($sProject) ) {
	$cond .= " AND a.project_id={$sProject}";
}
if ( !empty($sBank) ) {
	$cond .= " AND e.payment_bank_id={$sBank}";
}
if ( !empty($sStatus) ) {
	$cond .= " AND g.pay_status_id={$sStatus}";
}
if ( !empty($active) ) {
	$cond .= " AND e.active='{$active}'";
}
if ( !empty($bill_request) ) {
	$cond .= " AND a.bill_request='{$bill_request}'";
}


/* Paging  payment_bank_register_list*/
$sQuery = "SELECT
			e.payment_bank_register_list_id,
			e.merchant_id,
			e.payment_bank_id,
			e.payment_options_id,
			e.`code`,
			e.register_id,
			e.invoice,
			e.ref1,
			e.ref2,
			e.active,
			e.recby_id,
			e.rectime,
			e.runyear,
			e.runno,
			e.doc_prefix,
			e.doc_no,
			e.amount AS resp_amount,
			e.donation_date,
			e.order_expire_date,
			e.remark,
			e.pay_status,
			e.resp_update_date,
			e.resp_code,
			e.resp_desc,
			e.resp_pay_status,
			e.resp_start_transaction_date,
			e.resp_pay_date,
			e.credit_card_no,
			e.purchase_amt,
			e.fee,
			a.register_id AS reg_id,
			a.`no`,
			a.member_id,
			a.title,
			a.fname,
			a.lname,
			a.telephone,
			a.bill_request,
			a.slip_type,
			a.receipttype_id,
			a.receipttype_text,
			a.receipt_id,
			a.taxno,
			a.slip_name,
			a.slip_address,
			a.date,
			a.register_by,
			a.ref1 AS reg_ref1,
			a.ref2 AS reg_ref2,
			a.`status`,
			a.expire_date,
			a.project_id,
			a.project_name,
			a.project_price,
			a.receipt_title,
			a.receipt_fname,
			a.receipt_lname,
			a.receipt_no,
			a.receipt_gno,
			a.receipt_moo,
			a.receipt_soi,
			a.receipt_road,
			a.receipt_district_id,
			a.receipt_amphur_id,
			a.receipt_province_id,
			a.receipt_postcode,
			a.receipt_date,
			a.amount,
			a.payment_bank_id,
			a.active AS reg_active ,
			a.recby_id,
			a.rectime,
			a.remark,
			a.pay_status AS reg_pay_status,
			a.runno,
			a.docno,
			a.runyear,
			a.auto_del,
			b.name AS receipt_district_name,
			c.name AS receipt_amphur_name,
			d.name AS receipt_province_name,
			f.name_th AS project_name_th,
            f.name_eng AS project_name_en, 
            g.name AS pay_status_name,
            h.name AS payment_bank_name,
            i.name AS receipt_status_name 
			FROM payment_bank_register_list e
			LEFT JOIN register a ON a.register_id=e.register_id
			LEFT JOIN district b ON b.district_id=a.receipt_district_id
			LEFT JOIN amphur c ON c.amphur_id=a.receipt_amphur_id
			LEFT JOIN province d ON d.province_id=a.receipt_province_id
			LEFT JOIN project AS f ON f.project_id=a.project_id 
			LEFT JOIN pay_status AS g ON g.pay_status_id=e.pay_status
			LEFT JOIN payment_bank AS h ON h.payment_bank_id=e.payment_bank_id  
			LEFT JOIN receipt_status AS i ON i.receipt_status_id=a.receipt_status_id  
		   $WHERE $sAND $sWhere $cond
		   $sOrder
		   $sLimit";
// echo $sQuery;die();
$rResult = $db->get($sQuery);
$a = array();
if(is_array($rResult)){
	$runNo = 1;
	foreach ($rResult as $r){
		$chk_box = "";
		$id = $r["payment_bank_register_list_id"]; 
		$btn_list = '<a class="btn btn-success" target="_blank" onClick="btn_update('.$r["payment_bank_register_list_id"].')">รายละเอียด</a>';
	  	$active = ($r["active"]=="T") ? "active" : "nonActive";   
	  	$bill_request = ($r["bill_request"]=="T") ? "ต้องการ" : "ไม่ต้องการ";   

	  	if ( $r["bill_request"]=="T" ) {	  		
	  		$chk_box .= '<input type="checkbox" name="ckbox['.$id.']" id="ckbox-'.$id.'" value="'.$id.'" style="float:right;"><input type="hidden" name="register_id[]" id="'.$id.'" value="'.$id.'">';
	  	}//end if

		$a[] = array($chk_box." ".$runNo
				      // ,$r['payment_bank_register_list_id']
				      ,$r['donation_date']
				      ,$r['order_expire_date']
				      ,$r['receipt_title']." ".$r['receipt_fname']." ".$r['receipt_lname']
				      ,$r['project_name_th']
				      ,number_format($r['amount'],2)
				      ,$r['resp_pay_date']
				      ,$r['payment_bank_name']
				      ,$r['pay_status_name']
				      //,$btn_list
				      ,$bill_request
				      ,$r['receipt_status_name']
				);
		$runNo++;
	}
}

$aData = array();
$sQuery = "SELECT COUNT(*) as total
			FROM payment_bank_register_list e
			LEFT JOIN register a ON a.register_id=e.register_id
			LEFT JOIN district b ON b.district_id=a.receipt_district_id
			LEFT JOIN amphur c ON c.amphur_id=a.receipt_amphur_id
			LEFT JOIN province d ON d.province_id=a.receipt_province_id
			LEFT JOIN project AS f ON f.project_id=a.project_id 
			LEFT JOIN pay_status AS g ON g.pay_status_id=e.pay_status
			LEFT JOIN payment_bank AS h ON h.payment_bank_id=e.payment_bank_id
			$WHERE $sAND $sWhere $cond
";

$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM payment_bank_register_list";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
