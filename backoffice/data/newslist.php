<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/news.php";
global $db,$RIGHTTYPEID;

$single_info = $_POST["single"];
if($single_info=="T"){
	$aData = array();
	$id = $_POST["news_id"];
	if($id){
	   $r = view_news("", $id);
	   foreach($r as $k=>$v){
	   	  	if($v["target_date"]!='0000-00-00 00:00:00'){
				$v["target_date"] = revert_date($v["target_date"]);
			}else{
				$v["target_date"] = "";
			}
	   	  	if($v["created_date"]!='0000-00-00 00:00:00'){
				$v["created_date"] = revert_date($v["created_date"]);
			}else{
				$v["created_date"] = "";
			}
	      $aData[] = $v;
	   }  
	}
}else{

function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if ( $i == 0 ||$i == 1 )
		return "a.news_id";
	else if ( $i == 2 )
		return "a.code";
	else if ( $i == 3 )
		return "a.name";
	else if ( $i == 4 )
		return "a.newstype_id";
	else if ( $i == 5 )
		return "a.highlight";
}

$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}


/* Ordering */
if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
		$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
                ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
}
 
/* Filtering */
  $sWhere = "";
  $WHERE = "WHERE a.active!='' ";
  $sAND = "";
if($_POST['sSearch'] != ""){
   $sWhere = "a.code LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.name LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".			    
			    "a.highlight LIKE '%".$db->escape( $_POST['sSearch'] )."%'";
	$sAND = "AND ";
}
$sWhere .= ($_POST["newstype_id"]) ? " and a.newstype_id={$_POST["newstype_id"]}" : "";
$sWhere .= ($_POST["newsstatus_id"]) ? " and a.newsstatus_id={$_POST["newsstatus_id"]}" : "";
$sWhere .= ($_POST["assign_to_id"]) ? " and a.assign_to_id={$_POST["assign_to_id"]}" : "";
$sWhere .= ($_POST["company_id"]) ? " and a.company_id={$_POST["company_id"]}" : "";
$sWhere .= ($RIGHTTYPEID==3) ? " and a.active='T'" : " and a.active='T'";
$sWhere .= ($_POST["quotation_no"])? " and a.quotation_no='{$_POST["quotation_no"]}'" : "";

$target_date = ($_POST["target_date"]) ? thai_to_timestamp($_POST["target_date"]) :  "";
$created_date =  ($_POST["created_date"]) ? thai_to_timestamp($_POST["created_date"]) : "";
$sWhere .= ($target_date) ? " and a.target_date>='$target_date 00:00:00' and a.target_date<='$target_date 23:59:59'" : "";
$sWhere .= ($created_date) ? " and a.created_date>='$created_date 00:00:00' and a.created_date<='$created_date 23:59:59'" : "";
/* Paging */
$sQuery = "SELECT a.news_id, a.code, a.name, a.quotation_no, a.target_date, a.created_date, a.highlight, b.name as company_name, a.active, a.link_to,
				  d.name as newsstatus_name, c.prefix, c.fname, c.lname, a.newstype_id, a.company_id
           FROM news a left join company b on b.company_id=a.company_id
           left join emp c on c.emp_id=a.assign_to_id
           left join newsstatus d on d.newsstatus_id=a.newsstatus_id
		   $WHERE $sAND $sWhere
		   $sOrder
		   $sLimit";
/*echo $sQuery;*/
$rResult = $db->get($sQuery);
$a = array();
if(is_array($rResult)){
	$runNo = 1;
	foreach ($rResult as $r){
		$id = $r["news_id"]; 
	  if($RIGHTTYPEID==1 || $RIGHTTYPEID==2)
	  $manage =  get_datatable_icon("edit", $id);
	else
	  $manage =  get_datatable_icon("view", $id);
	  $active = ($r["active"]=="T") ? "active" : "nonActive";
	  $url = "index.php?p=company&type=newsinfo&company_id=".$r["company_id"]."&news_id=".$r["news_id"]."&newstype=".$r["newstype_id"];
	  $button = '<a class="btn btn-success" href="'.$url.'" target="_blank"><i class="fa fa-search"></i> View </a>';

	  $t = $r["newstype_id"];
	  if($t==2){
		$a[] = array($runNo
				      ,$r['code']
				      ,$r['name']
				      ,$r['quotation_no']
				      ,$r['prefix']." ".$r["fname"]." ".$r["lname"]
				      ,revert_date($r["target_date"])
				      ,revert_date($r["created_date"])
				      ,$r['newsstatus_name']
				      ,$button);
	  }else{	  	
		$a[] = array($runNo
				      ,$r['code']
				      ,$r['name']
				      ,$r['prefix']." ".$r["fname"]." ".$r["lname"]
				      ,revert_date($r["target_date"])
				      ,revert_date($r["created_date"])
				      ,$r['newsstatus_name']
				      ,$button);
	  }
		$runNo++;
	}
}

$aData = array();
$sQuery = "SELECT COUNT(*) as total
			  FROM news a
			  $WHERE $sAND $sWhere";

$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM news a";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
