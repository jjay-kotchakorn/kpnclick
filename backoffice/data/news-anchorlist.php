<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/news-anchor.php";
global $db;

$single_info = $_POST["single"];
if($single_info=="T"){
	$aData = array();
	$id = $_POST["news_anchor_id"];
	if($id){
	   $r = view_news_anchor("", $id);
	   foreach($r as $k=>$v){
	      $aData[] = $v;
	   }  
	}
}else{

function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if ( $i == 0 ||$i == 1 )
		return "a.news_anchor_id";
	else if ( $i == 2 )
		return "a.code";
	else if ( $i == 3 )
		return "a.name";
	else if ( $i == 5 )
		return "a.highlight";
	else return "a.news_anchor_id";
}

$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}


/* Ordering */
if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
		$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
                ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
}
 
/* Filtering */
  $sWhere = "";
  $WHERE = "WHERE a.active!='' ";
  $sAND = "";
if($_POST['sSearch'] != ""){
   $sWhere = "a.code LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.name LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.highlight LIKE '%".$db->escape( $_POST['sSearch'] )."%'";
	$sAND = "AND ";
}
/* Paging */
$sQuery = "SELECT a.news_anchor_id, a.code, a.name, a.section_id, a.highlight, a.active
           FROM news_anchor a  
		   $WHERE $sAND $sWhere
		   $sOrder
		   $sLimit";

$rResult = $db->get($sQuery);
$a = array();
if(is_array($rResult)){
	$runNo = 1;
	foreach ($rResult as $r){
		$id = $r["news_anchor_id"]; 
	  $manage =  get_datatable_icon("edit", $id);
	  $active = ($r["active"]=="T") ? "active" : "nonActive";
	  if($_POST["view_news"]=="T"){	  	
		  $url = "index.php?p=news-anchor-vews&news_anchor_id=".$r["news_anchor_id"];
		  $manage = '<a class="btn btn-success" href="'.$url.'" target="_blank"><i class="fa fa-search"></i> View </a>'; 
			$a[] = array($runNo .'<i class="fa fa-tag"></i>'
		      ,$r['code']
		      ,$r['name']
		      ,$manage);
	  }else{
			$a[] = array($runNo
		      ,$r['code']
		      ,$r['name']
		      ,$r["highlight"]=="T" ? 'Enable' : 'Disable' 
		      ,$manage);		
	  	
	  }  
		$runNo++;
	}
}

$aData = array();
$sQuery = "SELECT COUNT(*) as total
			  FROM news_anchor a
			  $WHERE $sAND $sWhere";

$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM news_anchor a";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
