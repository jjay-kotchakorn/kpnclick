<?php
// include_once "../inc/header-bootstrap.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/register.php";
global $db, $EMPID;
set_time_limit(0);
error_reporting(0);
$datetime_now = date('Y-m-d H:i:s');
$date_now = date('Y-m-d');

// d($_POST); die();
// d($_SESSION); die();

$id = trim($_POST["id"], ",");
$receipt_status_id = $_POST["receipt_status_id"];
$emp_id = $_POST["emp_id"];

if ( empty($id) || empty($receipt_status_id) ) {
	$args = array();
	$args["status"] = "error";
	$args["datetime"] = $datetime_now;
	$args["title"] = "ได้รับข้อมูลไม่ครบ";
	$args["text"] = "กรุณาติดต่อผู้ดูแลระบบ";
	$args["remark"] = "";
	$args["debug"] = "";

	echo json_encode($args);
	die();
}else{

	$ids = explode(",", $id);
	$count_id = count($ids);
	$runno = 0;
	foreach ($ids as $key => $id) {
		$payment_bank_register_list_id = trim($id);

		$payment_bank_register_list = get_payment_bank_register_list("", $payment_bank_register_list_id);
		$payment_bank_register_list = $payment_bank_register_list[0];
		$payment_bank_register_list_recby_id = $payment_bank_register_list["recby_id"];
		$register_id = $payment_bank_register_list["register_id"];

		$register_info = get_register("", $register_id);
		$register_info = $register_info[0];
		$register_recby_id = $register_info["recby_id"];
		$receipt_status_id_old = $register_info["receipt_status_id"];

		$args = array();
		$args["table"] = "receipt_status_log";
		$args["payment_bank_register_list_id"] = $payment_bank_register_list_id;
		$args["register_id"] = $register_id;
		$args["receipt_status_id"] = $receipt_status_id_old;
		$args["register_recby_id"] = (int)$register_recby_id;
		$args["payment_bank_register_list_recby_id"] = (int)$payment_bank_register_list_recby_id;
		$args["serialize_payment_bank_register_list"] = serialize($payment_bank_register_list);
		$args["serialize_register"] = serialize($register_info);
		$args["recby_id"] = (int)$emp_id;

		// var_dump($db->set($args, true, true)); die();
		$receipt_status_log_id = $db->set($args);

		if ( !empty($receipt_status_log_id) ) {
			$args = array();
			$args["table"] = "register";
			$args["id"] = $register_id;		
			$args["receipt_status_id"] = (int)$receipt_status_id;
			$args["recby_id"] = (int)$emp_id;

			// var_dump($db->set($args, true, true)); die();
			$db->set($args);

			unset($payment_bank_register_list);
			unset($register_info);
			unset($args);

		}//end if

		// die();
	}//end loop $id

	$args = array();
	$args["status"] = "success";
	$args["datetime"] = $datetime_now;
	$args["title"] = "บันทึกข้อมูลเรียบร้อย";
	$args["text"] = "";
	$args["remark"] = "";
	$args["debug"] = $id;

	echo json_encode($args);
	die();
}//end else

?>