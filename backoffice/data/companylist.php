<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/company.php";
global $db, $RIGHTTYPEID, $EMPID;
$single_info = $_POST["single"];
if($single_info=="T"){
	$aData = array();
	$id = $_POST["company_id"];
	if($id){
	   $r = company_info("", $id);
	   foreach($r as $k=>$v){
		  if(!$v["map_img"])
		     $v["map_img"] = "images/no-avatar-male.jpg";
	      $aData[] = $v;
	   }  
	}
}else{
  $aColumns = array( 'company_id','code','name','address','website');
/* Indexed column (used for fast and accurate table cardinality) */
$sIndexColumn = "companyId";

function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if ( $i == 0 ||$i == 1 )
		return "company_id";
	else if ( $i == 2 )
		return "code";
	else if ( $i == 3 )
		return "name";
	else if ( $i == 4 )
		return "address";
	else if ( $i == 5 )
		return "website";
}

$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}


/* Ordering */
if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
		$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
                ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
}else{
	$sOrder = "ORDER BY name ASC";
}
 
 $curent_id = 0;
/* Filtering */
$sWhere = "";
$WHERE = " WHERE active='T' ";
 if($RIGHTTYPEID!=1){
 	$q = "select org_name from emp where emp_id=$EMPID";

 	$org_name = $db->data($q);
 	if($org_name!=""){ 		
	 	$q = "select company_id from company where name like '%$org_name%'";
	 	$curent_id = $db->data($q);
 	}
 }
if($_POST['sSearch'] != ""){
   $sWhere .= " AND (name LIKE '%".$db->escape( $_POST['sSearch'] )."%' )";
	$sAND = "AND ";
	
}
/* Paging */
$sQuery = "SELECT company_id, code, name, address, website, phone, email
           FROM company
		   $WHERE $sWhere
		   $sOrder
		   $sLimit";

$rResult = $db->get($sQuery);
$a = array();
if(is_array($rResult)){
	$runNo = 1;
	foreach ($rResult as $r){
		$id = $r["company_id"]; 
		if($RIGHTTYPEID!=1 && $RIGHTTYPEID!=4){
			if($RIGHTTYPEID==3){
				if($curent_id==$id)
		  			$manage =  get_datatable_icon("view", $id);
			}else{
		  		$manage =  get_datatable_icon("view", $id);
			}
		}else{
		  $manage =  get_datatable_icon("edit", $id);
		  $manage .=  get_datatable_icon("close", $id, false);
		}
	  $active = ($r["active"]=="T") ? "active" : "nonActive"; 
	  
		$a[] = array($runNo
				      ,$r['name']
				      ,$r['website']
				      ,$manage);
		$runNo++;
	}
}

$aData = array();
$sQuery = "SELECT COUNT(*) as total
			  FROM company
			  $WHERE $sWhere";

$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM company";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
