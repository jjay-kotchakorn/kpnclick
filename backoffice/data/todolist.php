<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/todo.php";
global $db,$RIGHTTYPEID;

$single_info = $_POST["single"];
if($single_info=="T"){
	$aData = array();
	$id = $_POST["todo_id"];
	if($id){
	   $r = view_todo("", $id);
	   foreach($r as $k=>$v){
	   	  	if($v["target_date"]!='0000-00-00 00:00:00'){
				$v["target_date"] = revert_date($v["target_date"]);
			}else{
				$v["target_date"] = "";
			}
	   	  	if($v["created_date"]!='0000-00-00 00:00:00'){
				$v["created_date"] = revert_date($v["created_date"]);
			}else{
				$v["created_date"] = "";
			}
	      $aData[] = $v;
	   }  
	}
}else{

function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if ( $i == 0 ||$i == 1 )
		return "a.todo_id";
	else if ( $i == 2 )
		return "a.code";
	else if ( $i == 3 )
		return "a.name";
	else if ( $i == 4 )
		return "a.target_date";
	else if ( $i == 5 )
		return "a.created_date";
}

$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}


/* Ordering */
if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
		$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
                ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
}
 
/* Filtering */
  $sWhere = "";
  $WHERE = "WHERE a.active!='' ";
  $sAND = "";
if($_POST['sSearch'] != ""){
   $sWhere = "a.code LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.name LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".			    
			    "a.highlight LIKE '%".$db->escape( $_POST['sSearch'] )."%'";
	$sAND = "AND ";
}

$sWhere .= ($_POST["todostatus_id"]) ? " and a.todostatus_id={$_POST["todostatus_id"]}" : "";
$sWhere .= ($_POST["assign_to_id"]) ? " and a.assign_to_id={$_POST["assign_to_id"]}" : "";
$sWhere .= ($_POST["company_id"]) ? " and a.company_id={$_POST["company_id"]}" : "";
$sWhere .= ($RIGHTTYPEID==3) ? " and a.active='T'" : " and a.active='T'";


$target_date = ($_POST["target_date"]) ? thai_to_timestamp($_POST["target_date"]) :  "";
$created_date =  ($_POST["created_date"]) ? thai_to_timestamp($_POST["created_date"]) : "";
$sWhere .= ($target_date) ? " and a.target_date>='$target_date 00:00:00' and a.target_date<='$target_date 23:59:59'" : "";
$sWhere .= ($created_date) ? " and a.created_date>='$created_date 00:00:00' and a.created_date<='$created_date 23:59:59'" : "";
/* Paging */
$sQuery = "SELECT a.todo_id, a.code, a.name, a.target_date, a.created_date, b.name as company_name, a.active,
				  d.name as todostatus_name, c.prefix, c.fname, c.lname, a.company_id
           FROM todo a left join company b on b.company_id=a.company_id
           left join emp c on c.emp_id=a.assign_to_id
           left join todostatus d on d.todostatus_id=a.todostatus_id
		   $WHERE $sAND $sWhere
		   $sOrder
		   $sLimit";

$rResult = $db->get($sQuery);
$a = array();
if(is_array($rResult)){
	$runNo = 1;
	foreach ($rResult as $r){
		$id = $r["todo_id"]; 
	  if($RIGHTTYPEID==1 || $RIGHTTYPEID==2)
	  $manage =  get_datatable_icon("edit", $id);
	else
	  //$manage =  get_datatable_icon("view", $id);
	  $active = ($r["active"]=="T") ? "active" : "nonActive";
 	  $manage =  get_datatable_icon("edit", $id);
 	
		$a[] = array($runNo
				      ,$r['code']
				      ,$r['name']
				      ,$r['prefix']." ".$r["fname"]." ".$r["lname"]
				      ,revert_date($r["target_date"])
				      ,revert_date($r["created_date"])
				      ,$r['todostatus_name']
				      ,$manage);
	
		$runNo++;
	}
}

$aData = array();
$sQuery = "SELECT COUNT(*) as total
			  FROM todo a
			  $WHERE $sAND $sWhere";

$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM todo a";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
