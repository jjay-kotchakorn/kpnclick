<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/company.php";
global $db, $RIGHTTYPEID, $EMPID;
$single_info = $_POST["single"];
if($single_info=="T"){
	$aData = array();
	$id = $_POST["bank_id"];
	if($id){
	   $r = company_info("", $id);
	   foreach($r as $k=>$v){
		  if(!$v["map_img"])
		     $v["map_img"] = "images/no-avatar-male.jpg";
	      $aData[] = $v;
	   }  
	}
}else{
  $aColumns = array( 'bank_id','code','name','name_eng','remark');
/* Indexed column (used for fast and accurate table cardinality) */
$sIndexColumn = "companyId";

function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if ( $i == 0 ||$i == 1 )
		return "bank_id";
	else if ( $i == 2 )
		return "code";
	else if ( $i == 3 )
		return "name";
	else if ( $i == 4 )
		return "name_eng";
	else if ( $i == 5 )
		return "remark";
}

$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}


/* Ordering */
if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
		$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
                ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
}
 
 /* Filtering */
$sWhere = "";
if($_POST['sSearch'] != ""){
   $sWhere = "bank_id LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "name LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "name_eng LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "active LIKE '%".$db->escape( $_POST['sSearch'] )."%'";
	$sAND = "AND ";
	$WHERE = "WHERE ";
}
/* Paging */
$sQuery = "SELECT
				payment_bank_id,
				`code`,
				code_id,
				bank_id,
				`name`,
				name_eng,
				expire_day,
				image_url,
				file_name,
				active,
				recby_id,
				rectime,
				remark,
				inquiry
			FROM payment_bank
		   $WHERE $sWhere
		   $sOrder
		   $sLimit";
// echo $sQuery;die();
$rResult = $db->get($sQuery);
$a = array();
if(is_array($rResult)){
	$runNo = 1;
	foreach ($rResult as $r){
		$btn = "";
		$id = $r["payment_bank_id"]; 

		$btn_update = "";
		if ( $r["inquiry"]=='T' ) {
	 		$btn_update .= '<a class="btn btn-warning" onClick="btn_update('.$id.')"><i class="fa fa-refresh"></i>อัพเดตรายการ</a>';
		}//end if
	  	$btn_list =  '<a class="btn btn-primary" onClick="btn_list('.$id.')">รายการบริจาค</a>';

	  	$btn_edit = get_datatable_icon("edit", $id); 

	  	$active = ($r["active"]=="T") ? "เปิดใช้งาน" : "ปิด";   
	  	$btn .=  $btn_edit." ".$btn_update;
		$a[] = array($runNo
				      ,$r['name']
				      ,$r['expire_day']
				      ,$active
				      ,$btn
				      ,$r['remark']
				);
		$runNo++;
	}
}

$aData = array();
$sQuery = "SELECT COUNT(*) as total
			  FROM payment_bank
			  $WHERE $sWhere";

$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM payment_bank";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
