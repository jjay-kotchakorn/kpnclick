<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/menu.php";
global $db;

$righttype_id = $_POST["righttype_id"];
$type = $_POST["type"];
$array_data = array();
if($righttype_id && $type){
   $array_data = get_menuvisible($righttype_id, $type);
}

echo json_encode($array_data); ?>

