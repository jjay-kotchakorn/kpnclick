<?php
// include_once "../inc/header-bootstrap.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/register.php";
include_once "../pay-gate/lib/config_kbank.php";
global $db;
set_time_limit(0);
// error_reporting(0);
$datetime_now = date('Y-m-d H:i:s');
$date_now = date('Y-m-d');

// d($_POST); die();

$debug = true;
// $debug = false;

if ( $debug ) {
	$start_date = "2018-02-19";
	$stop_date = "2018-02-19";

	// echo json_encode(array("percent" => 100, "message" => 'กำลังอัพเดทข้อมูล..'));
	// die();
}else{
	$type = $_POST["type"];
	$start_date = $_POST["date_start"];
	$stop_date = $_POST["date_stop"];
}//end else



$cond = "";
if ( (!empty($_POST) && $type=="update_donation_register") || $debug ) {
	$cond = " AND a.active='T' AND a.donation_date BETWEEN '{$start_date} 00:00:00' AND '{$stop_date} 23:59:59'";
}//end if
// echo  $cond;

$rs = get_payment_bank_register($cond);

$count_all = count($rs);
// d($rs); 
// die();

if ( !empty($rs) ) {
	$runno = 0;
	$percent = 0;
	$txt = "";

	write_log('update-donation-register.tmp', "kplus=".$percent, $write_mode="w", $dir_log="./tmp/");

	foreach ($rs as $key => $v) {
		$runno++;
		$payment_bank_register_list_id = $v["payment_bank_register_list_id"];
		$payment_bank_id = $v["payment_bank_id"];
		$invoice = $v["invoice"];
		$ref1 = $v["ref1"];
		$ref2 = $v["ref2"];
		$amount = $v["amount"];
		$donation_date = revert_date($v["donation_date"]);
		$tmp = explode("-", $donation_date);
		$donation_date = $tmp[0].$tmp[1].$tmp[2]-543;
		// echo "$donation_date"; die();

		$params = unserialize($v["params_serialize"]);
		// d($params);

		$args = array();
		$args["USERNAME"] = $USERNAME;
		$args["TMERCHANTID"] = $params["MERCHANT2"];
		$args["TDATE"] = $donation_date;
		$args["TINVOICE"] = $params["INVMERCHANT"];
		$args["TAMOUNT"] = $params["AMOUNT2"];
		$args["TREF1"] = $params["REF1"];
		$args["TREF2"] = $params["REF2"];
		// d($args);

		$params = "";
		foreach ($args as $key => $v) {
			 $params .= $key . '='.$v.'&'; 
		}//end loop $v

		$ch = curl_init(); 

		curl_setopt($ch,CURLOPT_URL,$url_for_inquiry);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$params);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch,CURLOPT_HEADER, false);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 60);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		$output=curl_exec($ch);
		$error_msg = curl_error($ch);
		$info = curl_getinfo($ch);

		curl_close($ch);

		if ( !empty($output) && empty($error_msg) ) {
			$invoice_no = substr($output, 32, 12);
			// echo "<br>invoice_no  ".$invoice_no;
			$start_transaction_date = substr($output, 44, 8);
			// echo "<br>start_transaction_date  ".$start_transaction_date;
			$start_transaction_time = substr($output, 52, 6);
			// echo "<br>start_transaction_time  ".$start_transaction_time;
			$mobile_no = substr($output, 58, 10);
			// echo "<br>mobile_no  ".$mobile_no;
			$end_transaction_date = substr($output, 68, 17);
			// echo "<br>end_transaction_date  ".$end_transaction_date;
			$transaction_amount = substr($output, 85, 12);
			// echo "<br>transaction_amount  ".$transaction_amount;
			$response_code = substr($output, 97, 2);
			$response_code = trim($response_code);
			// echo "<br>response_code  ".$response_code;
			// echo "<hr>";

			$start_transaction_date = trim($start_transaction_date);
			$start_transaction_dd = substr($start_transaction_date, 0, 2);
			$start_transaction_mm = substr($start_transaction_date, 2, 2);
			$start_transaction_yyyy = substr($start_transaction_date, 4, 4);
			$start_transaction_date = $start_transaction_yyyy."-".$start_transaction_mm."-".$start_transaction_dd;

			$start_transaction_time = trim($start_transaction_time);
			$start_transaction_hh = substr($start_transaction_time, 0, 2);
			$start_transaction_ii = substr($start_transaction_time, 2, 2);
			$start_transaction_ss = substr($start_transaction_time, 4, 2);
			$start_transaction_time = $start_transaction_hh.":".$start_transaction_ii.":".$start_transaction_ss;

			$start_transaction_date = $start_transaction_date." ".$start_transaction_time;
			// echo $start_transaction_date;
			// echo "<hr>";

			$end_transaction_date = trim($end_transaction_date);
			$tmp = explode(" ", $end_transaction_date);
			$end_transaction_date = $tmp[0];
			$end_transaction_time = $tmp[1];

			$end_transaction_yyyy = substr($end_transaction_date, 0, 4);
			$end_transaction_mm = substr($end_transaction_date, 4, 2);
			$end_transaction_dd = substr($end_transaction_date, 6, 2);
			$end_transaction_date = $end_transaction_yyyy."-".$end_transaction_mm."-".$end_transaction_dd;

			$end_transaction_date = $end_transaction_date." ".$end_transaction_time;
			// echo $end_transaction_date;
			// echo "<hr>";

			$transaction_amount_satang = substr($transaction_amount, -2);
			$transaction_amount_bath = (int)(substr($transaction_amount, 0, 10));
			$transaction_amount = $transaction_amount_bath.".".$transaction_amount_satang;

			// echo $transaction_amount;
			// echo "<hr>";

			$response_serialize = "";
			$resp = array();
			$resp["rectime"] = $datetime_now;
			$resp["json_send_params"] = json_encode($args);
			if ( is_array($output) ) {
				$resp["resp_data_type"] = "serialize";
				$resp["resp_data"] = serialize($output);
			}else{
				$resp["resp_data_type"] = "text";
				$resp["resp_data"] = $output;
			}//end else

			// d($resp);
			$response_serialize = serialize($resp);
			// d(unserialize($response_serialize));
			// die();

			$q = "SELECT map_pay_status_id,
					name 
				FROM payment_response_code 
				WHERE payment_bank_id={$payment_bank_id}
					AND code='{$response_code}'
			";
			$resp_info = $db->rows($q);
			$pay_status = $resp_info["map_pay_status_id"];
			$payment_response_name = $resp_info["name"];

			$args = array();
			$args["table"] = "payment_bank_register_list";
			$args["id"] = (int)$payment_bank_register_list_id;
			$args["pay_status"] = (int)$pay_status;
			$args["resp_code"] = $response_code;
			$args["resp_start_transaction_date"] = trim($start_transaction_date);
			$args["resp_pay_date"] = trim($end_transaction_date);
			$args["purchase_amt"] = $transaction_amount;
			$args["response_serialize"] = $response_serialize;
			$args["resp_update_date"] = $datetime_now;

			$db->set($args);
			// var_dump($db->set($args, true, true));

			$info = get_payment_bank_register("", $payment_bank_register_list_id);
			$info = $info[0];
			// d($info);

			$order_expire_date = $info["order_expire_date"];
			$resp_pay_date = $info["resp_pay_date"];
			$pay_status = $info["pay_status"];
			$register_id = $info["register_id"];

			if ( $pay_status!=1 ) {
				$args = array();
				$args["table"] = "register";
				$args["id"] = $register_id;
				$args["pay_status"] = $pay_status;
				$args["auto_del"] = 'F';
				$db->set($args);
			}//end fi

		}//end if

		$txt .= "{$runno}/{$count_all}|donation_no={$invoice_no}|donation_date={$donation_date}|donation_response={$payment_response_name}";
		// echo $txt."<hr>";
		$percent = (int)(($runno/$count_all)*100);
		// echo json_encode(array("percent" => $percent, "message" => 'กำลังอัพเดทข้อมูล..'));
		// die();
		
		write_log('update-donation-register.tmp', "kplus=".$percent, $write_mode="w", $dir_log="./tmp/");

	}//end loop $v
}//end if

// die();
/*
$args = array();
$args["USERNAME"] = $USERNAME;
$args["TMERCHANTID"] = $MERCHANT2;
$args["TDATE"] = "19022018";
$args["TINVOICE"] = "000000000002";
$args["TAMOUNT"] = "000000000050";
$args["TREF1"] = "20180219000000000008";
$args["TREF2"] = "";
*/

/*
$params = "";
$str_txt = '';
foreach ($args as $key => $v) {
	 $params .= $key . '='.$v.'&'; 
}//end loop $v

// echo $params; die();

$ch = curl_init(); 

curl_setopt($ch,CURLOPT_URL,$url_for_inquiry);

curl_setopt($ch,CURLOPT_POSTFIELDS,$params);

curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

curl_setopt($ch,CURLOPT_HEADER, false);

curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 60);

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);


$output=curl_exec($ch);
$error_msg = curl_error($ch);
$info = curl_getinfo($ch);  


// d($output);
d($error_msg);
// d($info);

curl_close($ch);


echo $output;

$invoice_no = substr($output, 32, 12);
echo "<br>invoice_no  ".$invoice_no;

$start_transaction_date = substr($output, 44, 8);
echo "<br>start_transaction_date  ".$start_transaction_date;

$start_transaction_time = substr($output, 52, 6);
echo "<br>start_transaction_time  ".$start_transaction_time;

$mobile_no = substr($output, 58, 10);
echo "<br>mobile_no  ".$mobile_no;

$end_transaction_date = substr($output, 68, 17);
echo "<br>end_transaction_date  ".$end_transaction_date;

$transaction_amount = substr($output, 85, 12);
echo "<br>transaction_amount  ".$transaction_amount;

$response_code = substr($output, 97, 2);
echo "<br>response_code  ".$response_code;
echo "<hr>";

$start_transaction_date = trim($start_transaction_date);
$start_transaction_dd = substr($start_transaction_date, 0, 2);
$start_transaction_mm = substr($start_transaction_date, 2, 2);
$start_transaction_yyyy = substr($start_transaction_date, 4, 4);
$start_transaction_date = $start_transaction_yyyy."-".$start_transaction_mm."-".$start_transaction_dd;

$start_transaction_time = trim($start_transaction_time);
$start_transaction_hh = substr($start_transaction_time, 0, 2);
$start_transaction_ii = substr($start_transaction_time, 2, 2);
$start_transaction_ss = substr($start_transaction_time, 4, 2);
$start_transaction_time = $start_transaction_hh.":".$start_transaction_ii.":".$start_transaction_ss;

$start_transaction_date = $start_transaction_date." ".$start_transaction_time;
echo $start_transaction_date;
echo "<hr>";

$end_transaction_date = trim($end_transaction_date);
$tmp = explode(" ", $end_transaction_date);
$end_transaction_date = $tmp[0];
$end_transaction_time = $tmp[1];

$end_transaction_yyyy = substr($end_transaction_date, 0, 4);
$end_transaction_mm = substr($end_transaction_date, 4, 2);
$end_transaction_dd = substr($end_transaction_date, 6, 2);
$end_transaction_date = $end_transaction_yyyy."-".$end_transaction_mm."-".$end_transaction_dd;

$end_transaction_date = $end_transaction_date." ".$end_transaction_time;
echo $end_transaction_date;
echo "<hr>";
*/

?>
