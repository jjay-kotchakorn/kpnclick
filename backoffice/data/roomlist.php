<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/room.php";
global $db;

$single_info = $_POST["single"];
if($single_info=="T"){
	$aData = array();
	$id = $_POST["room_id"];
	if($id){
	   $r = get_room("", $id);
	   foreach($r as $k=>$v){
	      $aData[] = $v;

	   }  
	}
}else{

function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if ( $i == 0 || $i==4)
		return "a.room_id";
	else if ( $i == 1 )
		return "a.code";
	else if ( $i == 2)
		return "a.name";
	else if ( $i == 3 )
		return "a.roomtype_id";
	else return "a.room_id";
}


$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}

/* Ordering */
if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
		$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
                ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
}

/* Filtering */
  $sWhere = "";
  $WHERE = "WHERE a.active!='' ";
  $sAND = "";
if($_POST['sSearch'] != ""){
   $sWhere = "(a.code LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.name LIKE '%".$db->escape( $_POST['sSearch'] )."%') ";
	$sAND = "AND ";
}
$sWhere .= ($_POST["roomtype_id"]) ? " and a.roomtype_id={$_POST["roomtype_id"]}" : "";
$sWhere .= ($_POST["active"]) ? " and a.active='{$_POST["active"]}'" : "";


/* Paging */
$sQuery = "SELECT a.room_id, a.code, a.name, a.remark, a.active, a.roomtype_id
				 ,c.name as roomtype_name, a.amount
           FROM room a 
           		left join roomtype c on c.roomtype_id=a.roomtype_id
		   $WHERE $sAND $sWhere
		   $sOrder
		   $sLimit";

$rResult = $db->get($sQuery);
$a = array();
if(is_array($rResult)){
	$runNo = 1;
	$list_id = get_config('sectionDisplayButton');
	$array_ck = explode(",", $list_id);
	
	
	foreach ($rResult as $r){
	  $id = $r["room_id"];
	  $manage =   get_datatable_icon("edit", $id);
	  $active = ($r["active"]=="T") ? "active" : "nonActive";   
	  $ck = $r["section_id"];
	  if(in_array($ck, $array_ck)) $manage = $manage; 
		$a[] = array($runNo
				      ,$r['code']
				      ,$r['name']
				      ,$r["roomtype_name"]
				      ,$r['amount']
				      ,$r['remark']
				      ,$manage);
		$runNo++;
	}
}

$aData = array();
$sQuery = "SELECT COUNT(*) as total
			  FROM room a
			  $WHERE $sAND $sWhere";

$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM room a";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
