<?php
if (!isset($_SESSION)) {//initialize the session
  session_start();
}
header ('Content-type: text/html; charset=utf-8');
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_COMPILE_ERROR);
date_default_timezone_set('Asia/Bangkok');

$is_login = $_SESSION["login"]["isLogin"];
$is_admin_login = $_SESSION["login"]["isAdminLogin"];
if(isset($_SESSION["login"]["info"])){
   $LOGIN_INFO = $_SESSION["login"]["info"];
   $USERNAME = $LOGIN_INFO["username"];
   $EMPID = $LOGIN_INFO["emp_id"];
   $MEMBERID = $LOGIN_INFO["member_id"];
   $RIGHTTYPEID = $LOGIN_INFO["righttype_id"];
   $RIGTHTYPENAME = $LOGIN_INFO["righttype_name"];
   $ISADMIN = ($RIGHTTYPEID==1) ? "T" : "F";
   $SECTIONID = (int)$LOGIN_INFO["section_id"];
}
if(isset($_SESSION["emp"])){
	$EMPINFO = $_SESSION["emp"]["empInfo"];
	$EMPNAME = $EMPINFO["prefix"].$EMPINFO["fname"]." ".$EMPINFO["lname"];
	$EMPCODE = $EMPINFO["code"];
  if(!$EMPINFO["img"])
         $EMPINFO["img"] = "images/no-avatar-male.jpg";
  $EMPIMG  =  $EMPINFO["img"];
}
?>
