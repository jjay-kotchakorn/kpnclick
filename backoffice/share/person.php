<?php 
function get_person($con="", $person_id="", $order=false, $limit_row=1000){
   if($con=="" && $person_id=="") return array();
   global $db;   
   $con_person_id = $person_id ? " and a.person_id=$person_id" : "";
   $con = $person_id ? "" : $con;
   $con_orders = ($order==true) ? " a.person_id " : " a.person_id desc";
   $q = "select a.person_id,
				a.code,
				a.name,
				a.company_id,
				a.remark,
				a.email,				
				a.phone,				
				a.mobile,
				a.active,
				a.position,
				a.recby_id,
				a.rectime,
				a.main,
				a.ext,
				b.name as company_name
		 from person a left join company b on b.company_id=a.company_id          
		 where a.active!='' $con $con_person_id
		 order by a.main desc, $con_orders
		 limit $limit_row";
   $r = $db->get($q);   
   return $r;
}
?>
