<?php 
function emp_info($con="", $emp_id="", $order=false){
   if($con=="" && $emp_id=="") return array();
   global $db;   
   $conemp_id = $emp_id ? " and a.emp_id=$emp_id" : "";
   $con = $emp_id ? "" : $con;
   $conorder = ($order==true) ? " a.emp_id " : " a.emp_id desc";
   $q = "select a.emp_id, a.code, a.prefix, a.fname, a.lname, a.birthdate, a.active, a.address
		         ,a.cid, a.email, a.homephone, a.nickname, a.phone, a.recby_id, a.rectime
		         ,a.supper_admin, a.img, a.img as tmpimg
		         ,a.position
		         ,a.org_name
	       from emp a left join emp b on a.recby_id=b.emp_id
   		 where a.active!='' $con $conemp_id
         order by $conorder
		 limit 100";
   $r = $db->get($q);   
   return $r;
}

function login_info($con="", $login_id=""){
	if(!$con && !$login_id) return array();
	global $db;
	$conlogin_id = $login_id ? " and a.login_id=$login_id" : "";
	$q = "select a.login_id, a.username, a.password, a.righttype_id, a.active, a.emp_id
			    ,a.recby_id, a.rectime, b.name as righttype_name,  a.member_id
		  from login a inner join righttype b on b.righttype_id=a.righttype_id
			   left join emp c on c.emp_id=a.recby_id
		  where a.active!='' $con $conlogin_id";
	$r = $db->get($q);
	return $r;
}
?>
