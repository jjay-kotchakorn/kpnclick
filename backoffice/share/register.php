<?php 
function get_register($con="", $register_id="", $order=false){
   if($con=="" && $register_id=="") return array();
   global $db;   
   $cond = $register_id ? " and a.register_id=$register_id" : "";
   $con = $register_id ? "" : $con;
   $conorder = ($order==true) ? " a.register_id " : " a.register_id desc";
   $q = "SELECT
            a.register_id,
            a.`no`,
            a.member_id,
            a.title,
            a.fname,
            a.lname,
            a.bill_request,
            a.slip_type,
            a.receipttype_id,
            a.receipttype_text,
            a.receipt_id,
            a.taxno,
            a.slip_name,
            a.slip_address,
            a.date,
            a.register_by,
            a.ref1,
            a.ref2,
            a.`status`,
            a.expire_date,
            a.project_id,
            a.project_name,
            a.project_price,
            a.receipt_title,
            a.receipt_fname,
            a.receipt_lname,
            a.receipt_no,
            a.receipt_gno,
            a.receipt_moo,
            a.receipt_soi,
            a.receipt_road,
            a.receipt_district_id,
            a.receipt_amphur_id,
            a.receipt_province_id,
            a.receipt_postcode,
            a.receipt_date,
            a.amount,
            a.project_id,
            a.active,
            a.recby_id,
            a.rectime,
            a.remark,
            a.pay_status,
            a.runno,
            a.docno,
            a.runyear,
            a.auto_del,
            a.telephone,
            a.payment_bank_id,
            a.receipt_status_id
         FROM register AS a
   		WHERE a.active!='' $con $cond
         ORDER BY $conorder
		 LIMIT 100
   ";
   $r = $db->get($q);   
   return $r;
}//end func


function get_payment_bank_register($con="", $payment_bank_register_list_id="", $order=false){
   if($con=="" && $payment_bank_register_list_id=="") return array();
   global $db;   
   $cond = $payment_bank_register_list_id ? " and a.payment_bank_register_list_id=$payment_bank_register_list_id" : "";
   $con = $payment_bank_register_list_id ? "" : $con;
   $conorder = ($order==true) ? " a.payment_bank_register_list_id " : " a.payment_bank_register_list_id desc";

   $q = "SELECT
            a.payment_bank_register_list_id,
            a.merchant_id,
            a.payment_bank_id,
            a.payment_options_id,
            a.`code`,
            a.register_id,
            a.invoice,
            a.ref1,
            a.ref2,
            a.active,
            a.recby_id,
            a.rectime,
            a.runyear,
            a.runno,
            a.doc_prefix,
            a.doc_no,
            a.amount,
            a.url_for_thank_you,
            a.url_for_payment,
            a.donation_date,
            a.order_expire_date,
            a.remark,
            a.pay_status,
            a.resp_code,
            a.resp_desc,
            a.resp_pay_status,
            a.resp_pay_date,
            a.credit_card_no,
            a.purchase_amt,
            a.fee,
            a.params_serialize,
            a.response_serialize,
            a.resp_start_transaction_date,
            a.resp_update_date,
            b.telephone,
            b.bill_request,
            b.slip_type,
            b.project_id,
            b.taxno,
            b.receipttype_text,
            b.receipt_title,
            b.receipt_fname,
            b.receipt_lname,
            b.receipt_no,
            b.receipt_gno,
            b.receipt_moo,
            b.receipt_soi,
            b.receipt_road,
            b.receipt_district_id,
            b.receipt_amphur_id,
            b.receipt_province_id,
            b.receipt_postcode,
            b.receipt_date,
            c.name_th AS project_name_th,
            c.name_eng AS project_name_en,
            d.name AS district_name,
            e.name AS amphur_name,
            f.name AS province_name,
            g.name AS pay_status_name,
            h.name AS payment_bank_name
        FROM payment_bank_register_list AS a
        LEFT JOIN register AS b ON b.register_id=a.register_id
        LEFT JOIN project AS c ON c.project_id=b.project_id
        LEFT JOIN district AS d ON d.district_id=b.receipt_district_id
        LEFT JOIN amphur AS e ON e.amphur_id=b.receipt_amphur_id
        LEFT JOIN province AS f ON f.province_id=b.receipt_province_id
        LEFT JOIN pay_status AS g ON g.pay_status_id=a.pay_status
        LEFT JOIN payment_bank AS h ON h.payment_bank_id=a.payment_bank_id
        WHERE a.active!='' $con $cond
         ORDER BY $conorder
         LIMIT 1000
   ";
   $r = $db->get($q);   
   // return $q;
   return $r;
}//end func


function get_payment_bank_register_list($con="", $payment_bank_register_list_id="", $order=false){
   if($con=="" && $payment_bank_register_list_id=="") return array();
   global $db;   
   $cond = $payment_bank_register_list_id ? " and a.payment_bank_register_list_id=$payment_bank_register_list_id" : "";
   $con = $payment_bank_register_list_id ? "" : $con;
   $conorder = ($order==true) ? " a.payment_bank_register_list_id " : " a.payment_bank_register_list_id desc";
   $q = "SELECT
            a.payment_bank_register_list_id,
            a.merchant_id,
            a.payment_bank_id,
            a.payment_options_id,
            a.`code`,
            a.register_id,
            a.invoice,
            a.ref1,
            a.ref2,
            a.active,
            a.recby_id,
            a.rectime,
            a.runyear,
            a.runno,
            a.doc_prefix,
            a.doc_no,
            a.amount,
            a.url_for_thank_you,
            a.url_for_payment,
            a.donation_date,
            a.order_expire_date,
            a.remark,
            a.pay_status,
            a.resp_update_date,
            a.resp_code,
            a.resp_desc,
            a.resp_pay_status,
            a.resp_start_transaction_date,
            a.resp_pay_date,
            a.credit_card_no,
            a.purchase_amt,
            a.fee,
            a.params_serialize,
            a.response_serialize
        FROM payment_bank_register_list AS a
        WHERE a.active!='' $con $cond
         ORDER BY $conorder
         LIMIT 100
   ";
   $r = $db->get($q);   
   return $r;
}//end func

?>
