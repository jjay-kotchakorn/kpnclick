<?php
function datatype($con="", $table="", $load=false){
  if(!$table) return;
  if(isset($_SESSION["dataTypeInfo"][$table]) && $load===false){
    return $_SESSION["dataTypeInfo"][$table];
  }else{
    global $db;
    if($db){
      $q = "select a.".$table."_id, a.code, a.name, a.name_eng, a.active
      ,a.recby_id, a.rectime
      from $table a 
      where a.active!='' $con
      order by a.code, a.".$table."_id";
      $rs = $db->get($q);
      return $_SESSION["dataTypeInfo"][$table] = (is_array($rs)) ? $rs : "";
    }
  }
}

function datatype_import($name, $table="", $check=false){
  global $db;
  $db->query("SET NAMES 'utf8'");
  $db->query("SET CHARACTER SET 'utf8'");
  $args = array();
  if(!$name) return "";
  $args["table"] = "{$table}";
  $q = "select max(code) as t from {$table}";
  $args["code"] = $db->data($q)+1;
  $q = "select {$table}_id from {$table} where name='".trim($name)."'";
  $id = $db->data($q);
  if($id) $args["id"] = $id;
  if($id && $check) return $id; 
  $args["name"] = trim($name);
  $args["active"] = "T";
  $args["recby_id"] = 1;
  $args["rectime"] = date("Y-m-d H:i:s");
  $ret = $db->set($args);
  return ($id) ? $id : $ret;
}
function get_datatype_import($name, $table=""){
  global $db;
    $db->query("SET NAMES 'utf8'");
  $db->query("SET CHARACTER SET 'utf8'");
  $args = array();
  if(!$name) return "";
  $q = "select {$table}_id from {$table} where name='".trim($name)."'";
  $id = $db->data($q);
  return $id;
}
function datatype_university($load=false){
    global $db;
    if($db){
      $q = "SELECT UGRP, UID, UNAME 
    FROM `university` 
    WHERE `USTATUS` = 1 
      ORDER BY CONVERT(UNAME USING TIS620)";
      $rs = $db->get($q);
      return $_SESSION["dataTypeInfo"][$table] = (is_array($rs)) ? $rs : "";
    }
}

function datatype_sort_name($con="", $table="", $load=false){
  if(!$table) return;
  if(isset($_SESSION["dataTypeInfo"][$table]) && $load===false){
    return $_SESSION["dataTypeInfo"][$table];
  }else{
    global $db;
    if($db){
      $q = "select a.".$table."_id, a.code, a.name, a.name_eng, a.active
      ,a.recby_id, a.rectime
      from $table a 
      where a.active!='' $con
      order by CONVERT(a.name USING TIS620) asc";
      $rs = $db->get($q);
      return $_SESSION["dataTypeInfo"][$table] = (is_array($rs)) ? $rs : "";
    }
  }
}