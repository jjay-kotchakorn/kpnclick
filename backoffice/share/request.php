<?php 
function get_request($con="", $request_id="", $order=false, $limit_row=1000){
   if($con=="" && $request_id=="") return array();
   global $db;   
   $con_request_id = $request_id ? " and a.request_id=$request_id" : "";
   $con = $request_id ? "" : $con;
   $con_orders = ($order==true) ? " a.request_id " : " a.request_id desc";
   $q = "select a.request_id,
				a.runyear,
				a.runno,
				a.docno,
				a.dept_phone,
				a.docdate,
				a.title,
				a.date_start,
				a.date_stop,
				a.date_return,
				a.member_id,
				a.room_id,
				a.room_other,
				a.servicecharge_id,
				a.servicecharge_detail,
				a.returnapprove_id,
				a.returnapprovetime,
				a.requestapprove_id,
				a.requestapprovetime,
				a.cancelby_id,
				a.canceltime,
				a.requeststatus_id,
				a.remark,
				a.recby_id,
				a.rectime,
				a.active,
				a.requeststate_id,
				a.detail,
				a.request_item,
				a.request_camera,
				a.request_video,
				a.request_audio,
				a.request_editing_video,
				a.set_time,
				b.name as requeststatus_name,
				concat(c.prefix,' ',c.`fname`,' ',c.lname) as membername,
				c.code as membercode,
                d.name as department_name,
                e.name as membertype_name,
                concat(f.prefix,' ',f.`fname`,' ',f.lname) as requestname,
                concat(g.prefix,' ',g.`fname`,' ',g.lname) as returnname,
                h.name as room_name
		 from request a left join requeststatus b on b.requeststatus_id=a.requeststatus_id
		 	  left join member c on c.member_id=a.member_id
			  left join department d on d.department_id=c.department_id
			  left join membertype e on e.membertype_id=c.membertype_id
			  left join emp f on f.emp_id=a.requestapprove_id
			  left join emp g on g.emp_id=a.returnapprove_id
			  left join room h on h.room_id=a.room_id			  		 	              
		 where a.active!='' $con $con_request_id
		 order by $con_orders
		 limit $limit_row";

   $r = $db->get($q);   
   return $r;
}

function get_request_detail($con="", $request_detail_id="", $order=false, $limit_row=1000){
   if($con=="" && $request_detail_id=="") return array();
   global $db;   
   $con_request_detail_id = $request_detail_id ? " and a.request_detail_id=$request_detail_id" : "";
   $con = $request_detail_id ? " and a.request_detail_id=$request_detail_id" : $con;
   $con_orders = ($order==true) ? " a.request_detail_id " : " a.request_detail_id desc";
   $q = "select a.request_detail_id,
		 a.request_id,
		 a.item_id,
		 a.amount,
		 a.remark,
		 a.recby_id,
		 a.rectime,
		 b.itemtype_id,
		 b.itemtype_text,
		 b.code,
		 b.name,
		 c.name as itemtype_name
		 from request_detail a left join item b on a.item_id=b.item_id
		 	left join itemtype c on c.itemtype_id=b.itemtype_id
		 where a.active!='' $con $con_request_detail_id
		 order by $con_orders
		 limit $limit_row";
   $r = $db->get($q);   
   return $r;
}

?>
