<?php 
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
//include_once "./inc/header-bootstrap.php";
global $db;
// d($_SERVER);
// d($_POST); //die();
$province = datatype_sort_name(" and a.active='T'", "province", true);

$amount = $_POST["amount"];
$term_id = $_POST["term_id"];
$lang_ref = $_POST["lang_ref"];

$q ="SELECT project_id
	, name_th
	, donation_min
	, donation_min_receipt 
	FROM project WHERE code_project ={$term_id}
";
$rs = $db->rows($q);

$don_project = $rs["name_th"];
$project_id = $rs["project_id"];
$donation_min = $rs["donation_min"];
$donation_min_receipt = $rs["donation_min_receipt"];

$txt_alert_danation_min = "";
if ( $donation_min>$amount ) {
	$amount = $donation_min;
	$txt_alert_danation_min = "ยอดบริจาคขั้นตํ่าในโครงการนี้จำนวน ".number_format($donation_min, 2)." บาท";
}//end if

$txt_alert_donation_min_receipt = "";
if ( $donation_min_receipt>$amount ) {
	$txt_alert_donation_min_receipt = "ยอดบริจาคขั้นตํ่าที่สามารถออกใบเสร็จฯ คือ ".number_format($donation_min_receipt, 2)." บาท";
}//end if

?>

<!DOCTYPE html>
<html lang="en">

<?php include ('./inc/header-bootstrap.php'); ?>

<body>

	<div class="main-content">
		<div class="container">
			<!-- action="" -->
			<form  name="frm_donate" id="frm_donate"  method="post" action="pay-gate/process.php">
				
				<input type="hidden" class="form-control" name="amount" id="amount" value="<?php echo $amount;?>">
				<input type="hidden" class="form-control" name="project_id" id="project_id" value="<?php echo $project_id;?>">		
				<div class="row" id="headmascot">
					<!-- start logo -->
					<div class="form-group col-sm-3">
						<a id="logo" class="navbar-brand"  href="#" title="สำนักงานจัดหารายได้ สภากาชาดไทย" rel="home">
							<img src="./images/logo-redcross.png" alt="สำนักงานจัดหารายได้ สภากาชาดไทย logo">
						</a>
					</div>
					<!-- end logo -->
					<div class="form-group col-sm-9">
						<object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="700" height="115">
							<param name="movie" value="https://redcrossfundraising.org/Donation/images/head.swf" />
							<param name="quality" value="high" />
							<param name="wmode" value="opaque" />
							<param name="swfversion" value="6.0.65.0" />
							<!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->
							<param name="expressinstall" value="expressInstall.swf" />
							<!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
							<!--[if !IE]-->
							<object type="application/x-shockwave-flash" data="https://redcrossfundraising.org/Donation/images/head.swf" width="700"
							height="115">
							<!--[endif]-->
							<param name="quality" value="high" />
							<param name="wmode" value="opaque" />
							<param name="swfversion" value="6.0.65.0" />
							<param name="expressinstall" value="expressInstall.swf" />
							<!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
							<div>
								<h4>
									Content on this page requires a newer version of Adobe Flash Player.</h4>
									<p>
										<a href="http://www.adobe.com/go/getflashplayer">
											<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif"
											alt="Get Adobe Flash player" width="112" height="33" /></a></p>
										</div>
										<!--[if !IE]-->
									</object>
									<!--[endif]-->
								</object>
							</div>
						</div>

						<div class="main-content-inner col-md-9 col-sm-8 col-xs-12">

							<header id="header" class="inner-page-header">	
								<h1>รายละเอียดการบริจาค</h1>
							</header>
							<!-- <p>Resize the browser window to see the effect. When the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other.</p> -->


							<div class="don-content">
								<div class="row ">
									<div class="col-md-12">
										<h1>รายละเอียดการบริจาค</h1>
									</div>
								</div>

								<div id="don_detail">
									<div class="row">
										<label class="form-group col-sm-3" for="don_project">บริจาคให้โครงการ:</label>
										<div class="form-group col-sm-9">
											<span><?php echo $don_project; ?></span>
										</div>
									</div>
									<div class="row">
										<label class="form-group col-sm-3" for="don_amount">จำนวนเงินบริจาค: </label>
										<div class="form-group col-sm-9">
											<span><?php echo number_format($amount, 2); ?> บาท</span>
										<?php 
											if ( !empty($txt_alert_danation_min) ) {				
										?>
												<span class="red" style="font-size: 21px; font-weight:  bold;"> * <?php  echo $txt_alert_danation_min; ?></span>
										<?php 
											}//end if
										?>											
										</div>
									</div>
									<!-- <label class="form-group col-sm-3" for="don_name">ชื่อผู้บริจาค:</label>
									<div class="form-group col-sm-9">
										<input type="text" class="form-control" name="don_name" id="don_name" placeholder="name">
									</div> -->
								</div>

								<div class="row">
									<label class="form-group col-sm-3" for="don_attributes_address_1">บริจาคในนาม: <span class="red">*</span>
									</label>
									<!-- checked="checked" -->
									<div id="don_radio">
										<label class="radio-container">บุคคลธรรมดา
											<input type="radio" name="radio" id="radio_individual" value="radio_individual" >
											<span class="radio-checkmark"></span>
										</label>
										<label class="radio-container">นิติบุคคล
											<input type="radio" name="radio" id="radio_corporation" value="radio_corporation">
											<span class="radio-checkmark"></span>
										</label>
									</div>
								</div>

								<div class="row ">
									<div id="don_individual">
										<label class="form-group col-sm-2" style="text-align: right;">คำนำหน้าชื่อ<span class="red">*</span></label>
										<div class="form-group col-sm-2">
											<select name="title_th" id="title_th"  class="regis_select">
												<option value="นาย">นาย</option>
												<option value="นาง">นาง</option>
												<option value="นางสาว">นางสาว</option>
											</select>    
										</div>

										<label class="form-group col-sm-1" style="text-align: right;">ชื่อ<span class="red">*</span></label>
										<div class="form-group col-sm-3">
											<input type="text" onkeypress="enterToSubmit();" tabindex="3" class="boxInput" name="fname" id="fname" maxlength="255" style="width:90%;">
										</div>

										<label class="form-group col-sm-1" style="text-align: right;">นามสกุล<span class="red">*</span></label>
										<div class="form-group col-sm-3">
											<input type="text" onkeypress="enterToSubmit();" tabindex="3" class="boxInput" name="lname" id="lname" maxlength="255" style="width:90%;">
										</div>
									</div>
								</div>

								<div class="row ">
									<div id="don_corporation">
										<label class="form-group col-sm-1" style="text-align: right;">ชื่อ<span class="red">*</span></label>
										<div class="form-group col-sm-11">
											<input type="text" onkeypress="enterToSubmit();" tabindex="3" class="boxInput" name="cor_name" id="cor_name" maxlength="255" style="width:90%;">
										</div>
									</div>
								</div>

								<div class="row ">
									<div>
										<label class="form-group col-sm-2" style="text-align: right;">หมายเลขโทรศัพท์<span class="red">*</span></label>
										<div class="form-group col-sm-3">
											<input type="text" class="boxInput" name="telephone" id="telephone" maxlength="10" style="width:90%;">
										</div>
									</div>
								</div>
							</div>

							<div id="don_bottom">
								<div class="col-md-12">

							<?php 
								if ( !empty($txt_alert_donation_min_receipt ) ) {				
							?>
									<span class="red" style="font-size: 21px; font-weight:  bold;"> ** <?php  echo $txt_alert_donation_min_receipt; ?></span>
							<?php 
								}//end if
							?>				

							<!-- 									
								<button class="sub-button don-button" name="don_sub" id="don_sub">ออกใบเสร็จ</button>
							-->
							<!-- checked="checked" -->
							<label class="don-checkbox">ออกใบเสร็จรับเงิน
								<input type="checkbox" name="don_bill" id="don_bill">
								<span class="checkbox-checkmark"></span>
							</label>

						</div>
					</div>
					<br>
					<br>
					<hr>

					<div id="don_address">
						<div class="row ">
							<div class="col-md-12">
								<h1>ที่อยู่เพื่อจัดส่งใบเสร็จ</h1>
							</div>
						</div>

						<fieldset>
							<ol>
								<div class="row ">
									<li id="don_attributes_address_6_input">
										<label class="form-group col-sm-2" for="don_attributes_address_6_input">เลขประจำตัวผู้เสียภาษี <span class="red">*</span></label>
										<div class="form-group col-sm-6">
											<input name="taxno" id="taxno" type="text" title="" tabindex="16" class="boxInput" style="width:160px;" />
										</div>
									</li>
								</div>

								<div class="row ">
									<li id="don_attributes_address_2_input">
										<label class="form-group col-sm-2" for="don_attributes_address_2">ที่อยู่ <span class="red">*</span></label>
										<div class="form-group col-sm-10">
											<textarea name="address" rows="4" cols="20" id="address" tabindex="14" class="boxInput" style="width:77%;">
											</textarea>
										</div>
									</li>
								</div>

								<div class="row ">
									<li id="don_attributes_address_3_input">
										<label class="form-group col-sm-2" for="don_attributes_address_3_input">จังหวัด <span class="red">*</span></label>
										<div class="form-group col-sm-6">
											<select class="boxInput" name="province_id" tabindex="16" id="province_id" onchange="getDropDown('#amphur_id', this.value, 'amphur', './data/province.php')" style="width:160px;" >
												<option value="">---- เลือก ----</option>
												<?php foreach ($province as $key => $value) {
													$id = $value['province_id'];
													$name = $value['name'];
													echo  "<option value='$id'>$name</option>";
												} ?>
											</select>
										</div>
									</li>
								</div>

								<div class="row ">
									<li id="don_attributes_address_4_input">
										<label class="form-group col-sm-2" for="don_attributes_address_4_input">เขต/อำเภอ <span class="red">*</span></label>
										<div class="form-group col-sm-6">
											<select name="amphur_id" tabindex="17" id="amphur_id" onchange="getDropDown('#district_id', this.value, 'district', './data/province.php')" class="boxInput" style="width:160px;">
												<option value="">---- เลือก ----</option>
											</select>
										</div>
									</li>
								</div>

								<div class="row ">
									<li id="don_attributes_address_5_input">
										<label class="form-group col-sm-2" for="don_attributes_address_5_input">แขวง/ตำบล <span class="red">*</span></label>
										<div class="form-group col-sm-6">
											<select name="district_id" tabindex="18" id="district_id"  class="boxInput" style="width:160px;">
												<option value="">---- เลือก ----</option>

											</select>
										</div>
									</li>
								</div>

								<div class="row ">
									<li id="don_attributes_address_6_input">
										<label class="form-group col-sm-2" for="don_attributes_address_6_input">รหัสไปรษณีย์ <span class="red">*</span></label>
										<div class="form-group col-sm-6">
											<input name="zip_code" id="zip_code" type="text" maxlength="5"   title="กรอกเฉพาะตัวเลขให้ครบ 5 หลัก" tabindex="16" class="boxInput" style="width:160px;" />
											<span id="MainContent_reqZipcode" class="redText" style="visibility:hidden;"></span>
										</div>
									</li>
								</div>

							</ol>
						</fieldset>

						<hr>

					</div>


					<div id="don_bank">
						<input type="hidden" name="bank_type" id="bank_type">
						<input type="hidden" name="payment_bank_id" id="payment_bank_id" value="">	
						<div class="margin30"></div> 

						<h1>เลือกช่องทางการบริจาค</h1>
						<div class="row ">
							<div class="form-group">
								<?php
								$q = "SELECT * 
									FROM payment_bank 
									WHERE active='T'
									ORDER BY order_show ASC
								";
								$rs = $db->get($q);

								foreach ($rs as $key => $v) {
									$payment_bank_id = $v["payment_bank_id"];
									$image_url = $v["image_url"];							
									?>
									<div class="col-sm-2">
										<a href="#" onclick="select_bank('<?php echo $payment_bank_id; ?>')">
											<img style="width: 130px; height: 70px;" src="<?php echo $image_url; ?>">
										</a>
									</div>
									<?php 
							}//end loop $v
							?> 
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</form>
</div>
</div>

<footer id="footer" class="site-footer" role="contentinfo">
	<!-- <div class="container"> -->
	<div class="row">
		<div class="site-info col-md-12 text-center">
			สำนักงานจัดหารายได้ สภากาชาดไทย ตึกอำนวยนรธรรม ชั้น 2 เลขที่ 1873 ถนนพระราม 4 เขตปทุมวัน กรุงเทพฯ 10330 โทรศัพท์ 0 2256 4440, 0 2255 9911 			
		</div>
		<!-- .site-info -->
	</div>
	<!-- </div> --><!-- .col-full -->
</footer>

</body>
</html>

<script type="text/javascript">

	$(document).ready(function(){
		var donation_min_receipt = "<?php echo $donation_min_receipt; ?>";
		var amount = "<?php echo $amount; ?>";

		$('#don_address').hide();
		
		if ( (amount-donation_min_receipt)<0 ) {
			$(".don-checkbox").hide();
		}//end if


		$('#don_individual').hide();
		$('#don_corporation').hide();

		$("#radio_individual").click(function(){
			$("#don_individual").show();
			$("#don_corporation").hide();
		});
		$("#radio_corporation").click(function(){
			$("#don_corporation").show();
			$("#don_individual").hide();
		});//end radio

		$('#don_bill').change(function(){
			if(this.checked){
				$('#don_address').fadeIn();
			}else{
				$('#don_address').fadeOut();
			}//end else
		});//end checkbox

		$("#radio_individual").attr('checked', 'checked');
		$("#don_individual").show();
	});

	function select_bank(id){
		$("#payment_bank_id").val(id);
		var check_valid = false;

		if( $('#radio_individual').is(':checked') ) { 
			// alert("it's checked");
			//alert( $("#telephone").val() );
			var fname = $("#fname").val();
			var lname = $("#fname").val();
			var telephone = $("#telephone").val();
			
			if( fname=="" ){
				alert('กรุณาใส่ชื่อจริง');
			}else if( lname=="" ){
				alert('กรุณาใส่นามสกุล');
			}else if( telephone=="" ){
				alert('กรุณาใส่หมายเลขโทรศัพท์');
			}
			else{
				check_valid = true;
			}//else

		}else if( $('#radio_corporation').is(':checked') ) { 
			 //alert("it's checked");
			 var cor_name = $("#cor_name").val();
			 var telephone = $("#telephone").val();

			 if( cor_name=="" ){
			 	alert('กรุณาใส่ชื่อ');
			 }else if( telephone=="" ){
			 	alert('กรุณาใส่หมายเลขโทรศัพท์');
			 } 
			 else{
			 	check_valid = true;
			}//else 

		}//end else if


		if($('#don_bill').is(':checked')){ 
			check_valid = false;
			var taxno = $("#taxno").val();
			var address = $("#address").val();
			var province_id = $("#province_id").val();
			var amphur_id = $("#amphur_id").val()
			var district_id = $("#district_id").val()
			var zip_code = $("#zip_code").val();

			if( taxno=="" ){
				alert('กรุณาใส่เลขประจำตัวผู้เสียภาษี');
			}else if( address=="" ){
				alert('กรุณาใส่ที่อยู่');
			}else if( province_id=="" ){
				alert('กรุณาเลือกจังหวัด');
			}else if( amphur_id=="" ){
				alert('กรุณาเลือกเขต/อำเภอ'); 
			}else if( district_id=="" ){
				alert('กรุณาเลือกแขวง/ตำบล');
			}else if( zip_code=="" ){
				alert('กรุณาใส่รหัสไปรษณีย์');
			}
			else{
				check_valid = true;
			}//else 

		}// end if


		if ( check_valid ) {
			// alert("submit pass");
			//jQuery("#frm_donate").submit();

		    var r = confirm("ยืนยันการบริจาค");
		    if (r == true) {
		        jQuery("#frm_donate").submit();
		    } else {
		        
		    }//end else

		}//end if

	 }//end func

	 

	 $(document).ready(function($) {

	 	var ck = ckid(); 

	 	$("#form_p").validate();
	 	$(document).on("focusin", "#birthdate", function(event) {

	 		$(this).prop('readonly', true);

	 	});

	 	$(document).on("focusout", "#birthdate", function(event) {

	 		$(this).prop('readonly', false);

	 	});
	 	var msg = "<?php echo $msg?>";
	 	var error_msg = "<?php echo $error?>";
	 	if(msg!=""){
	 		var options =  $.parseJSON('{"text":"<p>'+msg+'</p>","layout":"center","type":"success"}');
	 		noty(options); 
	 	} 
	 	if(error_msg!=""){
	 		var options =  $.parseJSON('{"text":"<p>'+error_msg+'</p>","layout":"center","type":"error"}');
	 		noty(options); 
	 	}	
		//dateThai("#birthdate");
		var d = new Date();
		var toDay = d.getDate() + '/' + (d.getMonth() + 1) + '/' + (d.getFullYear() + 543);
		
		$("#birthdate").datepicker({
			maxDate: 0,
			dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, yearRange:"-100:+0", isBuddhist: true, defaultDate: toDay, dayNames: ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'],
			dayNamesMin: ['อา.', 'จ.', 'อ.', 'พ.', 'พฤ.', 'ศ.', 'ส.'],
			monthNames: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
			monthNamesShort: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
			onSelect: function(dateText) {
	          //$("#birthdate").focus();
	      }
	  });


		$.validator.addMethod( "captcha", function( value, element ) {
			var captcha = $("#captcha").val(); 
			return this.optional( element ) || value==captcha;
		}, "รหัสตรวจสอบความปลอดภัย ไม่ถูกต้อง" );

		orgtype_text_required('no');
		change_select_university();
		$(function(){
			$("#province_id").click(function(event) {
				var select = $("#district_id");
				var options = select.attr('options');
				$('option', select).remove();
				$(select).append('<option value="">---- เลือก ----</option>');
			});      
			$("#receipt_province_id").click(function(event) {
				var select = $("#receipt_district_id");
				var options = select.attr('options');
				$('option', select).remove();
				$(select).append('<option value="">---- เลือก ----</option>');
			});
			$("#same_addr").click(function() {
				if($(this).is(":checked")){
				}else{
					checkboxaddress_reset();
				}
			});      
			$("input[name=receipttype_id]").change(function() {
				$("#taxno").val('');
				$("#branch").val('');
				checkboxaddress_reset();
			});


			$("#title_th").change(function(){
				if($(this).val() == 'นาย'){ 
					$("#title_en").val("Mr");
					$("#title_th_text").hide();
					$("#title_en_text").hide();
					if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
					if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
					$("#gender").val("M");
				}
				else if($(this).val() == 'นาง'){ 
					$("#title_en").val("Mrs");
					$("#title_th_text").hide();
					$("#title_en_text").hide();
					if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
					if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
					$("#gender").val("F");
				}
				else if($(this).val() == 'นางสาว'){ 
					$("#title_en").val("Ms");
					$("#title_th_text").hide();
					$("#title_en_text").hide();
					if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
					if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
					$("#gender").val("F");
				}
				else if($(this).val() == 'อื่นๆ'){ 
					$("#title_en").val("Other");
					$("#title_th_text").show();
					$("#title_en_text").show();
					$("#title_th_text").addClass('required');
					$("#title_en_text").addClass('required');
					$("#gender").val("");
				}
			});

			$("#title_en").change(function(){
				if($(this).val() == 'Mr'){ 
				  //$("#title_th").val("นาย");
					//$("#title_th_text").hide();
					$("#title_en_text").hide();
				  //if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
				  if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
				  $("#gender").val("M");
				}
				else if($(this).val() == 'Mrs'){ 
				  //$("#title_th").val("นาง");
					//$("#title_th_text").hide();
					$("#title_en_text").hide();
				  //if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
				  if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
				  $("#gender").val("F");
				}
				else if($(this).val() == 'Ms'){ 
				  //$("#title_th").val("นางสาว");
					//$("#title_th_text").hide();
					$("#title_en_text").hide();
				//	if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
				if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
				$("#gender").val("F");
			}
			else if($(this).val() == 'Other'){ 
				 //	$("#title_th").val("อื่นๆ");
					//$("#title_th_text").show();
					$("#title_en_text").show();
	       //$("#title_th_text").addClass('required');
	       $("#title_en_text").addClass('required');
	       $("#gender").val("");
	   }
	});
			
			$("#receipt_title").change(function(){
				if($(this).val() == 'นาย'){ 
					$("#receipt_title_text").hide();
					if($("#receipt_title_text").hasClass('required'))  $("#receipt_title_text").removeClass('required');
				}
				else if($(this).val() == 'นาง'){ 
					$("#receipt_title_text").hide();
					if($("#receipt_title_text").hasClass('required'))  $("#receipt_title_text").removeClass('required');
				}
				else if($(this).val() == 'นางสาว'){ 
					$("#receipt_title_text").hide();
					if($("#receipt_title_text").hasClass('required'))  $("#receipt_title_text").removeClass('required');
				}
				else if($(this).val() == 'อื่นๆ'){ 
					$("#receipt_title_text").show();
					$("#receipt_title_text").addClass('required');
				}
			});

			$("input[type=radio][name='slip_type']").change(function(){   
	            //if($(this).val() == 'corparation'){ 
//	            	$("#taxno").addClass('required');
//	            }else{ 
//	            	if($("#taxno").hasClass('required'))  $("#taxno").removeClass('required');
//	            }
});	 

			$("input[type=radio][name='receipttype_id']").change(function(){   
				if($(this).val() == '5'){ 
					$("#receipttype_text").addClass('required');
				}else{ 
					if($("#receipttype_text").hasClass('required'))  $("#receipttype_text").removeClass('required');
				}
			});
			
			$("input[type=radio][name='orgtype_id']").change(function(){   
				if($(this).val() == '5'){ 
	            	//$("#orgtype_text").addClass('required');
	            }else{ 
	            	//if($("#orgtype_text").hasClass('required'))  $("#orgtype_text").removeClass('required');
	            	$("#orgtype_text").val('');
	            }
	        });
			
		});
});

function receipt_info(id){
	if(typeof id=="undefined") return;
	var url = "data/fa-receipt.php";
	var param = "name=receipt_detail&value="+id;
	$.ajax( {
		"dataType":'json', 
		"type": "GET",
		"async": false, 
		"url": url,
		"data": param, 
		"success": function(data){
			console.log(data);	
			$.each(data, function(index, array){			 				 	
				$("#taxno").val(array.taxno);
				$("#branch").val(array.branch);
				$("#receipt_no").val(array.no);
				$("#receipt_gno").val(array.gno);
				$("#receipt_moo").val(array.moo);
				$("#receipt_soi").val(array.soi);
				$("#receipt_road").val(array.road);
				$("#receipt_postcode").val(array.postcode);
				$("select[name=receipt_province_id] option").filter(function() {
					return $(this).val() == array.province_id; 
				}).attr('selected', true);		
				var select = $("#receipt_amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.amphur_id+'"> '+array.amphur_name+' </option>');	
				var select = $("#receipt_district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.district_id+'"> '+array.district_name+' </option>');					
			});				 
		}
	});
}

function enterToSubmit(){
	$(document).keypress(function(event) {
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13') {
			$("#form_p").submit();    
		}
	});
}
function display_element(str, show){
	if(typeof str=="undefined") return false;
	var show = (typeof show!="undefined") ? true : false;
	var data = str.split(', ');
	for(var i=0;i<data.length;i++){
		if(show) $("#"+data[i]).show();
		else $("#"+data[i]).hide();
	}
}

function receipt_other(){
	var select = $("#receipt_id");
	var options = select.attr('options');
	$('option', select).remove();
	$(select).append('<option value="">---- เลือก ----</option>');
	$("#trReceiptInfo").hide();
}

function trReceiptInfoShow()
{
	$("#trReceiptInfo").show();
}

function select_normal_niti(select_in){
	$("#receipt_detail").show();

	var ele = "typeCom1, typeCom2, typeCom3, typeCom4, typeCom5, typeCom6, tax_text, tax_input, select_menu_typeCom";
	if(select_in == "normal"){			
		$("#receipttype_text").val('');

		$("#taxno").val($("#cid").val());
			//$("#taxno").attr('disabled',true);
			$(".pro4-colum1-r1 input").attr('disabled',true);
			$(".pro4-colum1-r1 input").removeAttr('checked');			
			$(".pro4-colum1-r1").hide();
			$("#same_addr").prop( "checked", true );
			
			$("#receipt_detail_title").hide();
			//$("#receipt_taxno").hide();
			$("#receipt_id").removeClass('required');
			$("#receipt_id").removeClass('error');
			$("#receipt_id-error").hide();
			$("#same_address").show();
			$("#same_address_checkbox").show();
			$("#trSlipName").show();
			var select = $("#receipt_id");
			var options = select.attr('options');
			$('option', select).remove();
			$(select).append('<option value="">---- เลือก ----</option>');		
			checkboxaddress();
			
		}else if(select_in == "niti"){
			$("#taxno").val('');
			$("#branch").val('');
			$(".pro4-colum1-r1 input").attr('disabled',false);
			//$("#taxno").attr('disabled',false);			
			$(".pro4-colum1-r1").show();
			$("#same_addr").prop( "checked", false );
			$("#receipt_id").addClass('required');
			$("#receipt_detail_title").show();
			//$("#receipt_taxno").show();
			$("#same_address_checkbox").hide();
			$("#trSlipName").hide();
			checkboxaddress_reset();
			receipttype_text_required("no");
		}else{
			$("#receipttype_text").val('');
			$("#taxno").val('');
			$("#branch").val('');
			//$("#taxno").attr('disabled',true);
			$(".pro4-colum1-r1 input").attr('disabled',true);
			$(".pro4-colum1-r1 input").removeAttr('checked');
			$(".pro4-colum1-r1").hide();
			$("#same_addr").prop( "checked", true );
			$("#receipt_detail_title").hide();
			//$("#receipt_taxno").hide();
			$("#same_address").show();
			$("#same_address_checkbox").show();
			$("#trSlipName").show();
			checkboxaddress();
		}
	}

	function checkboxaddress(){
		
		if($("#same_addr").is(':checked'))
		{
			$("#taxno").val($("#cid").val());

			$("#receipt_title").val($("#title_th").val());
			if($("#receipt_title").val() == 'อื่นๆ'){ 
				$("#receipt_title_text").show();
				$("#receipt_title_text").addClass('required');
				$("#receipt_title_text").val($("#title_th_text").val());
				
			}
			else
			{
				$("#receipt_title_text").hide();
				$("#receipt_title_text").removeClass('required');
				$("#receipt_title_text").val("");
			}
			$("#receipt_fname").val($("#fname_th").val());
			$("#receipt_lname").val($("#lname_th").val());		
			
			$("#receipt_no").val($("#no").val());
			$("#receipt_gno").val($("#gno").val());
			$("#receipt_moo").val($("#moo").val());
			$("#receipt_soi").val($("#soi").val());
			$("#receipt_road").val($("#road").val());

			$('#receipt_province_id').val($('#province_id').val());

			var select = $("#receipt_amphur_id");
			$('option', select).remove();
			$(select).append('<option value="'+$("#amphur_id").val()+'"> '+ $("#amphur_id").find(":selected").text()+' </option>');	

			var select = $("#receipt_district_id");
			$('option', select).remove();
			$(select).append('<option value="'+$("#district_id").val()+'"> '+$("#district_id").find(":selected").text()+' </option>');	

			$("#receipt_postcode").val($("#postcode").val());
		}
	}
	
	function checkboxaddress_reset()
	{
		$("#taxno").val("");		
		$("#receipt_title").val("");
		$("#receipt_title_text").hide();
		$("#receipt_title_text").removeClass('required');
		$("#receipt_title_text").val("");
		$("#receipt_fname").val("");
		$("#receipt_lname").val("");		

		$("#receipt_no").val("");
		$("#receipt_gno").val("");
		$("#receipt_moo").val("");
		$("#receipt_soi").val("");
		$("#receipt_road").val("");

		$('#receipt_province_id').val("");

		var select = $("#receipt_amphur_id");
		$('option', select).remove();
		$(select).append('<option value="">---- เลือก ----</option>');

		var select = $("#receipt_district_id");
		$('option', select).remove();
		$(select).append('<option value="">---- เลือก ----</option>');

		$("#receipt_postcode").val("");
	}

	function orgtype_other()
	{
		var select = $("#org_name");
		var options = select.attr('options');
		$('option', select).remove();
		$(select).append('<option value="">---- เลือก ----</option>');
	}
	
	function change_select_university()
	{
		var id = $("#select_university").val().split("-");
		$("#grd_ugrp1").val(id[0]);
		$("#grd_uid1").val(id[1]);
		$("#grd_uname1").val($("#select_university option:selected").text());

		if ($("#select_university option:selected").val() != '0-0')
		{
			$("#grd_other").val("");
			$("#grd_other").attr('disabled','disabled');
		}
		else
		{
			$("#grd_other").removeAttr('disabled');
		}
	}
	
	function orgtype_text_required(value)
	{
		if(value == 'yes')
		{
			$("#orgtype_text").addClass('required');
			$("#table_Org_Name").hide();
			$(".table_Org_Name").hide();
			$("#orgtype_text").removeAttr('disabled');
		}
		else
		{
			$("#orgtype_text").val("");
			$("#orgtype_text").removeClass('required');
			$("#orgtype_text").removeClass('error');
			$("#orgtype_text-error").hide();
			$("#table_Org_Name").show();
			$(".table_Org_Name").show();
			$("#orgtype_text").attr('disabled','disabled');
		}
	}
	
	function receipttype_text_required(value)
	{
		if(value == 'yes')
		{
			$("#receipttype_text").addClass('required');
			$("#receipttype_text").removeAttr('disabled');
		}
		else
		{
			$("#receipttype_text").val("");
			$("#receipttype_text").removeAttr('class');
			$("#receipttype_text-error").hide();
			$("#receipttype_text").attr('disabled','disabled');
		}
	}
	
	function cid_check(v){
		if(v.length!=13) return;
		var cid = v;

		var url = "data/member-cid.php";
		var param = "cid="+cid;
		$.ajax({
			"dataType":'json', 
			"type": "POST",
			"async": false, 
			"url": url,
			"data": param, 
			"success": function(data){
				$.each(data, function(index, el) {
					if(el.result>0){
						msgError("เลขที่บัตรประชาชน "+v+" เคยสมัครเป็นสมาชิกแล้ว");
						$("#cid").val(' ');
					}
				});


			}
		});
	}

	function ckid(){
		if (!!navigator.userAgent.match(/Trident\/7\./))
			return true;
		else
			return false;
	}

</script>