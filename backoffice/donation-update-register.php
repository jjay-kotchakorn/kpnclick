<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
//include_once "./share/course.php";
//include_once "./share/member.php";
//require_once dirname(__FILE__) . '/PHPExcel.php';
global $db;

// d($db);
// d($_GET);

$payment_bank_id = $_GET["payment_bank_id"];
$q = "SELECT file_update_result FROM payment_bank WHERE payment_bank_id={$payment_bank_id}";
$url_update = $db->data($q);

?>

<style>
#progress {
  width: 950px;
  border: 1px solid #aaa;
  height: 30px;
}
#progress .bar {
  background-color: #21EC1C;
  height: 30px;
}
</style>

<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">	
		<div class="cl-mcont">
			<div class="row">
				<div class="col-md-12">
					<div class="block-flat">
						<div class="header">							
							<h3>อัพเดตรายการผู้มาบริจาค</h3>
						</div>
						<?php 
							$start = date("Y-m-01");
							$stop = date('Y-m-t',strtotime('today'));
							$dateStart = ($_POST["date_start"]) ? thai_to_timestamp($_POST["date_start"]) :  $start;
							$dateStop =  ($_POST["date_stop"]) ? thai_to_timestamp($_POST["date_stop"]) : $stop;
							if ($dateStart || $dateStop) {
							    if (!$dateStart && $dateStop)
							        $dateStart = $dateStop;
							    if (!$dateStop && $dateStart)
							        $dateStop = $dateStart;
							    $t = $dateStart;
							    if ($dateStart > $dateStop) {
							        $dateStart = $dateStop;
							        $dateStop = $t;
							    }
							}
						 ?>							
						<div class="content">
							<form action="" method="post" id="frmMain">							
								<div class="form-group row">
									<label class="col-sm-2 control-label"  style=" padding-right:0px;">วันที่รายการบริจาคหมดอายุ</label>
									<div class="col-sm-2"  style="padding-left:0px; padding-right:0px;">
										<input class="form-control" name="date_start" id="date_start" value="<?php echo revert_date($dateStart); ?>"  placeholder="Date Start" type="text">
									</div>                                          
									<label class="col-sm-1 control-label"  style=" text-align:center;"> ถึงวันที่ </label>
									<div class="col-sm-2"  style="padding-left:0px; padding-right:0px;">
										<input class="form-control" name="date_stop" id="date_stop" value="<?php echo revert_date($dateStop); ?>" placeholder="Date Stop" type="text">
									</div>
								 	<label class="col-sm-5 control-label">

										<!-- <a  class="btn btn-info" onclick="ckSave();"><i class="fa fa-search">&nbsp;เลือก</i></a>&nbsp;&nbsp;
										 -->

										<a id="bt-sync" style="" class="btn btn-primary pull-right" onclick="ckSave();"><i class="fa fa-refresh">&nbsp;&nbsp;อัพเดท</i></a>
									</label>  
								</div>
							</form>

							<div class="col-sm-2 form-group row">						
								<span>Process</span>
								<div id="progress"></div>
	  							<div id="message"></div>						
							</div>

<!-- 
							<div class="progress">
							    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%">
							      40%
							    </div>
							</div>
 -->

							<div class="clear"></div>
						</div>
					</div>				
				</div>
			</div>	
		</div>
	</div> 
</div>

<?php include ('inc/js-script.php') ?>
<script>
$(document).ready(function() {
	$("#date_start").datepicker({language:'th-th',format:'dd-mm-yyyy'});
	$("#date_stop").datepicker({language:'th-th',format:'dd-mm-yyyy'});	
	oTable = $("#tbEmp").dataTable({
		"sDom": 'T<"clear">lfrtip',
		"oLanguage": {
			"sInfoEmpty": "",
			"sInfoFiltered": ""
		},
		"oTableTools": {
			"aButtons":  ""
		},
		"bProcessing": true,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "asc" ]],
		'iDisplayLength': 15,
		"aLengthMenu" : [10, 15, 25, 50, 100 , 1000]
	});

});

var timer;

function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}
function ckSave(){
	// $("#frmMain").submit();
	var url = "<?php echo $url_update; ?>";
	var payment_bank_id = "<?php echo $payment_bank_id; ?>";
	// $.ajax({url: "process.php"});
	$.ajax({
        // url: 'data/update-donation-register.php',
        url: url,
        data: {
            type: 'update_donation_register',
            date_start: $("#date_start").val(),
            date_stop: $("#date_stop").val(),
            payment_bank_id: payment_bank_id,
            //person_id: id,
        },
        type: 'POST',
        dataType: 'text/html',
        // dataType: 'json',
        success: function(data){
            // console.log(data);
            // location.reload();
        }//end success
    });
  	timer = window.setInterval(refreshProgress, 100);
  	// Refresh the progress bar every 1 second.
}//end func  


// The function to refresh the progress bar.
function refreshProgress() {
	var url = "data/check-process.php";
	var payment_bank_id = "<?php echo $payment_bank_id; ?>";
	// alert(url);
	$.ajax({
        url: url,
        data: {
            type: 'check_process',
            payment_bank_id: payment_bank_id,
            //person_id: id,
        },
        type: 'POST',
        // dataType: 'text/html',
        dataType: 'json',
        success: function(data){
            // console.log(data);

			$("#progress").html('<div class="bar" style="width:' + data.percent + '%"></div>');
			$("#message").html(data.message);
			// If the process is completed, we should stop the checking process.
			if (data.percent >= 100) {
				window.clearInterval(timer);
				// timer = window.setInterval(completed, 1000);
				completed();
			}
        }//end success
    });
  	// Refresh the p
/*
	$("#progress").html('<div class="bar" style="width:' + data.percent + '%"></div>');
	$("#message").html(data.message);
	// If the process is completed, we should stop the checking process.
	if (data.percent >= 100) {
		window.clearInterval(timer);
		// timer = window.setInterval(completed, 1000);
		completed();
	}
*/
}//end func
 
function completed() {
  $("#message").html("Completed 100%");
  window.clearInterval(timer);
}
 
// When the document is ready
// $(document).ready(function(){
  // Trigger the process in web server.
  // $.ajax({url: "process.php"});
  // Refresh the progress bar every 1 second.
  // timer = window.setInterval(refreshProgress, 1000);
// });


</script>