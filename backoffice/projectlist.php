<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;

$righttype_id = $_SESSION["login"]["info"]["righttype_id"];

?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
					<div class="page-head">

						<?php if ( $righttype_id==1 ) { ?>
						<button class="btn btn-success btn-small pull-right" onclick="addnew()" style="margin-top:10px;"><i class="fa fa-plus"></i> เพิ่มโครงการบริจาค </button>
						<?php }//end if ?>
												
						 	<h3><i class="fa fa-list"></i> &nbsp;โครงการบริจาค</h3>
						
					</div>
					<table id="tbPro" class="table" style="width:100%">
						  <col width=3>
						  <col width=70>
						  <col width=30>
						  <col width=20>
						  <col width=10>

						  <col width=10>
						  <col width=30>
						  <thead>
							  <tr>
								  <th style="" width="5%">ลำดับ</th>
								  <!-- <th style="width: 50px;">project_id</th>
								  <th style="width: 50px;">code_project</th> -->
								  <!-- <th width="11%">คำนำหน้าชื่อ</th> -->
								  <th style="" width="30%">ชื่อภาษาไทย</th>
								  <th style="" width="30%">ชื่อภาษาอังกฤษ</th>
								  <th style="" width="10%">บริจาคขั้นต่ำ(บาท)</th>
								  <th style="" width="10%">บริจาคขั้นสูงสุด(บาท)</th>
								  <th style="" width="10%">ยอดบริจาคขอรับใบเสร็จ(บาท)</th>
								  <!-- <th width="">E-mail</th> -->
								  <th style="" width="10%">Manage</th>
							  </tr>
						  </thead>   
						<tbody>
						</tbody>
					</table>
					<br><br>
				</div>
			</div>

		</div>
	</div> 
</div>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
$(document).ready(function() {
	var oTable;
	$("#frmMain").validate();
	listItem();
});

function listItem(){
   var url = "data/projectlist.php";
   oTable = $("#tbPro").dataTable({
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
   	   "sInfoEmpty": "",
   		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons":  ""
		},
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({"name":"menu_id","value":$("#srchMenuId").val()});
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
		}
   }); 
}

function editInfo(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&project_id="+id+"&type=info";
   redirect(url);
}
function addnew(){
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=info";
   redirect(url);
}
function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}

</script>