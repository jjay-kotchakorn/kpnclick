<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db, $RIGHTTYPEID, $EMPID;

$righttype_id = $_SESSION["login"]["info"]["righttype_id"];

?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		
		<div class="col-md-12">  		
			<img src="./images/logo-fa.png" alt="">
		</div>

		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
					<div class="page-head">
						<?php if ( $righttype_id==1 ) { ?>

						<button class="btn btn-success btn-small pull-right" onclick="addnew()" style="margin-top:10px;"><i class="fa fa-plus"></i> เพิ่มช่องทางบริจาค</button>
						<?php } ?>
						
						<h3><i class="fa fa-users"></i> &nbsp;ช่องทางรับบริจาค</h3>
					</div>
					<table id="tbbank" class="table" style="width:100%">
						<thead>
							<tr>
								<th style="text-align:center" width="5%">ลำดับ</th>
								<th  style="text-align:center" width="30%">รายชื่อช่องทางบริจาค</th>
								<th  style="text-align:center" width="10%">บริจาคเงินภายใน (วัน)</th>

								<!-- <th width="30%">ชื่อบริษัท(อังกฤษ)</th> -->
								<th  style="text-align:center" width="10%">Active</th>
								<th  style="text-align:center" width="20%">Manage</th>
								<th  style="text-align:center" width="10%">หมายเหตุ</th>
							</tr>
						</thead>   
						<tbody>
						</tbody>
					</table>
					<br><br>
				</div>
			</div>

		</div>
	</div> 
</div>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
	$(document).ready(function() {
		var oTable;
		$("#frmMain").validate();
		listItem();
	});

	function listItem(){
		var url = "data/banklist.php";
		oTable = $("#tbbank").dataTable({
			"sDom": 'T<"clear">lfrtip',
			"oLanguage": {
				"sInfocompanyty": "",
				"sInfoFiltered": ""
			},
			"oTableTools": {
				"aButtons":  ""
			},
			"bProcessing": true,
			"bServerSide": true,
			"sAjaxSource": url,
			"sPaginationType": "full_numbers",
			"aaSorting": [[ 0, "desc" ]],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				aoData.push({"name":"menu_id","value":$("#srchMenuId").val()});
				$.ajax( {
					"dataType": 'json', 
					"type": "POST", 
					"url": sSource, 
					"data": aoData, 
					"success": fnCallback
				});
			}
		}); 
	}

	function addnew(){
		var url = "index.php?p=<?php echo $_GET["p"];?>&type=info";
		redirect(url);
	}
	function btn_update(id){
		if(typeof id=="undefined") return;
		var url = "index.php?p=donation&payment_bank_id="+id+"&type=donation-update";
		// alert(url);
		redirect(url);
	}
	function btn_list(id){
		if(typeof id=="undefined") return;
		var url = "index.php?p=donation&payment_bank_id="+id+"&type=donation-list";
		redirect(url);
	}
	function editInfo(id){
		if(typeof id=="undefined") return;
	   var url = "index.php?p=donation&payment_bank_id="+id+"&type=info";
	   redirect(url);
	}

	function closeInfo(id){
		var okToRefresh = confirm("ยืนยันการลบ");
		if (okToRefresh){
			
			$.ajax({
				url: 'update-company.php',
				data: {
					type: 'del',
					company_id: id,
		            //person_id: id,
		        },
		        type: 'POST',
		        dataType: 'text/html',
		        success: function(data){
		            // console.log(data);
		            location.reload();
		        }
		    });
			setTimeout("location.reload(true);",500);
		}//end if
	}

	function reCall(){
		oTable.fnClearTable( 0 );
		oTable.fnDraw();
	}

</script>