<?php
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/menu.php";
include_once "./share/datatype.php";
global $db;
$department = datatype(" and a.active='T'", "department", true);
$membertype = datatype(" and a.active='T'", "membertype", true);

$table = $_GET["table"] ? $_GET["table"] : "course";
$checkBox = $_GET["ckBox"] ? "T" : "";
$retFunc = $_GET["retFunc"] ? $_GET["retFunc"] : "retInfo";
$con = $_GET["con"] ? trim($_GET["con"]) : "";
$childlist = $_GET["childlist"] ? trim($_GET["childlist"]) : "";
global $db;

?>
<!DOCTYPE html>
<html lang="en">
<?php include ('inc/header.php'); ?>
<body>

        <div class="box-content"> 
                 
            <div class="alert search-header" style="padding:10px;">
                <button onClick="javascript:parent.$.fancybox.close();" class="close" type="button">×</button>
                &nbsp;&nbsp;<span style="font-size: 13px;line-height: 13px;width: 100px;display: inline-block;">ประเภทสมาชิก</span>&nbsp;&nbsp;                        
                <select name="sMembertype_id" id="sMembertype_id" class="form-control" onchange="disp();" style="width:130px;display:inline-block;">
                    <option value="">---- เลือก ----</option>
                    <?php foreach ($membertype as $key => $value) {
                        $id = $value['membertype_id'];
                        $name = $value['name'];
                        echo  "<option value='$id'>$name</option>";
                    } ?>

                </select>
                &nbsp;&nbsp;
                <span style="font-size: 13px;line-height: 13px;width:100px;display: inline-block;">ภาควิชา/หน่วยงาน</span>&nbsp;&nbsp;
                <select name="sDepartment_id" id="sDepartment_id" class="form-control" onchange="disp();" style="width:130px;display:inline-block;">
                    <option value="">---- เลือก ----</option>
                    <?php foreach ($department as $key => $value) {
                        $id = $value['department_id'];
                        $name = $value['name'];
                        echo  "<option value='$id'>$name</option>";
                    } ?>

                </select>&nbsp;&nbsp;
                <input class="form-control focused" id="sSearch" onKeyUp="disp();" style="width:140px;display:inline-block;" type="text" value="">               
                &nbsp;&nbsp;
                <select name="sActive" id="sActive" class="form-control" onchange="disp();" style="width:90px;display:inline-block;">
                    <option selected="selected" value="T">active</option>
                    <option value="F">inActive</option>
                </select>&nbsp;&nbsp;                            
                <a href="#" class="btn btn-rad btn-info" onClick="disp();"><i class="fa fa-search"></i></a>&nbsp;&nbsp;
                <button onClick="getKey();" id="ok" class="btn btn-rad btn-success" type="button" title="คลิกเพื่อเลือกรายการ">เลือก/OK</button>
            </div>
            <div style="overflow-y:scroll; height:100%; vertical-align:top;"> 
                <table width="100%" class="table">
                    <thead>
                        <tr class="alert alert-success" style="font-weight:bold;">
                            <td width="7%"><input data-no-uniform="true" type="checkbox" onClick="ckAll(this)" id="ckAll">&nbsp;</td>
                            <td width="13%">รหัส</td>
                            <td width="10%">คำนำหน้า</td>
                            <td width="15%">ชื่อ</td>
                            <td width="15%">นามสกุล</td>
                            <td width="20%">ภาควิชา/หน่วยงาน</td>
                            <td width="20%">ประเภทสมาชิก</td>                                  
                        </tr>
                    </thead>   
                </table>
            </div>
            <div style="overflow-y:scroll; height:285px; vertical-align:top; margin-top:-6px;">                      
                <table class="table table-striped" id="tbList">
                    <thead>
                        <tr style="cursor:pointer; " valign="middle" onClick="singleKey(this);">
                            <td width="7%"><input type="hidden" name="member_id" id="member_id">-</td>
                            <td width="13%" class="center" id="code">&nbsp;</td>
                            <td width="10%" class="center" id="prefix">&nbsp;</td>
                            <td width="15%" class="center" id="fname">&nbsp;</td>
                            <td width="15%" class="center" id="lname">&nbsp;</td>
                            <td width="20%" class="center" id="department_name">&nbsp;</td>
                            <td width="20%" class="center" id="membertype_name">&nbsp;</td>  
                        </tr>
                    </thead>
                    <tbody>
                        <tr style="cursor:pointer;" onClick="singleKey(this);">
                            <td width="7%"><input type="hidden" name="member_id" id="member_id">
                                &nbsp;</td>
                            <td width="13%" class="center" id="code"></td>
                            <td width="10%" class="center" id="prefix">&nbsp;</td>
                            <td width="15%" class="center" id="fname">&nbsp;</td>
                            <td width="15%" class="center" id="lname">&nbsp;</td>
                            <td width="20%" class="center" id="department_name"></td>
                            <td width="20%" class="center" id="membertype_name"></td>                                  
                        </tr>
                        <tr style="cursor:pointer;">
                            <td width="7%">
                                <input type="hidden" name="member_id" id="member_id">
                                <input type="checkbox" id="ckBox">&nbsp;&nbsp;</td>
                            <td width="13%" class="center" id="code"></td>
                            <td width="10%" class="center" id="prefix">&nbsp;</td>
                            <td width="15%" class="center" id="fname">&nbsp;</td>
                            <td width="15%" class="center" id="lname">&nbsp;</td>
                            <td width="20%" class="center" id="department_name"></td>
                            <td width="20%" class="center" id="membertype_name"></td>                                   
                        </tr>
                    </tbody>
                </table>   
            </div>                      
        </div>
        

<?php   include_once ('inc/js-script.php'); ?>
    <?php include ('inc/footer.php') ?>
<script type="text/javascript">
        var checkBox = "<?php echo $checkBox ?>";
        var mapField = "<?php echo $map; ?>";
        var con = "<?php echo $con; ?>";
        var childlist = "<?php echo $childlist; ?>"
        if (checkBox == "T") {
            var trList = $("#tbList tbody tr:eq(1)").clone();
        } else {
            $("#ok").hide();
            var trList = $("#tbList tbody tr:eq(0)").clone();
            $("#ckAll").hide();
        }

        (checkBox == "T") ? delCkRow("#tbList", "1") : delRow("#tbList");

        $(document).ready(function() {
         
            disp();
        });

        function disp() {
            (checkBox == "T") ? delCkRow("#tbList") : delRow("#tbList");
            var url = "data/member-search.php";
            var param = "";
            param = param + "&department_id=" + $("#sDepartment_id").val();
            param = param + "&membertype_id=" + $("#sMembertype_id").val();
            param = param + "&active=" + $("#sActive").val();
            param = param + "&othercon=" + con;
            param = param + "&search=" + $("#sSearch").val();
            param = param + "&childlist=" + childlist;
            if ($("#ck").is(":checked")) {
                param = param + "&all=T";
            }
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": url,
                "data": param,
                "success": function(data) {
                    (checkBox == "T") ? delCkRow("#tbList") : delRow("#tbList");
                    $.each(data, function(index, array) {
                        var ck = "";
                        $("#tbList tbody tr :input").each(function() {
                            var t = $(this).val();
                            var ckId = array.member_id;
                            if (ckId == t) {
                                ck = "1";
                            }
                        });
                        if (ck == "1")
                            return;
                        addTrLine("#tbList", trList, array);
                    });
                }
            });
        }


        function singleKey(tag) {
            var ret = new Array();
            var i = 0;
            ret[i] = new Array();
            ret[i]["member_id"] = $(tag).find('#member_id').val();
            ret[i]["code"] = $(tag).find('#code').text();
            ret[i]["prefix"] = $(tag).find('#prefix').text();
            ret[i]["fname"] = $(tag).find('#fname').text();
            ret[i]["lname"] = $(tag).find('#lname').text();
            ret[i]["membertype_name"] = $(tag).find('#membertype_name').text();
            ret[i]["department_name"] = $(tag).find('#department_name').text();
            parent.<?php echo $retFunc; ?>(ret);
            parent.$.fancybox.close();
        }

        function getKey() {
            var ret = new Array();
            var i = 0;
            $("#tbList tbody tr :checkbox").each(function() {
                if ($(this).is(":checked")) {
                    var tag = $(this).parent().parent();
                    ret[i] = new Array();
                    ret[i]["member_id"] = $(tag).find('#member_id').val();
                    ret[i]["code"] = $(tag).find('#code').text();
                    ret[i]["prefix"] = $(tag).find('#prefix').text();
                    ret[i]["fname"] = $(tag).find('#fname').text();
                    ret[i]["lname"] = $(tag).find('#lname').text();
                    ret[i]["department_name"] = $(tag).find('#department_name').text();
                    ret[i]["membertype_name"] = $(tag).find('#membertype_name').text();
                }
                i++;
            });
            parent.<?php echo $retFunc; ?>(ret);
            parent.$.fancybox.close();
        }

        function ckAll(tag) {
            $("#tbList tbody tr :checkbox").each(function() {
                if ($(tag).is(":checked")) {
                    $(this).attr('checked', true);
                } else {
                    $(this).attr('checked', false);
                }
            });
        }
    </script>   
</body>
</html>