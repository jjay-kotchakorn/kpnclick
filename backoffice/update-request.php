<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";

global $db;
global $EMPID;

if ($_POST["approve"] != "" && $_POST["request_id"] > 0) {
    $request_id = $_POST["request_id"];
    $state = $_POST["requeststate_id"];

    $args = array();
    $args["table"] = "request";
    $args["id"] = $_POST["request_id"];
    $app = $_POST["approve"];
    if($state==1 && $app=="T"){
    	// ยืม
    	$args["requeststatus_id"] = 2;
    	$args["requeststate_id"] = 2;
    	if($args["requeststatus_id"]==2){
    		$args["requestapprove_id"] = $EMPID;
        	$args["requestapprovetime"] = date("Y-m-d H:i:s");
            $db->query("update room set roomstatus_id=2 where room_id={$_POST["room_id"]}");
    	}  
    }else if($state==2){
    	if($app=="F"){
    		$args["requestapprove_id"] = "";
        	$args["requestapprovetime"] = "";	
	    	$args["requeststatus_id"] = 1;
	    	$args["requeststate_id"] = 1;
            $db->query("update room set roomstatus_id=1 where room_id={$_POST["room_id"]}");
    	}else if($app=="T"){
			$args["requeststatus_id"] = 3;
			$args["requeststate_id"] = 3;
    		$args["returnapprove_id"] = $EMPID;
        	$args["returnapprovetime"] = date("Y-m-d H:i:s");
            $args["date_return"] = thai_to_timestamp($_POST["date_return"])." ".$_POST["date_return_time"];
            $db->query("update room set roomstatus_id=1 where room_id={$_POST["room_id"]}");            
    	}
    }else if($state==3 && $app=="F"){
        $args["date_return"] = "";
		$args["requestapprove_id"] = "";
    	$args["requestapprovetime"] = "";    		
		$args["requeststatus_id"] = 2;
		$args["requeststate_id"] = 2;
        $args["returnapprove_id"] = $EMPID;
        $args["returnapprovetime"] = date("Y-m-d H:i:s");
        $db->query("update room set roomstatus_id=2 where room_id={$_POST["room_id"]}");   	
    }
    $db->set($args);
} else if ($_POST["del"] == "T" && $_POST["request_id"] > 0) {
    $args = array();
    $args["table"] = "request";
    $args["id"] = $_POST["request_id"];
    $args["requeststatus_id"] = 4;
    $args["requeststate_id"] = 4;
    $args["cancelby_id"] = $EMPID;
    $args["canceltime"] = date("Y-m-d H:i:s");
    $args["active"] = "F";
    $db->set($args);
    $request_id = $args["id"];
} else if($_POST){
	$args = array();
	$args["table"] = "request";
    if ($_POST["request_id"]) {
        $args["id"] = $_POST["request_id"];
    } else {
        $y = date("Y");
        $q = "select max(runno) from request where runyear='$y'";
        $runno = $db->data($q) + 1;
        $args["runyear"] = $y;
        $args["runno"] = $runno;
        $args["docdate"] = date("Y-m-d H:i:s");
        $args["docno"] = "RQ{$y}/" . sprintf("%06d", $runno);
        $args["requeststatus_id"] = 1;
        $args["requeststate_id"] = 1;        
    }
	$args["dept_phone"] = $_POST["dept_phone"];
	$args["title"] = $_POST["title"];
	$args["date_start"] = thai_to_timestamp($_POST["date_start"])." ".$_POST["date_start_time"];
	$args["date_stop"] = thai_to_timestamp($_POST["date_stop"])." ".$_POST["date_stop_time"];
	$args["member_id"] = $_POST["member_id"];
	$args["room_id"] = (int)$_POST["room_id"];
	$args["servicecharge_id"] = (int)$_POST["servicecharge_id"];
	$args["servicecharge_detail"] = ($args["servicecharge_id"]==3) ? $_POST["servicecharge_detail"] : "";
    $args["detail"] = $_POST["detail"];
    $args["request_item"] = $_POST["request_item"];
    $args["request_camera"] = $_POST["request_camera"];
    $args["request_video"] = $_POST["request_video"];
    $args["request_audio"] = $_POST["request_audio"];
	$args["request_editing_video"] = $_POST["request_editing_video"];
	$args["recby_id"] = (int)$EMPID;
	$args["rectime"] = date("Y-m-d H:i:s");
   $ret = $db->set($args);
   $request_id = $args["id"] ? $args["id"] : $ret;
   $ck_ids = "";
	if(is_array($_POST["ckRequestDetail"])){
		foreach($_POST["ckRequestDetail"] as $index=>$v){
			$args = array();
			$args["table"] = "request_detail";
			if($_POST["request_detail_id"][$index])
				$args["id"] = $_POST["request_detail_id"][$index];
			else $args["request_id"] = (int)$request_id;
			$args["item_id"] = (int)$_POST["item_id"][$index];
			$args["amount"] = (int)$_POST["amount"][$index];
			$args["recby_id"] = (int)$EMPID;
			$args["rectime"] = date("Y-m-d H:i:s");
			if(!$args["id"] && $v=="F") continue;
			if($args["id"] && $v=="F"){
				$id = $args["id"];
				$q = "select request_detail_id from request_detail where request_detail_id=$id";
				$testId = $db->data($q);
				if($testId){
					$db->query("delete from request_detail where request_detail_id=$testId");
					continue;
				}
			}
            $ck_ids .= ",".$args["item_id"];
			$db->set($args);
		}

        $ck_ids = trim($ck_ids,",");
        $db->query("delete from request_detail where request_id=$request_id and item_id not in($ck_ids)");
	}
}
$_SESSION["success"]["msg"] = "บันทึกข้อมูลเรียบร้อยแล้ว";

$args = array();
$args["p"] = "request";
$args["request_id"] = $request_id;
$args["type"] = "info";
redirect_url($args);
?>