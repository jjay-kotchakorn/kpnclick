<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/member.php";
global $db;
$section = datatype(" and a.active='T'", "section", true);
$member_id = $_GET["member_id"];
$q = "select  prefix, fname, lname from member where member_id=$member_id";
$r = $db->rows($q);
$str = "";
if($r){
	$str = $r["prefix"]." ".$r["fname"]." ".$r["lname"];
}else{
	$str = "เพิ่มการสมัครทำงาน";
}

$department = datatype(" and a.active='T'", "department", true);
$membertype = datatype(" and a.active='T'", "membertype", true);
$university = datatype(" and a.active='T'", "university", true);
$educationlevel = datatype(" and a.active='T'", "educationlevel", true);
$bank = datatype(" and a.active='T'", "bank", true);

$q = "select university_id, educationlevel_id, receive_file_part, cart_no_file_part, bank_id from member where member_id=$member_id";
$r = $db->rows($q);
if($r){
	$educationlevel_id = $r["educationlevel_id"];
	$university_id = $r["university_id"];
	$bank_id = $r["bank_id"];
	$receive_file_part = $r["receive_file_part"];
	$cart_no_file_part = $r["cart_no_file_part"];
}
$str = ( $_GET['p']=="register") ? " สมัครงาน" : $str;

?>
<div id="cl-wrapper" style="<?php echo ($_GET['p']=="register") ? 'padding-top:0px;' : '' ?>">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
				  <div class="col-md-12">			      
					<div class="block-flat">
					  <div class="header">
					  	<ol class="breadcrumb">
					  		<?php if($_GET['p']=="register"): ?>
								<li><a href="#" onClick="clearPage('default');">หน้าหลัก</a></li>
							<?php else: ?>
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
							<?php endif; ?>
							<li class="active"><?php echo $str; ?></li>
						</ol>							
					  </div>
					  <div class="content">
						  <form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="update-member.php">
						  <input type="hidden" name="member_id" id="member_id" value="<?php echo $member_id; ?>">
						    <input type="hidden" name="cart_no_file_part_tmp" />
						    <input type="hidden" name="receive_file_part_tmp" />						    
						    <input type="hidden" name="del_receive_file_part" />
  							<input type="hidden" name="del_cart_no_file_part" />
  							<input type="hidden" name="mode" value="<?php echo  $_GET['p']; ?>" />
						  <div class="col-sm-12">
							  <div class="form-group row">		                
								<label class="col-sm-1 control-label" style="padding-left:0px; padding-right:0px;">คำนำหน้า</label>
								<div class="col-sm-2">
								  <input class="form-control" id="prefix" name="prefix" required="" placeholder="คำนำหน้า" type="text">
								</div>			                			                
								<label class="col-sm-1 control-label">ชื่อ</label>
								<div class="col-sm-2">
								  <input class="form-control" name="fname" id="fname" required="" placeholder="ชื่อ" type="text">
								</div>			                			                
								<label class="col-sm-1 control-label">นามสกุล</label>
								<div class="col-sm-2">
								  <input class="form-control" required="" name="lname" id="lname" placeholder="นามสกุล" type="text">
								</div>
								<label class="col-sm-1 control-label" style="padding-left:0px; padding-right:0px;">ชื่อเล่น</label>
								<div class="col-sm-2">
								  <input name="nickname" type="text" placeholder="ชื่อเล่น" class="form-control" id="nickname">
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-sm-1 control-label" style="padding-left:0px; padding-right:0px;">ID Card No.</label>
								<div class="col-sm-2">
								  <input class="form-control" required="" name="cid" id="cid" placeholder="ID Card No." type="text">
								</div>
								<label class="col-sm-1 control-label">มือถือ</label>
								<div class="col-sm-2">
								  <input class="form-control" required="" name="phone" id="phone" placeholder="เบอร์มือถือ" type="text">
								</div>
								<label class="col-sm-1 control-label">E-Mail</label>
								<div class="col-sm-2">
								  <input class="form-control" name="email" required="" id="email" placeholder="Enter a valid e-mail" type="email">
								</div>
								<label class="col-sm-1 control-label">Line</label>
								<div class="col-sm-2">
								  <input class="form-control" name="line" required="" id="line" placeholder="Line">
								</div>
							  </div>
							  <div class="form-group row">
									<label class="col-sm-2 control-label">ชื่อสถาบันการศึกษา</label>
									<div class="col-sm-3">
										<select id="university_id" name="university_id" class="required select2"  style="float:inherit;">
											<option value="">-เลือก-</option>
											<?php foreach ($university as $key => $value) {
												$id = $value['university_id'];
												$name = $value['name'];
												$sl = ($university_id==$id) ? 'selected="selected"' : '';
												echo  "<option value='$id' {$sl}>$name</option>";
											} ?>
										</select>
									</div>
									<label class="col-sm-2 control-label">ระดับชั้นที่กำลังศึกษา</label>
									<div class="col-sm-3">
										<select id="educationlevel_id" name="educationlevel_id" class="required select2"  style="float:inherit;">
											<option value="">-เลือก-</option>
											<?php foreach ($educationlevel as $key => $value) {
												$id = $value['educationlevel_id'];
												$name = $value['name'];
												$sl = ($educationlevel_id==$id) ? 'selected="selected"' : '';
												echo  "<option value='$id' {$sl}>$name</option>";
											} ?>
										</select>
									</div>
								</div>
							  <div class="form-group row">
									<label class="col-sm-1 control-label"  style="padding-left:0px; padding-right:0px;">Bank Name</label>
									<div class="col-sm-3">
										<select id="bank_id" name="bank_id" class="required select2"  style="float:inherit;">
											<option value="">-เลือก-</option>
											<?php foreach ($bank as $key => $value) {
												$id = $value['bank_id'];
												$name = $value['name'];
												$sl = ($bank_id==$id) ? 'selected="selected"' : '';
												echo  "<option value='$id' {$sl}>$name</option>";
											} ?>
										</select>
									</div>
									<label class="col-sm-1 control-label"  style="padding-left:0px; padding-right:0px;">Account No.</label>
									<div class="col-sm-2">
									  <input class="form-control" name="bank_no" required="" id="bank_no" placeholder="Account No.">
									</div>
									<label class="col-sm-2 control-label"  style="padding-left:0px; padding-right:0px;">คนที่แนะนำให้มาทำงานกับ OJ</label>
									<div class="col-sm-3">
									  <input class="form-control" name="guide" required="" id="guide" placeholder="คนที่แนะนำให้มาทำงานกับ OJ">
									</div>
								</div>

								<div>
									<h4>วันที่สามารถทำงานได้</h4>
								</div>
									<table class="no-border" id="tbList">
										<thead>
											<tr class="no-border">
												<th width="6%">ลำดับ</th>
												<th width="">วันที่</th>
												<th width="15%">ช่วงเวลา</th>
												<th width="25%">Remark</th>
												<th width="40%" style="display:none;">Records/Time</th>
											</tr>
										</thead>   
										<tbody class="no-border-x no-border-y">
											<tr>
												<td><input name="ckbox[]" type="checkbox" checked id="ckbox" value="T">&nbsp;&nbsp;<span id="runNo"></span></td>
												<td class="center">
													<input name="work_time_id[]" type="hidden" id="work_time_id">
													<input name="work_time_type[]" type="hidden" id="work_time_type" value="1">
													<div class="row">
														<div class="col-md-5">													
															<input class="form-control focused s_date_start" name="date_job_start[]" id="date_job_start" type="text" value="">
														</div>
													</div>
												</td>   
												<td class="center">													
													<select name="time_job[]" id="time_job" class="form-control ">
														<option selected="selected" value="เช้า">เช้า</option>
														<option value="บ่าย">บ่าย</option>
														<option value="ทั้งวัน">ทั้งวัน</option>
													</select>
												</td>
												<td class="center">
													<textarea class="form-control focused" name="remark[]" id="remark"></textarea></td>
												</tr>
											<tr>
												<td><input name="ckbox[]" type="checkbox" checked id="ckbox" value="T">&nbsp;&nbsp;<span id="runNo"></span></td>
												<td class="center">
													<input name="work_time_id[]" type="hidden" id="work_time_id">
													<input name="work_time_type[]" type="hidden" id="work_time_type" value="2">
													<div class="row">
														<div class="col-md-5"><input class="form-control focused s_date_start" name="date_job_start[]" id="date_job_start" type="text" value=""></div>
														<div class="col-md-1">-</div>
														<div class="col-md-5"> <input class="form-control focused s_date_stop" name="date_job_stop[]" id="date_job_stop" type="text" value=""></div>
													</div>												
												</td>   
												<td class="center">													
													<select name="time_job[]" id="time_job" class="form-control ">
														<option selected="selected" value="เช้า">เช้า</option>
														<option value="บ่าย">บ่าย</option>
														<option value="ทั้งวัน">ทั้งวัน</option>
													</select>
												</td>
												<td class="center">
													<textarea class="form-control focused" name="remark[]" id="remark"></textarea></td>
												</tr>
											</tbody>
										</table> 

									</div>
					
								<hr>
							<div class="col-md-3">
							<br>
								<button type="button" class="btn " onClick="addItem();"><span ><strong>+</strong>&nbsp;เพิ่มวันเดี่ยว</span></button> 
								<button type="button" class="btn " onClick="addItemM()();"><span ><strong>+</strong>&nbsp;เพิ่มวันที่เป็นช่วงเวลา</span></button> 
							</div>
							<div class="clear"></div>
							<br>
						  
							<div class="col-sm-12">
								<div class="form-group">							  		
									<label class="col-sm-2 control-label">สำเนาบัตรประชาชน</label>
									<div class="col-sm-4">
										<?php
											
											$cart_no_file_part = $r["cart_no_file_part"];
											if($cart_no_file_part!=""){
												echo '<a target="_blank" href="'.$cart_no_file_part.'" style="padding-top: 7px; margin-top: 0px; margin-bottom: 0px; position: relative; display: inline-block;"><i class="fa fa-paperclip"></i> ดาวน์โหลด</a>';
												echo '&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:removefile(1);" style="padding-top: 7px; margin-top: 0px; margin-bottom: 0px; position: relative; display: inline-block;"><i class="fa fa-times"></i> ลบ</a>';
											}else{
												?>
													<input type="file" name="cart_no_file" id="cart_no_file" >							
												<?php

											}
										?>
								  	</div>							  		
									<label class="col-sm-2 control-label">ใบสำคัญรับเงิน</label>
									<div class="col-sm-4">
										<?php
											$receive_file_part = $r["receive_file_part"];											
											if($receive_file_part){
												echo '<a target="_blank"  href="'.$receive_file_part.'" style="padding-top: 7px; margin-top: 0px; margin-bottom: 0px; position: relative; display: inline-block;"><i class="fa fa-paperclip"></i> ดาวน์โหลด</a>';
												echo '&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:removefile(2);" style="padding-top: 7px; margin-top: 0px; margin-bottom: 0px; position: relative; display: inline-block;"><i class="fa fa-times"></i> ลบ</a>';
											}else{
												?>
																			
													<input type="file" name="receive_file" id="receive_file">							
												<?php

											}
										?>
								  	</div>
						  		</div>
							</div>
						  
							<div class="clear"></div>
							<br>
							<hr>
							<?php if( $_GET['p']=="register"): ?>
								<span>กรุณากรอกรหัสที่ท่านเห็น </span>
								<div id="secure_validate">
									<div class="row">
										<div class="col-md-2">											
											<input class="required captcha form-control"  type="text" id="f_txt_secure" name="f_txt_secure">
										</div>
										<div class="col-md-1">
											<img src="./imglib/img.inc.php?numbers=<?php
											function rand_string($len, $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
											{
												$string = '';
												for ($i = 0; $i < $len; $i++)
												{
													$pos = rand(0, strlen($chars)-1);
													$string .= $chars{$pos};
												}
												return $string;
											}
											$my_rand = rand_string(4);
											echo $my_rand;
											?>" style="margin-bottom: -11px;" />											
										</div>
										<div class="reload col-md-1" style="cursor:pointer; font-size:25px;line-height:30px;" onclick="newValidate();"><i class="fa fa-refresh"></i></div>
									</div>
									<input id="ch_validation" name="ch_validation" type="hidden" value="want_ch" />
									<input type="hidden" value="<?php echo $my_rand; ?>" id="captcha">
									<script language="javascript" type="text/javascript">
										function ChkCus_valid(){
											if(document.getElementById("f_txt_secure").value != "<?php echo $my_rand; ?>" ) {
												document.getElementById("ch_validation").value = "no";
												return "ErrCaptcha";	
											}
											document.getElementById("ch_validation").value = "yes";
											return "DoneCaptcha";
										}

										function newValidate(){
											$.ajax({
												url: "register-validate.php",
												cache: false
											})
											.done(function( html ) {
												$( "#secure_validate" ).html( html );
											});
										}
									</script>
								</div>                                
							</div>	
							<?php endif; ?>
						  <div class="form-group row">
								<div class="col-md-12">
									<?php $str = ( $_GET['p']=="register") ? "สมัครงาน" : "บันทึกข้อมูล"; ?>
									<button type="button" class="btn btn-primary" onClick="ckSave()"><?php echo $str; ?></button>
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">ยกเลิก</button>
								</div>
						  </div>
						</form>
				  			<div class="clear"></div>
					  </div>
				  	  <div class="clear"></div>
					</div>
				  <div class="clear"></div>
				  </div>
				  <div class="clear"></div>
				</div>
					<div class="clear"></div>

		</div>
		<div class="clear"></div>
	</div> 
	<div class="clear"></div>
</div>
<?php include_once ('inc/js-script.php'); ?>
<div id="custom_script">
	
</div>
<script type="text/javascript">
  var worksdates = ["12-10-2015", "12-11-2015", "12-10-2015", "13-10-2015"];
  $(document).ready(function() {
   $("#frmMain").validate();
   var member_id = "<?php echo $_GET["member_id"]?>";
   if(member_id) viewInfo(member_id);
	$("#tbList").on('focus' ,'.s_date_start', function(event) {
		//$(this).datepicker({language:'th-th',format:'dd-mm-yyyy', beforeShowDay: DisableSpecificDates});
		$(this).datepicker({language:'th-th',format:'dd-mm-yyyy'});
	});
	$("#tbList").on('focus' ,'.s_date_stop', function(event) {
		//$(this).datepicker({language:'th-th',format:'dd-mm-yyyy', beforeShowDay: DisableSpecificDates});
		$(this).datepicker({language:'th-th',format:'dd-mm-yyyy'});
	});
	<?php if($_GET["p"]=="register") : ?>

		$.validator.addMethod( "captcha", function( value, element ) {
			var captcha = $("#captcha").val(); 
			return this.optional( element ) || value==captcha;
		}, "รหัสตรวจสอบความปลอดภัย ไม่ถูกต้อง" );

	<?php endif; ?>
 });

function DisableSpecificDates(date) {
 
 var m = date.getMonth();
 var d = date.getDate();
 var y = date.getFullYear();
 
 // First convert the date in to the mm-dd-yyyy format 
 // Take note that we will increment the month count by 1 
 var currentdate = d+ '-'+ (m + 1) +'-' + y ;

  // We will now check if the date belongs to worksdates array 
 for (var i = 0; i < worksdates.length; i++) {
 // Now check if the current date is in disabled dates array. 
 if ($.inArray(currentdate, worksdates) == -1 ) {

 return false;
 }
 }
 
}

var trList = $("#tbList tbody tr:eq(0)").clone();
var trListM = $("#tbList tbody tr:eq(1)").clone();
delRow("#tbList");

function addItem(){
	var t = addTrLine("#tbList", trList);
	$(t).find("#runNo").text($("#tbList tbody tr").length);
	var no = $("#tbList tbody tr").length;

}

function addItemM(){
	var t = addTrLine("#tbList", trListM);
	$(t).find("#runNo").text($("#tbList tbody tr").length);
	var no = $("#tbList tbody tr").length;

}

function readURL(input) {
	 if (input.files && input.files[0]) {
	  var reader = new FileReader();
	  reader.onload = function(e) {
	   $('#img').attr('src', e.target.result);
	 }
	 reader.readAsDataURL(input.files[0]);
	}
}
 function viewInfo(member_id){
   if(typeof member_id=="undefined") return;
   getmemberInfo(member_id);
   dispWorkList(member_id);
}

function dispWorkList(id){
	delRow("#tbList");
   var url = "data/work-time-list.php";
	var param = "member_id="+id;
	$.ajax( {
		"dataType":'json', 
		"type": "POST", 
		"url": url,
		"data": param, 
		"success": function(data){	
			 $.each(data, function(index, array){
				 array.name = array.menuname;
				 if(array.work_time_type==1){
             		addTrLine("#tbList", trList, array, "runNo");    
				 }else{
             		addTrLine("#tbList", trListM, array, "runNo");    

				 }
			 });				 
		}
	});
}

function getmemberInfo(id){
	if(typeof id=="undefined") return;
	var url = "data/memberlist.php";
	var param = "member_id="+id+"&single=T";
	dataUrl(url, param,"#frmMain");
}

function removeImg(){
  var defaultImg = "images/no-avatar-male.jpg";
  if($('input[name=tmpimg]').val()=="")return;
  var t = confirm("ลบรูปภาพ");
  if(!t) return;
  $('#img').attr('src', defaultImg);
  $('input[name=delimg]').val("T");	
  ckSave();
}

function removefile(type){
  var defaultImg = "images/no-avatar-male.jpg";
  if($('input[name=tmpimg]').val()=="")return;
  var t = confirm("ยืนยันการลบไฟล์");
  if(!t) return;
  if(type==1)
  	$("input[name=del_cart_no_file_part]").val("T");
  else
  	$("input[name=del_receive_file_part]").val("T");

  ckSave();
}
function ckSave(id){
  onCkForm("#frmMain");
  $("#frmMain").submit();
}	
</script>

