<?php
function get_datatable_icon($type, $id="", $showtext=true){
	if($type=="" || $id=="") return;

	if($type=="view" || $type==1){
	   $text = ($showtext) ? "View" : "";
	   return $txtEdit = '<a class="btn btn-success" title="ดูรายละเอียด" onClick="viewInfo(\''.$id.'\')">
				      <i class="fa fa-search"></i> '.$text.' </a>';
	}else if($type=="edit" || $type==2){
		$text = ($showtext) ? "แก้ไข" : "";
	   return $txtEdit = '<a class="btn btn-info" title="แก้ไข" onClick="editInfo(\''.$id.'\')">
				      <i class="fa fa fa-edit"></i> '.$text.' </a>';
	}else if($type=="trash" || $type==3){
		$text = ($showtext) ? "ลบข้อมูล" : "";
	   return $txtEdit = '<a class="btn btn-danger" title="ลบข้อมูล" onClick="delInfo(\''.$id.'\')">
				      <i class="fa fa-trash"></i> '.$text.' </a>';
	}else if($type=="report" || $type==4){
		$text = ($showtext) ? "รายงาน" : "";
	   return $txtEdit = '<a class="btn btn-info" title="รายงาน" onClick="editInfo(\''.$id.'\')">
				      <i class="fa fa-edit"></i> '.$text.' </a>';
	}else if($type=="save" || $type==5){
		$text = ($showtext) ? "บันทึก" : "";
	   return $txtEdit = '<a class="btn btn-primary" title="บันทึก" onClick="saveInfo(\''.$id.'\')">
				      <i class="fa fa-save"></i> '.$text.' </a>';
	}else if($type=="close" || $type==6){
		$text = ($showtext) ? "ลบข้อมูล" : "";
	   return $txtEdit = '<a class="btn btn-danger" title="ลบข้อมูล" onClick="closeInfo(\''.$id.'\')">
				      <i class="fa fa-times"></i> '.$text.' </a>';
	}
}
function thai_to_timestamp($date, $time=false){
	if(!$date) return 'NULL';
	$a = explode(" ",$date);
	$r = str_replace("/", "-", $a[0]);
	$arr_data = explode("-", $r);
	$arr_data[2] = ($arr_data[2]>2400) ? $arr_data[2]-543 : $arr_data[2];

	if($time==true){
		$t = (strlen($a[1])>5) ? substr($a[1], 0, 5) : $a[1];
		$d = $arr_data[2]."-".$arr_data[1]."-".$arr_data[0]." ".$t;
	}else{
		$d = $arr_data[2]."-".$arr_data[1]."-".$arr_data[0];
	}//end else
	return $d ;
}
function revert_date($date, $time=false){
	if(!$date or $date=="" or $date=="0000-00-00 00:00:00")return "";
	if(strlen($date)>19){
		$date = substr($date, 0, 19);
	}
	$a = explode(" ",$date);
	$arr_data = explode("-", $a[0]);
	$arr_data[0] = $arr_data[0]+543;
	$t = "";
	if(strlen($a[1])>5){
		$t = substr($a[1], 0, 5);
	}
	if($time==true)
		$d = sprintf("%02d-%02d-%04d", $arr_data[2], $arr_data[1], $arr_data[0])." ".$t;
	else
		$d = sprintf("%02d-%02d-%04d", $arr_data[2], $arr_data[1], $arr_data[0]);
	return $d ;
}
function set_comma($num, $r=2){
	$sign = ($num<0) ? "-" : "";
	$num = abs($num);
	$num = round(str_replace(",", "", "$num"), $r)."";
	$point = "".round( ( ($num - ((int)$num))*pow(10,$r) ), 0);
	for(;strlen($point)<$r;)
		$point = "0".$point;
	$num = ( (int)$num )."";
	$l = strlen($num);
	$ret = "";
	$c = 0;
	for($i=$l; $i>=1; $i--){
		$c++;
		$ret = $num[$i-1].$ret;
		if($i!=1 && $c==3){
			$ret = ",".$ret;
			$c = 0;
		}
	}
	$ret =  $ret.".$point";
	return ($ret>0 ? $sign : "").$ret;
}
function get_age($birthdate, $str=false, $y=true){
	$aD = array();
	if(!$birthdate) return $aD;
	if(strlen($birthdate)>19){
	   $birthdate = substr($birthdate, 0, 19);
	}
	if(strlen($birthdate)>10){
		$list = explode(" ", $birthdate);
		$birthdate = $list[0];
	}
	$today = date("Y-m-d");
	list($b_year, $b_month, $b_day)= explode("-", $birthdate);
	list($t_year, $t_month, $t_day)= explode("-", $today);
	$m_birthdat_y = mktime(0, 0, 0, $b_month, $b_day, $b_year);
	$m_now = mktime(0, 0, 0, $t_month, $t_day, $t_year);
	$m_age = ($m_now - $m_birthdat_y);
	$arr_data[0] = date("Y", $m_age)-1970;
	$arr_data[1] = date("m", $m_age)-1;
	$arr_data[2] = date("d", $m_age)-1;
	if($str==true){
		$str  = $arr_data[0] ? $arr_data[0]." ปี " : "";
		if($y==true) return $str;
		$str .= $arr_data[1] ? $arr_data[1]." เดือน " : "";
		$str .= $arr_data[2] ? $arr_data[2]." วัน " : "";
		return $str;
	}
	return $arr_data;
}
$thai_day_arr=array("อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์");
$thai_month_arr=array(
		"0"=>"",
		"01"=>"มกราคม",
		"02"=>"กุมภาพันธ์",
		"03"=>"มีนาคม",
		"04"=>"เมษายน",
		"05"=>"พฤษภาคม",
		"06"=>"มิถุนายน",
		"07"=>"กรกฎาคม",
		"08"=>"สิงหาคม",
		"09"=>"กันยายน",
		"10"=>"ตุลาคม",
		"11"=>"พฤศจิกายน",
		"12"=>"ธันวาคม"
);
function date_thai($time, $str=false){
	global $thai_day_arr,$thai_month_arr;
	$thai_date_return = array();
	$thai_date_return[0]= $thai_day_arr[date("w",$time)];
	$thai_date_return[1]= date("d",$time);
	$thai_date_return[2]= $thai_month_arr[date("m",$time)];
	$thai_date_return[3]= (date("Y",$time)+543);
	if($str===true){
		return $thai_date_return[1]." ".$thai_date_return[2]." ".$thai_date_return[3];
	}	 
	return $thai_date_return;
}
function convert_mktime($input){
	if(!$input) return;
	$list = explode(" ",$input);
	$data = explode("-",$list[0]);
	$y = $data[0];
	$m = $data[1];
	$d = $data[2];
	$data2 = explode(':',$list[1]);
	$h = $data2[0];
	$mm = $data2[1];
	$s = $data2[2];
	$value = mktime($h,$mm,$s,$m,$d,$y);
	return $value;
}
function get_num_day($start,$stop){
	$start = convert_mktime($start);
	$stop = convert_mktime($stop);
	$time = $stop - $start;
	$full_day = (int) ( $time/(24*60*60) );
	$remain_hour = ($time - $full_day*(24*60*60))/(60*60);
	$hour=23;
	if($hour && $remain_hour>$hour) $full_day++;
	return $full_day;
}
function get_page_url() {
   return substr($_SERVER["REQUEST_URI"],strrpos($_SERVER["REQUEST_URI"],"/")+1);
}
function get_config($name){
   if(isset( $_SESSION['cache']['configSetting'][$name])){
      return $_SESSION['cache']['configSetting'][$name];
   }else{
      global $db;
      if($db){
         $q = "select data_value from config where name='$name'";
         $data = $db->data($q);
         if(!isset($data)){
            $fd = fopen("./tmp/e_configError.log", "a+");
            fwrite($fd, "Config Name '$name' Not in Table config File {$_SERVER["SCRIPT_FILENAME"]}\n");
            fclose($fd);
         }
         $_SESSION['cache']['configSetting'][$name] = $data;
      }
   }
   return $data;
}
function log_file($str, $file=""){
   if(!$file) $file = "./tmp/z_LogFileDebug.log";
   if(is_array($str)){
      $a = $str;
      $str = "";
      foreach($a as $k=>$v)
         if(!is_numeric($k))
         $str = $str."$k = > $v \n";
      $str = $str."\n";
   }
   fwrite( fopen($file, "a+"), "\n\n$str\n");
}
function gen_array_post($array){
   if(!is_array($array)) return;
   echo "<pre>";
   foreach($array as $k=>$v){
      echo '$args["',$k,'"] = $_POST["',$k,'"];<br>';
   }
   echo"</pre>";
}
function ck_mobile(){
   if(preg_match('/(alcatel|amoi|android|avantgo|blackberry|benq|cell|cricket|docomo|elaine|htc|iemobile|iphone|ipad|ipaq|ipod|j2me|java|midp|mini|mmp|mobi|motorola|nec-|nokia|palm|panasonic|philips|phone|playbook|sagem|sharp|sie-|silk|smartphone|sony|symbian|t-mobile|telus|up\.browser|up\.link|vodafone|wap|webos|wireless|xda|xoom|zte)/i', $_SERVER['HTTP_USER_AGENT']))
      return true;
   else
      return false;
}
function gen_passwords ($length = 6){
   // start with a blank password
   $password = "";
   // define possible characters - any character in this string can be
   // picked for use in the password, so if you want to put vowels back in
   // or add special characters such as exclamation marks, this is where
   // you should do it
   $possible = "123467890abcdfghjkmnpqrtvwxyzABCDFGHJKLMNPQRTVWXYZ9876543210";
   //$possible .= $unix;
   // we refer to the length of $possible a few times, so let's grab it now
   $maxlength = strlen($possible);
   // check for length overflow and truncate if necessary
   if ($length > $maxlength){
      $length = $maxlength;
   }
   // set up a counter for how many characters are in the password so far
   $i = 0;
   // add random characters to $password until $length is reached
   while ($i < $length) {
      // pick a random character from the possible ones
      $char = substr($possible, mt_rand(0, $maxlength-1), 1);
      // have we already used this character in $password?
      if (!strstr($password, $char)) {
         // no, so it's OK to add it onto the end of whatever we've already got...
         $password .= $char;
         // ... and increase the counter by one
         $i++;
      }
   }
   // done!
   return $password;
}
function unix_time($time){
   $time = explode(" ", $time);
   $d = explode("-", $time[0]);
   $t = explode(":", $time[1]);
   $ret = mktime((int)$t[0], (int)$t[1], (int)$t[2], (int)$d[1], (int)$d[2], (int)$d[0]);
   return $ret;
}
function get_median($arr_data){
   if(!is_array($arr_data)) return "N/A";
   $total = count($arr_data);
   $middle = floor(($total+1) / 2)-1;
   $ck_number = ($total)%2;
   $median = 0;
   if($ck_number==0){
      $low = $arr_data[$middle];
      $high = $arr_data[$middle + 1];
      $median = round(($low + $high) / 2);
   }else{
      $median = $arr_data[$middle];
   }
   return $median;
}
function get_mode(array $arr_data){
   $counts = array_count_values($arr_data);
   arsort($counts); // Sort counts in descending order
   $modes  = array_keys($counts, current($counts), TRUE);
   // If each value only occurs once, there is no mode
   if (count($arr_data) === count($counts))
      return FALSE;
   // Only one modal value
   if (count($modes) === 1)
      return $modes[0];
   // Multiple modal values
   return $modes;
}
function redirect_url($params="", $clear_arg=false, $arr_clear_arg=false){
	if( strpos($_SERVER["HTTP_REFERER"], "http://")!==false){
		$url = substr($_SERVER["HTTP_REFERER"], strpos($_SERVER["HTTP_REFERER"], "/", 15));
	}else{
		$url = $_SERVER["HTTP_REFERER"];
	}
	$arg = array();
	if(strpos($url, "?")!==false){
		$urlRef = $url;
		$url = substr($url, 0, strpos($url, "?")+1);
		$sArg = substr($urlRef, strpos($urlRef, "?")+1);
		while(strpos($sArg, "&&")!==false)
			$sArg = str_replace("&&", "&", $sArg);
		$sArg = explode("&", $sArg);
		foreach($sArg as $index=>$val){
			$aTmp = explode("=", $val, 2);
			$arg[$aTmp[0]] = $aTmp[1];
		}
		$arg_not_convert = $arg;
	}else{
		$url = "$url?";
	}
	if($params){
		foreach($params as $index=>$val){
			$arg[$index] = $val;
		}
	}
	if($arr_clear_arg){
		foreach($arr_clear_arg as $name)
			if($name) unset($arg[$name]);
	}
	if($clear_arg===true){
		$url = str_replace("?", "", $url);
	}else{
		foreach($arg as $index=>$val){
			if(!$index) continue;
			if($arg_not_convert[$index]==$val)
				$url .= "&$index=".($val);
			else
				$url .= "&$index=".urlencode($val);
		}
	}
	header("Location: $url");
	exit();
}
function render_pages($call_page){
	if($call_page){
		include_once ("$call_page");
	}	
}
function digit_name($i){
   $i = (int)$i;
   switch($i){
      case 1 : return "หนึ่ง";
      case 2 : return "สอง";
      case 3 : return "สาม";
      case 4 : return "สี่";
      case 5 : return "ห้า";
      case 6 : return "หก";
      case 7 : return "เจ็ด";
      case 8 : return "แปด";
      case 9 : return "เก้า";
   }
}
function number_letter($n){
   $int = (int)$n;
   if($int==0) return "";
   $int = "$int";
   $len = strlen($int);
   if($len<=6) return convert_number_name($int)."บาทถ้วน";
   else {
      $first = substr($int, 0, $len-6 );
      $last = substr($int, $len-6 );
      return convert_number_name( $first )."ล้าน".convert_number_name( $last )."บาทถ้วน";;
   }
}
function convert_number_name($n){
   $ret = "";
   $len = strlen($n);
   for($i=$len; $i>=1; $i--){
      $di = $n[$len-$i];
      $ldi = $n[$len-$i-1];
      if($di=="0") continue;
      switch($i){
         case 6 : $unit = "แสน"; break;
         case 5 : $unit = "หมื่น"; break;
         case 4 : $unit = "พัน"; break;
         case 3 : $unit = "ร้อย"; break;
         case 2 : $unit = "สิบ"; break;
         case 1 : $unit = ""; break;
      }
      $name = "";
      if($unit=="สิบ" && $di=="1"){
         $name = "";
         $ret = $ret.$name.$unit;
         continue;
      }else if($unit=="สิบ" && $di=="2"){
         $name = "ยี่";
      }else if($unit=="" && $di=="1" && $i==1 && $ret){
         $name = "เอ็ด";
      }
      if(!$name) $name = digit_name($di);
      $ret = $ret.$name.$unit;
   }
   return $ret;
}
function thai_number($num){
	if(!$num) return "";
    return str_replace(array( '0' , '1' , '2' , '3' , '4' , '5' , '6' ,'7' , '8' , '9' ),
	array( "o" , "๑" , "๒" , "๓" , "๔" , "๕" , "๖" , "๗" , "๘" , "๙" ),
    $num);
}
/**
 * Make Debug Array 
 * http://php.net
 */
function d($data){
	echo "<pre>";
		print_r($data);
	echo "</pre>";
	//die();
}

function get_select_course($section_id="", $sec_action){
	global $db;
	$t = get_config("select_course_setting", true);
	$r = json_decode($t, true);

	if($section_id && $sec_action){
		$section = $r[$section_id];
		return $section[$sec_action];
	}else{
		return 1;
	}
}


function rundocno($coursetype_id, $section_id){
	global $db;
    $y = date("y")+43;  
    if($section_id==3){
    	$prefix = "FA";
	}else if($section_id==1 && $coursetype_id==2) {
    	$prefix = "AE";
    }else{
    	$prefix = "AT";
    }
    $q = "select max(runno) from register where runyear='$y' and doc_prefix='$prefix' ";
    $runno = $db->data($q) + 1;
    $args["runyear"] = $y;
    $args["runno"] = $runno;
    $args["doc_prefix"] = $prefix;
    $args["docno"] = "{$prefix}{$y}/" . sprintf("%06d", $runno);	
    return $args;
}

function check_cart_id($number) {
    if(strlen($number) != 13) return false;
    for($i=0, $sum=0; $i<12;$i++)
        $sum += (int)($number{$i})*(13-$i);
    if((11-($sum%11))%10 == (int)($number{12}))
        return true;
    return false;
}


function alert_danger($massage){
  $msg = '<div class="col-md-12">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<i class="fa fa-times-circle sign"></i><strong>Error!</strong> '.$massage.'
			</div>';
	return $msg;	
}

function get_signature($upload_signature_id, $width=80, $height=80){
	if(!$upload_signature_id) return "";
	global $db;
	$q = "select url from upload_signature where upload_signature_id=$upload_signature_id";
	$url = $db->data($q);
	if($url!=""){
		return '<img src="'.$url.'" alt="" height="'.$height.' width='.$width.'">';
	}else{
		return '';
	}
}


function write_log($file_name="", $msg="", $write_mode="a", $dir_log="../log/"){
	$str = $msg;	
	$date_now = date('Y-m-d H:i:s');
	$date_log = date('Y-m-d');
	$log_name = $dir_log.$file_name."_".$date_log.".log";
	$file = fopen($log_name, $write_mode);
	$str_txt = $date_now."|".$str."\r\n";
	fwrite($file, $str_txt);
	fclose($file);
}//end func


function get_expire_date($number="", $unit="day"){
	if( empty($number) ) return "";
	$datetime_now = date('Y-m-d H:i:s');
	$dt = $number." ".$unit;
	$expire_date = date("Y-m-d H:i:s",strtotime("{$dt}"));
	return $expire_date;
}//end func

?>