<?php 
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
//include_once "./share/datatype.php";
global $db;
// d($db);
//d($_POST); //die();


?>

<!DOCTYPE html>
<html lang="en">

<?php include ('./inc/header-bootstrap.php'); ?>
<?php //include ('./inc/js-script.php'); ?>

<body>

  <form action='' method="POST">
   <div class="container" style="margin-top: 10%; margin-bottom: 10%;">
   <br>
     <div class="row">
      <!-- start logo -->
      <div class="form-group col-sm-12">
        <a id="logo" class="img-responsive"  href="http://www2.redcrossfundraising.org" title="สำนักงานจัดหารายได้ สภากาชาดไทย" rel="home">
        <!-- <img src="./images/logo-kplus.png" alt="สำนักงานจัดหารายได้ สภากาชาดไทย logo"  style="width: 150px;" > -->
         <img src="./images/logo-redcross.png" alt="สำนักงานจัดหารายได้ สภากาชาดไทย logo"  style="width: 150px; margin-left: -20px;" >
        </a>
      </div>
      <!-- end logo -->
    </div>
    
    <br>

    <div class="row">
      <div class="form-group col-sm-12">
        <div class="billinfo">
          <div class="control-group">       
              <center>
                <span style="color: red; font-size: 30px; font-weight: bold;">การทำรายการบริจาคของท่านไม่สำเร็จ</span> 
                <br><br>
                <span style="color: red; font-size: 30px; font-weight: bold;">กรุณาลองบริจาคในช่องทางอื่น หรือติดต่อเจ้าหน้าที่ตามที่อยู่ข้างล่างนี้</span>
                <br><br>
                <span style="color: red; font-size: 30px; font-weight: bold;">หากท่านต้องการสั่งซื้อสินค้าที่ระลึก สามารถเลือกชมได้ <a href="http://shopping.redcrossfundraising.org/" target="_blank" >ที่นี่</a></span>
              </center>            
          </div>
        </div>
      </div>
    </div>
    <!-- Button -->
   <div class="col-sm-12">
  <!-- <div class="buttonb"> 
    กลับหน้าแรก
  </div> -->
</div> 

</div>
</form>
<br>
<br>
<footer id="footer" class="site-footer" role="contentinfo">
  <!-- <div class="container"> -->
  <div class="row">
    <div class="site-info col-md-12 text-center">
      สำนักงานจัดหารายได้ สภากาชาดไทย ตึกอำนวยนรธรรม ชั้น 2 เลขที่ 1873 ถนนพระราม 4 เขตปทุมวัน กรุงเทพฯ 10330 โทรศัพท์ 0 2256 4440, 0 2255 9911       
    </div>
    <!-- .site-info -->
  </div>
  <!-- </div> --><!-- .col-full -->
</footer>


</body>
</html>