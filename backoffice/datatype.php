<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;
$msg = $_SESSION["popup"]["msg"];
unset($_SESSION["popup"]["msg"]);
?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-3">
				<div class="content block-flat ">
					<div class="header" style="border-bottom:none;">							
						<h3><i class="fa fa-list"></i> &nbsp;Datatype</h3>
					</div> 
					<table id="tbMenu"style="width:100%">
						<thead>
							<tr>
								<th width="6%">No</th>
								<th width="94%">Datatype List</th>
							</tr>
						</thead>  
						<tbody>
							<?php
							$i = 1;
							$id = trim($_GET["con_id"],",");
							$con = $id ? " and datatype_id in ($id)" : ""; 
							$q = "select * from dataType where active='T' $con";
							$array_right = $db->get($q);
							if(is_array($array_right)){
								foreach($array_right as $k=>$v){
									?> 
									<tr onClick="singleKey(this)" style="cursor:pointer;">
										<td><?php echo $i;?>.<input name="pId" id="pId" type="hidden" value="<?php echo $v["datatype_id"];?>">&nbsp;<input name="table_name" id="table_name" type="hidden" value="<?php echo $v["table_name"];?>"></td>
											<td id="name"><?php echo $v["name"];?>&nbsp;</td>							    
										</tr>
										<?php
										$i++;
									} 
								}?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="col-sm-9">
					<div class="content block-flat ">
						<form action="update-datatype.php" class="form-horizontal" id="frmMain" method="post" >
							<div class="header">							
								<h3><i class="fa fa-edit"></i> &nbsp;Settings <span id="showname"></span></h3>
							</div>
							<input name="datatype_id" id="datatype_id" type="hidden" ><input name="con_id" value="<?php echo $_GET["con_id"]?>" type="hidden" > &nbsp;<input type="hidden" name="datatype_name" id="datatype_name"  class="required"> 
							<input type="hidden" name="table_name" id="table_name" >
							<fieldset>
								<div class="content">
									<table class="no-border" id="tbList">
										<thead>
											<tr class="no-border">
												<th width="6%">No</th>
												<th width="10%">Code</th>
												<th width="">Name</th>
												<th width="25%">Remark</th>
												<th width="20%" style="display:none;">Records/Time</th>
											</tr>
										</thead>   
										<tbody class="no-border-x no-border-y">
											<tr>
												<td><input name="ckbox[]" type="checkbox" checked id="ckbox" value="T">&nbsp;&nbsp;<span id="runNo"></span></td>
												<td class="center">
													<input name="table_id[]" type="hidden" id="table_id">
													<input class="form-control focused" name="code[]" id="code" type="text" value="" style="width:50px">
												</td>   
												<td class="center">
													<input class="form-control focused" name="name[]" id="name" type="text" value="" style="width:350px">
												</td>
												<td class="center">
													<textarea class="form-control focused" name="remark[]" id="remark" type="text" value="" style="width:210px"></textarea></td>
													<td class="center" style="display:none;">
														<span id="recby_name"></span><br><span id="rectime"></span>
													</td>
												</tr>
											</tbody>
										</table> 
									</div>
									<div class="content">
										<button type="button" class="btn" onClick="addItem();"><span ><strong>+</strong>&nbsp;Add</span></button> 
									</div>
									<div class="content">
										<button type="button" class="btn btn-primary" onClick="ckSave()">Save changes</button>&nbsp;
										<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>')">Clear Page</button>						
									</div>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div> 
		</div>

		<?php include ('inc/js-script.php') ?>
		
		<script type="text/javascript">
			$(document).ready(function() {
				//$("#frmMain").validate();
				var datatype_id = "<?php echo $_GET["datatype_id"]?>";
				var tmpname = "<?php echo $_GET["datatype_name"]?>";
				var tmptable_name = "<?php echo $_GET["table_name"]?>";
			// $("#tbMenu").dataTable();
			if(datatype_id) viewInfo(datatype_id, tmpname, tmptable_name);
			var msg = "<?php echo $msg?>";
			if(msg!="")	msgSuccess(msg); 
		});
			var trList = $("#tbList tbody tr:eq(0)").clone();
			delRow("#tbList");

			function dispList(table_name){
				delRow("#tbList");
				var url = "data/datatype.php";
				var param = "table="+table_name;
				$.ajax( {
					"dataType":'json', 
					"type": "POST",
					"async" : false,  
					"url": url,
					"data": param, 
					"success": function(data){	
						$.each(data, function(index, array){
							array.table_id = array.datatype_id;
							addTrLine("#tbList", trList, array, "runNo");
						});				 
					}
				});	
			}

			function viewInfo(id, tmpname, tmptable_name){
				if(typeof id=="undefined") return;
				$("#frmMain input[name=datatype_id]").val(id);
				$("#frmMain input[name=datatype_name]").val(tmpname);
				$("#frmMain input[name=table_name]").val(tmptable_name);
				$("#frmMain #showname").html(tmpname);
				dispList(tmptable_name);
			}

			function addItem(){
				var t = addTrLine("#tbList", trList);
				$(t).find("#runNo").text($("#tbList tbody tr").length);	
			}

			function singleKey(tag){
				var id = $(tag).find('#pId').val();	
				var name = $(tag).find('#name').text();
				var table_name = $(tag).find('#table_name').val();
				$("#frmMain input[name=datatype_id]").val(id);
				$("#frmMain input[name=datatype_name]").val(name);
				$("#frmMain input[name=table_name]").val(table_name);
				$("#frmMain #showname").html(name);
				dispList(table_name);
			}

			function ckSave(id){
				var ck = $("#showname").text();
				if(strTrim($("#showname").text())==""){
					alert("กรุณาเลือกรายการที่ต้องการตั้งค่า");
					return;
				}	
				onCkForm("#frmMain");  
				$("#frmMain").submit();
			}
		</script>	