<?php
/*
  Plugin Name: WP Bank Donation
  Version: 1.0
  Plugin URI: http://numplus.net/
  Author: bankeyboy
  Author URI: http://numplus.net/
  Description: Easily accept PayPal Express Checkout Payments in WordPress
  Text Domain: wp-paypal
  Domain Path: /languages
 */
  if (!defined('ABSPATH'))
    exit;
  if (session_id() == "") session_start();
  class WP_BANK_DONATION {
    var $plugin_version = '1.0';
    var $plugin_url;
    var $plugin_path;
    function __construct() {
      define('WP_BANK_DONATION_VERSION', $this->plugin_version);
      define('WP_BANK_DONATION_SITE_URL', site_url());
      define('WP_BANK_DONATION_HOME_URL', home_url());
      define('WP_BANK_DONATION_URL', $this->plugin_url());
      define('WP_BANK_DONATION_PATH', $this->plugin_path());
      $use_sandbox = get_option('wp_bank_donation_enable_testmode');
      if (isset($use_sandbox) && !empty($use_sandbox)) {
        define('WP_BANK_DONATION_USE_SANDBOX', true);
      } else {
        define('WP_BANK_DONATION_USE_SANDBOX', false);
      }
      $this->plugin_includes();
      $this->loader_operations();
    }
    function plugin_includes() {
      include_once('wp-bank-donation.php');
    }
    function loader_operations() {
      register_activation_hook(__FILE__, array($this, 'activate_handler'));
      add_action('plugins_loaded', array($this, 'plugins_loaded_handler'));
      add_action('admin_notices', array($this, 'admin_notice'));
      add_action('wp_enqueue_scripts', array($this, 'plugin_scripts'));
      add_action('init', array($this, 'plugin_init'));
      add_action('add_meta_boxes', array($this, 'add_meta_boxes'));
      add_filter('manage_wp_bank_donation_posts_columns', 'wp_bank_donation_transection_columns');
      add_action('manage_wp_bank_donation_posts_custom_column', 'wp_bank_donation_custom_column', 10, 2);
      add_shortcode('wp_bank_donation', 'wp_bank_donation_load_handler');
    }
    function plugins_loaded_handler() {  //Runs when plugins_loaded action gets fired
      load_plugin_textdomain( 'wp-bank-donation', false, plugin_basename( dirname( __FILE__ ) ) . '/languages' );
      $this->check_upgrade();
    }
    function admin_notice() {
    }
    function activate_handler() {
      add_option('wp_bank_donation_plugin_version', $this->plugin_version);
    }
    function check_upgrade() {
      if (is_admin()) {
        $plugin_version = get_option('wp_bank_donation_plugin_version');
        if (!isset($plugin_version) || $plugin_version != $this->plugin_version) {
          $this->activate_handler();
          update_option('wp_bank_donation_plugin_version', $this->plugin_version);
        }
      }
    }
    function plugin_init() {
        //register orders
      wp_bank_donation_transection_page();
    }
    function add_meta_boxes() {
        //add_meta_box('wp-bank-donation-order-box', __('Edit PayPal Order', 'wp-bank-donation'), 'wp_bank_donation_order_meta_box', 'wp_bank_donation_order', 'normal', 'high');
    }
    function plugin_scripts() {
      if (!is_admin()) {
      }
    }
    function plugin_url() {
      if ($this->plugin_url)
        return $this->plugin_url;
      return $this->plugin_url = plugins_url(basename(plugin_dir_path(__FILE__)), basename(__FILE__));
    }
    function plugin_path() {
      if ($this->plugin_path)
        return $this->plugin_path;
      return $this->plugin_path = untrailingslashit(plugin_dir_path(__FILE__));
    }
  }
  $GLOBALS['wp_bank_donation'] = new WP_BANK_DONATION();
  function wp_bank_donation_load_handler($atts) {
    global $post, $wpdb;
    $wporg_atts = shortcode_atts([
     'lang' => 'th',
     ], $atts);
    $lang = strtolower($wporg_atts["lang"]); 
    // $lang  = "en";
    ob_start();
    $action = (isset($_GET["action"])) ? $_GET["action"] : '';
    if($action=="complete"){
      $texthome = ($lang=="en") ? 'Back To Donate' : 'กลับไปหน้าบริจาค';
      $msg = 'ขอบคุณที่บริจาค';      
      echo '<h3>'.$msg.'</h3>';
      echo '<p> <a class="btn btn-primary" href="'.get_permalink($post->ID).'">'.$texthome.'</a></p>';
      $output = ob_get_contents();
      ob_clean();
      return $output;        
    }
    $price = array(100, 500,750, 1000);
    ?>
    <form action = "<?php echo WP_BANK_DONATION_URL . '/process.php';?>" id="frm-donate" method = "post">
      <input type="hidden" name="redirect_page_id" value="<?php echo $post->ID; ?>">
      <input type="hidden" name="lang_ref" value="<?php echo $lang; ?>">
      <div class="funkyradio">
        <h2><?php echo ($lang=="en") ? 'Amount' : "จำนวนที่บริจาค"; ?></h2>
        <?php
        $ck = 0; 
        foreach ($price as $key => $value) {
          $select = (++$ck==1) ? 'checked="checked"' : ''; 
          ?>
          <div class="funkyradio-success">
            <input type="radio"  <?php echo $select; ?> name="amount" value="<?php echo $value;?>" id="amount-<?php echo $value;?>" />
            <label for="amount-<?php echo $value;?>"><?php echo $value;?></label>
          </div>
          <?php
        } ?>
        <div class="funkyradio-success">
          <input type="radio" id="amount-other" name="amount" value="n" />
          <label for="amount-other"><?php echo ($lang=="en") ? 'Custom Amount' : "ระบุจำนวน"; ?></label>
        </div>
      </div>
      <fieldset id="other-amount">
        <legend><h4><?php echo ($lang=="en") ? 'Edit Amount' : "เพิ่มจำนวน"; ?></h4></legend>
        <div class="input-group" id="disableOtherAmount">
          <input id="custom-amount" type="input"> 
          <button id="btn-custom-amount" class="btn btn-default" type="button"><?php echo ($lang=="en") ? 'OK' : "เพิ่มข้อมูล"; ?></button> 
        </div>         
        <div class="margin30"></div>
      </fieldset>
      <div class="margin30"></div>


 <?php /*     
     <div class="funkyradio">
        <h4><?php echo ($lang=="en") ? 'Currency ' : "สกุลเงิน"; ?></h4>
        <?php 
        $ck = 0;
        $json = '{
            "Australian dollar": {
                "currency-code": "AUD"
            },
            "Brazilian real <sup>2</sup>": {
                "currency-code": "BRL"
            },
            "Canadian dollar": {
                "currency-code": "CAD"
            },
            "Czech koruna": {
                "currency-code": "CZK"
            },
            "Danish krone": {
                "currency-code": "DKK"
            },
            "Euro": {
                "currency-code": "EUR"
            },
            "Hong Kong dollar": {
                "currency-code": "HKD"
            },
            "Hungarian forint <sup>1</sup>": {
                "currency-code": "HUF"
            },
            "Israeli new shekel": {
                "currency-code": "ILS"
            },
            "Japanese yen <sup>1</sup>": {
                "currency-code": "JPY"
            },
            "Malaysian ringgit <sup>2</sup>": {
                "currency-code": "MYR"
            },
            "Mexican peso": {
                "currency-code": "MXN"
            },
            "New Taiwan dollar <sup>1</sup>": {
                "currency-code": "TWD"
            },
            "New Zealand dollar": {
                "currency-code": "NZD"
            },
            "Norwegian krone": {
                "currency-code": "NOK"
            },
            "Philippine peso": {
                "currency-code": "PHP"
            },
            "Polish złoty": {
                "currency-code": "PLN"
            },
            "Pound sterling": {
                "currency-code": "GBP"
            },
            "Russian ruble": {
                "currency-code": "RUB"
            },
            "Singapore dollar": {
                "currency-code": "SGD"
            },
            "Swedish krona": {
                "currency-code": "SEK"
            },
            "Swiss franc": {
                "currency-code": "CHF"
            },
            "Thai baht": {
                "currency-code": "THB"
            },
            "United States dollar": {
                "currency-code": "USD"
            }
        }';
        $currency = json_decode($json, true);
        ?>
<select name="currency" class="form-cotrol">
        <?php
        $dc = get_option('wp_bank_donation_currency_code');
        foreach ($currency as $key => $value) {
            if(!$key) continue;
            $select = ($value["currency-code"]==$dc) ? 'selected="selected"' : '';
            ?>
            <option <?php echo $select; ?> value="<?php echo $value["currency-code"];?>"><?php echo ucwords($key);?> &nbsp;(<?php echo $value["currency-code"];?>)</option>            
            <?php
        } ?>
     </select>
     </div> 
*/?>


<div class="margin30"></div>
<?php /*
     <pre>
     <?php if($lang=="en"){ ?>
Note : 
        1 This currency does not support decimals. Passing a decimal amount will result in an error.
        2 This currency is supported as a payment currency and a currency balance for in-country PayPal accounts only.
<?php }else{ ?>     
 หมายเหตุ:
     1 สกุลเงินนี้ไม่สนับสนุนทศนิยม ผ่านจำนวนทศนิยมจะส่งผลให้เกิดข้อผิดพลาด
     2 สกุลเงินนี้ได้รับการสนับสนุนเป็นสกุลเงินการชำระเงินและความสมดุลสำหรับสกุลเงินในประเทศบัญชี PayPal เท่านั้น 
     <?php } ?>       
     </pre>
*/?>

<div class="margin30"></div>
<h4><?php echo ($lang=="en") ? 'Donations to the project ' : "บริจาคให้กับโครงการ"; ?></h4>
<?php
$taxonomy     = 'bank_project';
$orderby      = 'name';  
$show_count   = 0;     
$pad_counts   = 0;     
$hierarchical = 1;     
$title        = '';  
$empty        = 0;
$parent = 0;
$args = array(
  'taxonomy'     => $taxonomy,
  'orderby'      => $orderby,
  'show_count'   => $show_count,
  'pad_counts'   => $pad_counts,
  'hierarchical' => $hierarchical,
  'title_li'     => $title,
  'hide_empty'   => $empty,
  'parent' => $parent,
  );
$ck = 0;
$parent_cat = get_categories( $args );
if(is_array($parent_cat)){
  foreach ($parent_cat as $k => $v) {
    $category_id = $v->term_id;
    $select = (++$ck==1) ? 'checked="checked"' : '';  
    if($lang=="en") $v->name = $v->description;  
    ?>
    <div class="funkyradio row">
      <div class="col-md-12 funkyradio-info">
        <input type="radio" <?php echo $select; ?> name="term_id" id="radio-<?php echo $v->term_id;?>" value="<?php echo $v->term_id;?>" />
        <label class="col-md-12" for="radio-<?php echo $v->term_id;?>"><?php echo $v->name; ?></label>
      </div>
    </div>
    <?php 
  }
}
?>
<div class="margin30"></div>
<div class="funkyradio">
  <h4><?php //echo ($lang=="en") ? 'Transaction Type' : "ประเภทการบริจาค"; ?></h4>
  <?php 
  $ck = 0;
  if($lang!="en"){
            // $recuring = array('one-time'=>'ครั้งเดียว', 'Month'=>'รายเดือน');
            // $recuring = array('one-time'=>'ครั้งเดียว');
  }else{
            // $recuring = array('one-time'=>'One Time', 'Month'=>'Month');
  }
  foreach ($recuring as $key => $value) {
    $select = (++$ck==1) ? 'checked="checked"' : '';
    ?>
    <div class="funkyradio-success">
      <input type="radio" <?php echo $select; ?> name="recuring" value="<?php echo $key;?>" id="recuring-<?php echo $key;?>" />
      <label for="recuring-<?php echo $key;?>"><?php echo $value;?></label>
    </div>
    <?php
  } ?>
</div>
<div class="margin30"></div>
<label><?php //echo ($lang=="en") ? 'Request  to invoice' : "ขอใบกำกับภาษี"; ?>    
  <!-- <input class="form-cotrol" type="checkbox" name="invoice_required" value="YES" style="cursor: pointer;"> -->
</label>


<?php
if ( $lang=="th" ) {
  ?>    
  <div id="donor_submit">
    <div class="col-md-12">
      <button class="sub-button don-button">ยืนยันการบริจาค</button>
    </div>
  </div>
  <?php 
    }//end if
    ?>



    <?php do_action('wp_bank_donation_button_display' ); ?>       
  </form>
  <div id="pop2" class="simplePopup">
    <div id="loader"><img src="<?php echo WP_BANK_DONATION_URL . '/images' ?>/ajax-loader.gif"/><img id="processing_animation" src="<?php echo WP_BANK_DONATION_URL . '/images' ?>/processing_animation.gif"/></div>
  </div>
  <script src="<?php echo WP_BANK_DONATION_URL;?>/js/jquery.simplePopup.js" type="text/javascript"></script> 
  <script type="text/javascript">

    function select_bank(id){
     jQuery("#bank_type").val(id);
     var t =jQuery('input[name=amount]:checked', '#frm-donate').val()
     if(isNaN(t) && t!=""){
      alert('<?php echo ($lang=="en") ? 'Amount Donation is Required' : "ระบุจำนวน"; ?>');
      return;
    } 
    var t =jQuery('input[name=term_id]:checked', '#frm-donate').val()
    if(isNaN(t) && t!=""){
      alert('<?php echo ($lang=="en") ? 'Specify donations to projects' : "ระบุการบริจาคให้กับโครงการ"; ?>');
      return;
    }                  
    jQuery('#pop2').simplePopup();
    jQuery("#frm-donate").submit();
  }
  jQuery(document).ready(function($) {    
    $("#btn-sumbit").click(function(event) {
      event.preventDefault();
      var t = $('input[name=amount]:checked', '#frm-donate').val()
      if(isNaN(t) && t!=""){
        alert('<?php echo ($lang=="en") ? 'Amount Donation is Required' : "ระบุจำนวน"; ?>');
        return;
      } 
      var t = $('input[name=term_id]:checked', '#frm-donate').val()
      if(isNaN(t) && t!=""){
        alert('<?php echo ($lang=="en") ? 'Specify donations to projects' : "ระบุการบริจาคให้กับโครงการ"; ?>');
        return;
      }                  
      $('#pop2').simplePopup();
      $("#frm-donate").submit();
    });
    $('input[name=amount]').click(function() {
     if($('#amount-other').is(':checked')) { 
       $("#other-amount").slideDown('400', function() {
       });
     }else{
       $("#other-amount").slideUp('400', function() {
       });
     }           
   });
    $('#btn-custom-amount').click(function() {
      var t = $("#custom-amount").val();
      if((isNaN(t) && t!="") || t=="") {
        $("#amount-other").val(0); 
        $("label[for=amount-other]").html('<?php echo ($lang=="en") ? 'Custom Amount' : "ระบุจำนวน"; ?>'); 
      }else{
        $("label[for=amount-other]").html(' '+t+' '); 
        $("#amount-other").val(t);
      }              
    });
  }); 
</script>
<style type="text/css">
  #other-amount{
    display: none;
    position: relative;
    transition: fade .25s ease-in-out;
    -moz-transition: fade .25s ease-in-out;
    -webkit-transition: fade .25s ease-in-out;
  }
  #other-amount h4{
    transition: fade .25s ease-in-out;
    -moz-transition: fade .25s ease-in-out;
    -webkit-transition: fade .25s ease-in-out;       
  }
  #disableOtherAmount{
    border: 1px solid #eee;
  }
  .funkyradio div {
    clear: both;
    /*margin: 0 50px;*/
    overflow: hidden;
    display: inline-flex;
  }
  #other-amount input {
    padding: 0px 15px 0px 2px;
    border-radius: 3px;
    border: 1px solid #D1D3D4;
    font-weight: normal;
    line-height: 36px;
  }
  .funkyradio label {
    padding: 0px 15px 0px 2px;
    border-radius: 3px;
    border: 1px solid #D1D3D4;
    font-weight: normal;
  }
  .funkyradio input[type="radio"]:empty, .funkyradio input[type="checkbox"]:empty {
    display: none;
  }
  .funkyradio input[type="radio"]:empty ~ label, .funkyradio input[type="checkbox"]:empty ~ label {
    position: relative;
    line-height: 40px;
    text-indent: 3.25em;
    margin-top: 0em;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
  }
  .funkyradio input[type="radio"]:empty ~ label:before, .funkyradio input[type="checkbox"]:empty ~ label:before {
    position: absolute;
    display: block;
    top: 0;
    bottom: 0;
    left: 0;
    content:'';
    width: 2.5em;
    background: #D1D3D4;
    border-radius: 3px 0 0 3px;
  }
  .funkyradio input[type="radio"]:hover:not(:checked) ~ label:before, .funkyradio input[type="checkbox"]:hover:not(:checked) ~ label:before {
    content:'\2714';
    text-indent: .9em;
    color: #C2C2C2;
  }
  .funkyradio input[type="radio"]:hover:not(:checked) ~ label, .funkyradio input[type="checkbox"]:hover:not(:checked) ~ label {
    color: #888;
  }
  .funkyradio input[type="radio"]:checked ~ label:before, .funkyradio input[type="checkbox"]:checked ~ label:before {
    content:'\2714';
    text-indent: .9em;
    color: #333;
    background-color: #ccc;
  }
  .funkyradio input[type="radio"]:checked ~ label, .funkyradio input[type="checkbox"]:checked ~ label {
    color: #777;
  }
  .funkyradio input[type="radio"]:focus ~ label:before, .funkyradio input[type="checkbox"]:focus ~ label:before {
    box-shadow: 0 0 0 3px #999;
  }
  .funkyradio-default input[type="radio"]:checked ~ label:before, .funkyradio-default input[type="checkbox"]:checked ~ label:before {
    color: #333;
    background-color: #ccc;
  }
  .funkyradio-primary input[type="radio"]:checked ~ label:before, .funkyradio-primary input[type="checkbox"]:checked ~ label:before {
    color: #fff;
    background-color: #337ab7;
  }
  .funkyradio-success input[type="radio"]:checked ~ label:before, .funkyradio-success input[type="checkbox"]:checked ~ label:before {
    color: #fff;
    background-color: #5cb85c;
  }
  .funkyradio-danger input[type="radio"]:checked ~ label:before, .funkyradio-danger input[type="checkbox"]:checked ~ label:before {
    color: #fff;
    background-color: #d9534f;
  }
  .funkyradio-warning input[type="radio"]:checked ~ label:before, .funkyradio-warning input[type="checkbox"]:checked ~ label:before {
    color: #fff;
    background-color: #f0ad4e;
  }
  .funkyradio-info input[type="radio"]:checked ~ label:before, .funkyradio-info input[type="checkbox"]:checked ~ label:before {
    color: #fff;
    background-color: #5bc0de;
  }   
  .simplePopup {
    display:none;
    position:fixed;
    border:4px solid #808080;
    background:#fff;
    z-index:3;
    padding:12px;
    width: 16%;
    min-width: 25%;
  }
  .simplePopupClose {
    float:right;
    cursor:pointer;
    margin-left:10px;
    margin-bottom:10px;
  }
  .simplePopupBackground {
    display:none;
    background:#000;
    position:fixed;
    height:100%;
    width:100%;
    top:0;
    left:0;
    z-index:1;
  }
  img.load{
    width: 10%;
    margin: 10px 41%;
  }
  img.logo{
    width: 40%;
    margin: -6px 105px;
  }
  p.load_text{
    margin: 10px 97px;
  }
  img.formget_logo{
    margin: 10px 110px;
  }
  .sub-button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 16px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 24px;
    margin: 4px 2px;
    -webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.4s;
    cursor: pointer;
  }

  .don-button {
    border-radius: 8px;
    background-color: #4CAF50;
    color: white;
  }

  .don-button:hover {
    background-color: white; 
    color: black; 
    border: 2px solid #4CAF50;
    border-radius: 8px;
  }
</style>
<?php
$output = ob_get_contents();
ob_clean();
return $output;
}
function wp_bank_donation_debug_log($msg, $success, $end = false) {
  if (!WP_BANK_DONATION_DEBUG) {
    return;
  }
    $date_time = date('F j, Y g:i a');//the_date('F j, Y g:i a', '', '', FALSE);
    $text = '[' . $date_time . '] - ' . (($success) ? 'SUCCESS :' : 'FAILURE :') . $msg . "\n";
    if ($end) {
      $text .= "\n------------------------------------------------------------------\n\n";
    }
    // Write to log.txt file
    $fp = fopen(WP_BANK_DONATION_DEBUG_LOG_PATH, 'a');
    fwrite($fp, $text);
    fclose($fp);  // close file
  }
  function wp_bank_donation_debug_log_array($array_msg, $success, $end = false) {
    if (!WP_BANK_DONATION_DEBUG) {
      return;
    }
    $date_time = date('F j, Y g:i a');//the_date('F j, Y g:i a', '', '', FALSE);
    $text = '[' . $date_time . '] - ' . (($success) ? 'SUCCESS :' : 'FAILURE :') . "\n";
    ob_start();
    print_r($array_msg);
    $var = ob_get_contents();
    ob_end_clean();
    $text .= $var;
    if ($end) {
      $text .= "\n------------------------------------------------------------------\n\n";
    }
    // Write to log.txt file
    $fp = fopen(WP_BANK_DONATION_DEBUG_LOG_PATH, 'a');
    fwrite($fp, $text);
    fclose($fp);  // close filee
  }
  function wp_bank_donation_reset_log() {
    $log_reset = true;
    $date_time = date('F j, Y g:i a');//the_date('F j, Y g:i a', '', '', FALSE);
    $text = '[' . $date_time . '] - SUCCESS : Log reset';
    $text .= "\n------------------------------------------------------------------\n\n";
    $fp = fopen(WP_BANK_DONATION_DEBUG_LOG_PATH, 'w');
    if ($fp != FALSE) {
      @fwrite($fp, $text);
      @fclose($fp);
    } else {
      $log_reset = false;
    }
    return $log_reset;
  }