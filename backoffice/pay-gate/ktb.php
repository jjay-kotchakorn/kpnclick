<?php 
		if( $ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING" ){
			/*echo '<pre>';
				print_r($resArray);
			echo '</pre>';*/
			$txn_id = ($recuring!="one-time") ? $resArray["PROFILEID"] : $resArray["TRANSACTIONID"];
	        $args = array(
	            'post_type' => 'wp_paypal',
	            'meta_query' => array(
	                array(
	                    'key' => '_txn_id',
	                    'value' => $txn_id,
	                    'compare' => '=',
	                ),
	            ),
	        );
	        $query = new WP_Query($args);
	        if ($query->have_posts()) {  //a record already exists
	            echo "An order with this transaction ID already exists. This payment will not be processed.";
	            die();
	        }else{
	        	$content = '<ul>';
					$txn_txt = ($recuring!="one-time") ? 'PROFILEID' : 'TRANSACTIONID';
	        		$content .= '<li><p>'.$txn_txt.' : '.$txn_id.'</p></li>';
	        		$content .= '<li><p>FIRSTNAME : '.$firstName.'</p></li>';
	        		$content .= '<li><p>LASTNAME : '.$lastName.'</p></li>';
	        		$content .= '<li><p>SHIPTONAME : '.$shipToName.'</p></li>';
	        		$content .= '<li><p>SHIPTOSTREET : '.$shipToStreet.'</p></li>';
	        		$content .= '<li><p>SHIPTOCITY : '.$shipToCity.'</p></li>';
	        		$content .= '<li><p>SHIPTOSTATE : '.$shipToState.'</p></li>';
	        		$content .= '<li><p>SHIPTOZIP : '.$shipToZip.'</p></li>';
	        		$content .= '<li><p>SHIPTOCOUNTRYNAME : '.$shipToCntryName.'</p></li>';
	        	$content .= '</ul>';
				$defaults = array(
					'post_status'           => 'draft', 
					'post_type'             => 'wp_paypal',
					'post_title'    		  => wp_strip_all_tags( $firstName . ' ' . $lastName . ' - ' . $resArray["TIMESTAMP"] . ' ('.$txn_txt.' : '.$txn_id.')'.$_SESSION["invoice_required"] ),
					'post_content'  		  => $content,
					'post_parent'           => 0,
					'menu_order'            => 0,
					'to_ping'               =>  '',
					'pinged'                => '',
					'post_password'         => '',
					'guid'                  => ''
					);

					
				// Insert the post into the database
				$post_id = wp_insert_post( $defaults );
				if($post_id){

					wp_set_post_terms( $post_id, $_SESSION["term_obj"]->term_id, 'bank_project');
					wp_set_post_terms( $post_id, $bank_type_id, 'bank_type');
		            update_post_meta($post_id, '_txn_id', $txn_id);
		            update_post_meta($post_id, '_first_name', $firstName);
		            update_post_meta($post_id, '_last_name', $lastName);
		            update_post_meta($post_id, '_term_obj', $_SESSION["term_obj"]->term_id);
		            update_post_meta($post_id, '_payment_status', $resArray["PAYMENTSTATUS"]);
		            update_post_meta($post_id, '_mc_gross', $resArray["AMT"]);					

		            update_post_meta($post_id, '_invoice_required', $_SESSION["invoice_required"]);				
		            update_post_meta($post_id, '_currency_code', get_option('wp_paypal_currency_code'));					
					$complete_url = add_query_arg( array(
						'action' => 'complete'
						), get_permalink($_SESSION['redirect_page_id']));			
					wp_redirect($complete_url );
				}
				exit;
	        }			






 ?>