<?php 
//include_once "../share/authen.php";
include_once "../inc/header-bootstrap.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "./lib/config_kbank.php";
include_once "../share/register.php";
global $db;

$datetime_now = date('Y-m-d H:i:s');
$date_now = date('Y-m-d');
// echo $register_id."<hr>";
// d($db);

// d($_SERVER);

function run_invoice(){
	global $db;
    $y = date("y")+43;  
    $prefix = "DN";

    $q = "select max(runno) from payment_bank_register_list where runyear='$y'";
    $runno = $db->data($q) + 1;
    $args["runyear"] = $y;
    $args["runno"] = $runno;
    $args["doc_prefix"] = $prefix;
    // $args["docno"] = "{$prefix}{$y}/" . sprintf("%06d", $runno);	
    $args["docno"] = "{$prefix}{$y}" . sprintf("%08d", $runno);	
    return $args;
}//end func

// $register_id = 54;
if ( !empty($register_id) ) {
	$register_info = get_register("", $register_id);
	$register_info = $register_info[0];
	$project_id = $register_info["project_id"];
	$payment_bank_id = $register_info["payment_bank_id"];
	// d($register_info);

	$q = "SELECT a.name_th
			, a.name_eng
			, b.name AS project_type_name_th
			, b.name_eng AS project_type_name_en
		FROM project AS a
		LEFT JOIN projecttype AS b ON b.projecttype_id = a.projecttype_id
		WHERE a.project_id={$project_id}
	";
	$project = $db->rows($q);
	$project_type_name_th = $project["project_type_name_th"];
	$project_type_name_en = $project["project_type_name_en"];
	$project_name_th = $project["name_th"];
	$project_name_en = $project["name_eng"];

	$q = "SELECT expire_day FROM payment_bank WHERE payment_bank_id={$payment_bank_id}";
	$expire_day = $db->data($q);

	$expire_date = get_expire_date($expire_day);

}//end if

/*Merchant ID*/
//$MERCHANT2 = "10418";
/*Amount of purchased order (Original product amount) and last 2 digits are decimal.*/ 
$amount = $register_info["amount"];
$arr_amount = explode(".", $amount);

// d($arr_amount);

if ( count($arr_amount)>1 ) {
	$amount_bath = (int)$arr_amount[0];
	$amount_satang = trim($arr_amount[1]);

	if ( strlen($amount_satang)==2 ) {
		$amount_satang = $amount_satang;
	}else{
		$amount_satang = sprintf("%1d0", $amount_satang);
	}//end else

	$amount = $amount_bath.$amount_satang;
}else{
	$amount = $amount."00";
}//end else

// echo $amount; die();

$AMOUNT2 = sprintf("%012d", $amount);
// $URL2 = $url_back_dev;
$RESPURL = $url_for_response;
/*Public IP address for Server or Web Hosting.*/ 
// $IPCUST2 = "203.150.231.128";
$DETAIL2 = $project_type_name_th." ({$project_type_name_en})";
$INVMERCHANT = sprintf("%012d", $register_id);
/*PayPLUS send 00.*/ 
$SHOPID = "00";
/*Application Authentication Function MD5 Hashing (MERCHANT2+AMOUNT2+URL2+RESPURL+IPCUST2+DETAIL2+INVMERCHANT+SHOPID+MD5KEY +REF1+REF2+INFO1+INFO2+INFO3+TIMEOUT)*/
$CHECKSUM = "";
/*
$ref_1 = date('Ymd').sprintf("%012d", $project_id);
$REF1 = $ref_1;
$ref_2 = sprintf("%020d", $register_id);
$REF2 = $ref_2; 
*/
$REF1 = date('Ymd').sprintf("%012d", $project_id);
$REF2 = "";

// $INFO1 = $project_type_name_th." ".$project_name_th;
$INFO1 = $project_type_name_th."ให้สภากาชาดไทย";
// $INFO2 = $project_type_name_en." ".$project_name_en;
$INFO2 = $project_type_name_en." For The Thai Red Cross Society";
$INFO3 = "";
// $TIMEOUT = date('Ymd')." 23:59:59";
$TIMEOUT = str_replace("-", "", $expire_date);

$CHECKSUM = $MERCHANT2.$AMOUNT2.$URL2.$RESPURL.$IPCUST2.$DETAIL2.$INVMERCHANT.$SHOPID.$MD5KEY.$REF1.$REF2.$INFO1.$INFO2.$INFO3.$TIMEOUT;
// $CHECKSUM = '10418000000001050https://devred.numplus.com/donation/pay-gate/pay-kplus.phphttps://devred.numplus.com/donation/pay-gate/result-kbank.php203.150.231.128test donation00000000000100QxMjcGFzc3MOIQ=v09QTBOTT9Ed0RnT0201802060000011602000001test donation INFO 1test donation INFO 2test donation INFO 320180209 23:59:59';
// echo "CHECKSUM = ".$CHECKSUM;
// echo "<hr>";
$CHECKSUM = md5($CHECKSUM);
// echo "CHECKSUM (MD5) = ".$CHECKSUM;

$args = array();
$args["MERCHANT2"] = $MERCHANT2;
$args["AMOUNT2"] = $AMOUNT2;
$args["URL2"] = $URL2;
$args["RESPURL"] = $RESPURL;
$args["IPCUST2"] = $IPCUST2;
$args["DETAIL2"] = $DETAIL2;
$args["INVMERCHANT"] = $INVMERCHANT;
$args["SHOPID"] = $SHOPID;
$args["CHECKSUM"] = $CHECKSUM;
$args["REF1"] = $REF1;
$args["REF2"] = $REF2;
$args["INFO1"] = $INFO1;
$args["INFO2"] = $INFO2;
$args["INFO3"] = $INFO3;
$args["TIMEOUT"] = $TIMEOUT;

// d($args);

$params_serialize = serialize($args);

$q = "SELECT payment_bank_register_list_id FROM payment_bank_register_list WHERE active='T' AND register_id={$register_id}";
$payment_bank_register_list_id = $db->data($q);

$args = array();
$args["table"] = "payment_bank_register_list";
if ( !empty($payment_bank_register_list_id) ) {
	$args["id"] = $payment_bank_register_list_id;
}//end if
$args["merchant_id"] = $MERCHANT2;
$args["payment_bank_id"] = $payment_bank_id;
$args["invoice"] = $INVMERCHANT;
$args["ref1"] = $REF1;
$args["ref2"] = $REF2;
$args["payment_options_id"] = 4;
$args["register_id"] = $register_id;
$args["pay_status"] = 1;
$args["order_expire_date"] = $expire_date;
$args["donation_date"] = $datetime_now;
$args["amount"] = (float)$register_info["amount"];
$args["url_for_thank_you"] = $URL2;
$args["url_for_payment"] = $url_for_payment;
$args["params_serialize"] = $params_serialize;

// d($args);
// var_dump($db->set($args, true, true));
$ret = $db->set($args);
$payment_bank_register_list_id = !empty($ret) ? $ret : $payment_bank_register_list_id;



// $rs = run_invoice();
// d($rs);

$params = "";
$str_txt = '';
foreach ($args as $key => $v) {
	 $params .= $key . '='.$v.'&'; 
}//end loop $v

// echo $params; die();
/*
$ch = curl_init(); 

curl_setopt($ch,CURLOPT_URL,$url_for_payment);

curl_setopt($ch,CURLOPT_POSTFIELDS,$params);

curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

curl_setopt($ch,CURLOPT_HEADER, false);

curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 60);

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);


$output=curl_exec($ch);
$error_msg = curl_error($ch);
$info = curl_getinfo($ch);  

curl_close($ch);
*/

// die();
//echo "<hr> SEND POST to URL : https://uatkpgw.kasikornbank.com/payplus/payment.aspx <hr>";
//$url_for_payment
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ระบบบริจาค</title>
</head>
<body>
	<form name="sendform" method="post" action="<?php echo $url_for_payment ?>">
	<!-- <form name="sendform" method="post" action="test.php"> -->
		<input type="hidden" id=MERCHANT2 name=MERCHANT2 value="<?php echo $MERCHANT2;?>">
		<input type="hidden" id=AMOUNT2 name=AMOUNT2 value="<?php echo $AMOUNT2;?>">
		<input type="hidden" id=URL2 name=URL2 value="<?php echo $URL2;?>">
		<input type="hidden" id=RESPURL name=RESPURL value="<?php echo $RESPURL;?>">
		<input type="hidden" id=IPCUST2 name=IPCUST2 value="<?php echo $IPCUST2;?>">
		<input type="hidden" id=DETAIL2 name=DETAIL2 value="<?php echo $DETAIL2;?>">
		<input type="hidden" id=INVMERCHANT name=INVMERCHANT value="<?php echo $INVMERCHANT;?>">
		<input type="hidden" name="SHOPID" id="SHOPID" value="00">
		<input type="hidden" name="CHECKSUM" id="CHECKSUM" value="<?php echo $CHECKSUM;?>">
	<?php /*
		<input type="text" id="REF1" name="REF1" value="<?php echo $REF1;?>">
		<input type="text" id="REF2" name="REF2" value="<?php echo $REF2;?>">
		<input type="text" id="INFO1" name="INFO1" value="<?php echo $INFO1;?>">
		<input type="text" id="INFO2" name="INFO2" value="<?php echo $INFO2;?>">
		<input type="text" id="INFO3" name="INFO3" value="<?php echo $INFO3;?>">
		<input type="text" id="TIMEOUT" name="TIMEOUT" value="<?php echo $TIMEOUT;?>">
	*/ ?>
		<input type="hidden" id="REF1" name="REF1" value="<?php echo $REF1;?>">
		<input type="hidden" id="REF2" name="REF2" value="<?php echo $REF2;?>">
		<input type="hidden" id="INFO1" name="INFO1" value="<?php echo $INFO1;?>">
		<input type="hidden" id="INFO2" name="INFO2" value="<?php echo $INFO2;?>">
		<input type="hidden" id="INFO3" name="INFO3" value="<?php echo $INFO3;?>">
		<input type="hidden" id="TIMEOUT" name="TIMEOUT" value="<?php echo $TIMEOUT;?>">
	</form>
	<!-- <button onclick="document.sendform.submit();">Submit</button> -->
</body>
</html>

<script type="text/javascript">

	$(document).ready(function(){
		// alert(555);
		document.sendform.submit();
	});
</script>		