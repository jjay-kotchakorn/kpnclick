<?php

// $url_payment_dev = "https://chillchill.ais.co.th:8002/AISMPAYPartnerInterface/InterfaceService?";

$url_payment_dev = "https://testpaygate.ktc.co.th/scs/eng/merchandize/payment/payForm.jsp";
$url_payment_prod = "https://paygate.ktc.co.th/ktc/eng/merchandize/payment/payForm.jsp";

$url_response_dev = "http://devred.numplus.com/wp-content/plugins/wp-bank-donation/result-kbank.php";
$url_response_prod = "http://www2.redcrossfundraising.org/wp-content/plugins/wp-bank-donation/result-kbank.php";

$url_back_dev = "http://devred.numplus.com/donation/donation-thank.php";
$url_back_prod = "http://www2.redcrossfundraising.org/donation/pay-gate/result-success-ktc.php";

$url_end_point_dev = "http://devred.numplus.com";
$url_end_point_prod = "http://www2.redcrossfundraising.org";

$url_fail_dev = "http://devred.numplus.com/donation/pay-gate/result-error-ktc.php";
$url_fail_prod = "http://www2.redcrossfundraising.org/donation/pay-gate/result-error-ktc.php";


$url_for_payment = $url_payment_dev;
$url_for_response = $url_response_dev;
//$url_end_point = $url_end_point_dev;

/*Merchant ID*/
$merchantId = "023300116";
/*Amount of purchased order (Original product amount) and last 2 digits are decimal.*/ 
$amount = "";
/*“764” – THB “840” – USD*/ 
$currCode = "764";
/*“T” – Thai “E” – English*/ 
$lang = "T";
$cancelUrl = $url_end_point_prod;
$failUrl = $url_fail_prod;
$successUrl = $url_back_prod;
/* ”N” – Normal Payment (Sales)   “H” – Hold Payment (Authorize only)*/ 
$payType = "N";
/*“CC” – Credit Card Payment*/
$payMethod = "CC";
$TxType = "Retail";


?>