<?php 
//include_once "../share/authen.php";
include_once "../inc/header-bootstrap.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "./lib/config_ktc.php";
include_once "../share/register.php";
global $db, $dbktc;

$datetime_now = date('Y-m-d H:i:s');
$date_now = date('Y-m-d');
// echo $register_id."<hr>";
// d($dbktc); die();

// d($_SERVER);

function run_invoice(){
	global $db;
    $y = date("y")+43;  
    $prefix = "DN";

    $q = "select max(runno) from payment_bank_register_list where runyear='$y'";
    $runno = $db->data($q) + 1;
    $args["runyear"] = $y;
    $args["runno"] = $runno;
    $args["doc_prefix"] = $prefix;
    // $args["docno"] = "{$prefix}{$y}/" . sprintf("%06d", $runno);	
    $args["docno"] = "{$prefix}{$y}" . sprintf("%08d", $runno);	
    return $args;
}//end func

// $register_id = 54;
if ( !empty($register_id) ) {
	$register_info = get_register("", $register_id);
	$register_info = $register_info[0];
	$project_id = $register_info["project_id"];
	$payment_bank_id = $register_info["payment_bank_id"];
	d($register_info);
	$orderRef = sprintf("%012d", $register_id);
	$amount = $register_info["amount"];

	$q = "SELECT a.name_th
			, a.name_eng
			, b.name AS project_type_name_th
			, b.name_eng AS project_type_name_en
		FROM project AS a
		LEFT JOIN projecttype AS b ON b.projecttype_id = a.projecttype_id
		WHERE a.project_id={$project_id}
	";
	$project = $db->rows($q);
	$project_type_name_th = $project["project_type_name_th"];
	$project_type_name_en = $project["project_type_name_en"];
	$project_name_th = $project["name_th"];
	$project_name_en = $project["name_eng"];

	$q = "SELECT expire_day FROM payment_bank WHERE payment_bank_id={$payment_bank_id}";
	$expire_day = $db->data($q);

	$expire_date = get_expire_date($expire_day);

	$q = "SELECT payment_bank_register_list_id FROM payment_bank_register_list WHERE active='T' AND register_id={$register_id}";
	$payment_bank_register_list_id = $db->data($q);

	$REF1 = date('Ymd').sprintf("%012d", $project_id);
	$REF2 = "";

	$args = array();
	$args["table"] = "payment_bank_register_list";
	if ( !empty($payment_bank_register_list_id) ) {
		$args["id"] = $payment_bank_register_list_id;
	}//end if
	$args["merchant_id"] = $merchantId;
	$args["payment_bank_id"] = $payment_bank_id;
	$args["invoice"] = $orderRef;
	$args["ref1"] = $REF1;
	$args["ref2"] = $REF2;
	$args["payment_options_id"] = 1;
	$args["register_id"] = $register_id;
	$args["pay_status"] = 1;
	$args["order_expire_date"] = $expire_date;
	$args["donation_date"] = $datetime_now;
	$args["amount"] = (float)$amount;
	$args["url_for_thank_you"] = $successUrl;
	$args["url_for_payment"] = $url_for_payment;
	$params_serialize = serialize($args);
	$args["params_serialize"] = $params_serialize;

	// d($args);
	// var_dump($db->set($args, true, true));
	$ret = $db->set($args);
	$payment_bank_register_list_id = !empty($ret) ? $ret : $payment_bank_register_list_id;
}//end if


// die();
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ระบบบริจาค</title>
</head>
<body>
	<form name="sendform" method="post" action="<?php echo $url_for_payment; ?>">
		<input type="hidden" name="merchantId" value="<?php echo $merchantId; ?>">
		<input type="hidden" name="amount" value="<?php echo $amount; ?>" >
		<input type="hidden" name="orderRef" value="<?php echo $orderRef; ?>">
		<input type="hidden" name="currCode" value="764" >
		<input type="hidden" name="successUrl" value="<?php echo $successUrl; ?>">
		<input type="hidden" name="failUrl" value="<?php echo $failUrl; ?>">
		<input type="hidden" name="cancelUrl" value="<?php echo $cancelUrl; ?>">
		<input type="hidden" name="payType" value="N">
		<input type="hidden" name="lang" value="T">
		<input type="hidden" name="TxType" value="Retail" >
<?php /*
		<input type="hidden" name="Term" value="03" >
		<input type="hidden" name="promotionType" value="01" >
		<input type="hidden" name="supplierId" value="" >
		<input type="hidden" name="productId" value="" >
		<input type="hidden" name="serialNo" value="12123131">
		<input type="hidden" name="model" value="" >
		<input type="hidden" name="itemTotal" value="1" >
*/ ?>
		<input type="submit" name="submit">
	</form> 
	<!-- <button onclick="document.sendform.submit();">Submit</button> -->
<?php /*
	<form name="sendform" method="post" action="https://testpaygate.ktc.co.th/scs/eng/merchandize/payment/payForm.jsp">
		<input type="hidden" name="merchantId" value="023300116">
		<input type="hidden" name="amount" value="100.0" >
		<input type="hidden" name="orderRef" value="000000000001">
		<input type="hidden" name="currCode" value="764" >
		<input type="hidden" name="successUrl"
		value="http://www.yourdomain.com/Success.html">
		<input type="hidden" name="failUrl" value="http://www.yourdomain.com/Fail.html">
		<input type="hidden" name="cancelUrl" value="http://www.yourdomain.com/Cancel.html">
		<input type="hidden" name="payType" value="N">
		<input type="hidden" name="lang" value="T">
		<input type="hidden" name="TxType" value="Retail" >
		<input type="hidden" name="Term" value="03" >
		<input type="hidden" name="promotionType" value="01" >
		<input type="hidden" name="supplierId" value="" >
		<input type="hidden" name="productId" value="" >
		<input type="hidden" name="serialNo" value="12123131">
		<input type="hidden" name="model" value="" >
		<input type="hidden" name="itemTotal" value="1" >
		<input type="submit" name="submit">
	</form> 
	<!-- <button onclick="document.sendform.submit();">Submit</button> -->
*/ ?>
</body>
</html>

<script type="text/javascript">

	$(document).ready(function(){
		// alert(555);
		document.sendform.submit();
	});
</script>		