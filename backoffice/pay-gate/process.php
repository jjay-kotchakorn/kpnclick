<?php
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
global $db;

$datetime_now = date('Y-m-d H:i:s');

// d($_POST); //die();

if ( !empty($_POST) ) {

    $amount = trim($_POST["amount"]); 
    $project_id = $_POST["project_id"]; 
    $radio = $_POST["radio"]; 
    $title_th = trim($_POST["title_th"]); 
    $fname = trim($_POST["fname"]); 
    $lname = trim($_POST["lname"]); 
    $cor_name = trim($_POST["cor_name"]); 
    $don_bill = trim($_POST["don_bill"]); 
    $address = trim($_POST["address"]);                                          
    $province_id = $_POST["province_id"]; 
    $amphur_id = $_POST["amphur_id"]; 
    $district_id = $_POST["district_id"]; 
    $zip_code = trim($_POST["zip_code"]);  
    $taxno = trim($_POST["taxno"]);  
    $telephone = trim($_POST["telephone"]);  
    $payment_bank_id = (int)$_POST["payment_bank_id"]; 

    $slip_type = "";
    if ( $radio=="radio_individual" ) {
        $slip_type = "individual";
    }else if( $radio=="radio_corporation" ){
        $slip_type = "corporation";
    }//end else if

    $amount = str_replace(",", "", $amount);

    $args = array();
    $args["table"] = "register";

    if ( $slip_type=="corporation" ) {
        $args["receipttype_text"] = $cor_name;
    }else if ( $slip_type=="individual" ) {
        $args["receipt_title"] = $title_th;
        $args["receipt_fname"] = $fname;
        $args["receipt_lname"] = $lname;
    }//end else
    
    $args["amount"] = $amount;
    $args["payment_bank_id"] = $payment_bank_id;
    $args["project_id"] = $project_id;
    $args["slip_type"] = $slip_type;
    $args["taxno"] = $taxno;
    $args["telephone"] = $telephone;
    $args["date"] = $datetime_now;
    

    if ( $don_bill=="on" ) {
        $args["bill_request"] = "T";
        $args["receipt_date"] = $datetime_now;
        $args["receipt_no"] = $address;
        $args["receipt_district_id"] = $district_id;
        $args["receipt_amphur_id"] = $amphur_id;
        $args["receipt_province_id"] = $province_id;
        $args["receipt_postcode"] = $zip_code;
    }//end if

    // d($args);
    // var_dump( $db->set($args, true, true) );
    $register_id = $db->set($args);

    if ( !empty($register_id) ) {

        $q = "SELECT file_name FROM payment_bank WHERE payment_bank_id={$payment_bank_id}";
        $url_for_pay = $db->data($q);

        include_once $url_for_pay;

    }//end if

}//end if



?>