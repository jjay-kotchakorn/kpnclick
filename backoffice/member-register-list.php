<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;
// d($_SESSION["login_member_company"]);
$member_company_id = $_SESSION["login_member_company"]["info"]["member_company_id"];
$member_company_righttype = $_SESSION["login_member_company"]["info"]["member_company_righttype"];
// echo $member_company_id."=".$member_company_righttype;
?>
<link rel="stylesheet" type="text/css" href="js/jquery.nanoscroller/nanoscroller.css" />
<link href="js/jquery.icheck/skins/square/blue.css" rel="stylesheet">
<div id="cl-wrapper" style="overflow: hidden;">
	<div class="container" id="pcont">
		<div class="row">
			<div class="col-md-12">  		
				<img src="images/logo-fa.png" alt="">
			</div>
		</div>
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
					<div class="page-head">
						<button style="visibility: hidden;" class="btn btn-success btn-small pull-right" onclick="addnew()" style="margin-top:10px; margin-right:-25px;"><i class="fa fa-plus"></i> เพิ่มข้อมูลสมาชิก</button>
						<h3><i class="fa fa-user"></i> &nbsp;รายชื่อผู้ผ่านการทดสอบ FA</h3>
					</div>
					<table id="tbMember" class="table" style="width:100%">
						  <thead>
							  <tr>
								  <th width="3%">ลำดับ</th>
								  <th width="20%">ชื่อ-นามสกุล</th>
								  <th width="20%">ชื่อบริษัท</th>
								  <th width="10%">โทรศัพท์</th>
								  <th width="10%">E-mail</th>
							  </tr>
						  </thead>   
						<tbody>
						</tbody>
					</table>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div> 
</div>
<?php include ('inc/js-script.php') ?>
<script type="text/javascript">
$(document).ready(function() {
	$("a[rel=fancybox]").fancybox();
	var oTable;
	$("#frmMain").validate();
	listItem();
    $("#date_start").datepicker({language:'th-th',format:'dd-mm-yyyy'});
    $("#date_stop").datepicker({language:'th-th',format:'dd-mm-yyyy'});
   $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue checkbox',
        radioClass: 'iradio_square-blue'
      });
});
function listItem(){
   var url = "data/member-register-list.php";
   oTable = $("#tbMember").dataTable({
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
   	   "sInfomemberty": "",
   		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons":  [	
			   ]
		},
		"aoColumns": [
		null,
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false }
		],
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({"name":"fa_member","value":"T"});	
			aoData.push({"name":"member_company","value":"T"});	
			aoData.push({"name":"member_company_sort","value":"T"});	
			aoData.push({"name":"member_company_id","value":"<?php echo $member_company_id;?>"});	
			aoData.push({"name":"member_company_righttype","value":"<?php echo $member_company_righttype;?>"});	
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
		}
   });
   $('#tbMember_filter input').unbind();
   $('#tbMember_filter input').bind('keyup', function(e) {
       if(e.keyCode == 13) {
        	oTable.fnFilter(this.value);   
    	}
   }); 
}
function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}
function view_info(member_id="", cid=""){
	//var url = "index.php?p=<?php // echo $_GET["p"];?>&member_id="+member_id+"&type=info";
   	//redirect(url);
}
</script>