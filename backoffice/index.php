<?php 
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/menu.php";
?>
<!DOCTYPE html>
<html lang="en">
<?php include ('inc/header.php'); ?>
<body>
	<?php if($is_login) include ('inc/top-menu.php'); ?>
	<?php

	if($is_login){
		$args = $menu_access;
		$args['main'] = 'donation.php';
	}else{
		$args = array(
			'default' => 'login.php',
			//'default' => 'news-anchor-front.php',
			//'default' => 'news-anchor-front.php',
			// 'news-anchor-front' => 'news-anchor-front.php',
			// 'news-anchor-vews' => 'news-anchor-views.php',
			// 'forget-password' => 'forgot-password.php',
			'register' => 'memberinfo.php'
			);
	}

	$call_page = isset($_GET['p']) ? $_GET['p'] : "";
	if(!$call_page || !($args[$call_page])) 
		$call_page = ($is_login) ? "main" : "default";

	?>
	<?php 
		  if(file_exists($args[$call_page])) 
		  	render_pages($args[$call_page]); 
		  else 
		  	include_once ('inc/js-script.php');
	?>
	<?php include ('inc/footer.php') ?>
</body>
</html>
<?php $db->disconnect(); ?>