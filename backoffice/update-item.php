<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";

global $db;

$code = trim($_POST["code"]);
$dup = 0;
$id = "";
if($code && $_POST["active"]!='F'){
	$id = $_POST["item_id"];
	$con = ($id) ? " and item_id != $id" : "";
	$q = "select count(item_id) as c from item where code='$code' $con and active!='F'";
	$rs = $db->data($q);
	if($rs>0) $dup = 1;
}
if(!$code || $dup==1){
	$args = array();
	$args["p"] = "item";
	$args["item_id"] = $id;
	$args["type"] = "info";
	$_SESSION["error"]["msg"] = "รหัส $code ซ้ำ";
	redirect_url($args);
}

if($_POST){
	$args = array();
	$args["table"] = "item";
	if($_POST["item_id"])
	   $args["id"] = $_POST["item_id"];
	$args["code"] = $_POST["code"];
	$args["itemtype_id"] = (int)$_POST["itemtype_id"];
	$args["itemtype_text"] = $_POST["itemtype_text"];
	$args["room_id"] = (int)$_POST["room_id"];
	$args["name"] = $_POST["name"];
	$args["address"] = $_POST["address"];
	$args["remark"] = $_POST["remark"];
	$args["active"] = $_POST["active"];
	$args["recby_id"] = (int)$EMPID;
	$args["rectime"] = date("Y-m-d H:i:s");
	
   $ret = $db->set($args);
   $item_id = $args["id"] ? $args["id"] : $ret;
}
$_SESSION["success"]["msg"] = "บันทึกข้อมูลเรียบร้อยแล้ว";

$args = array();
$args["p"] = "item";
$args["item_id"] = $item_id;
$args["type"] = "info";
redirect_url($args);
?>