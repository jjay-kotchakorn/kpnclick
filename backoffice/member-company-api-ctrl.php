<?php
include_once "./connection/connection.php";
include_once "./lib/lib.php";
require "./vendor/autoload.php";
global $db;

use \Curl\Curl;
$date_now = date('Y-m-d H:i:s');
// $api_user = "fauser";
// $api_key = "fauser@fa8615";

$url = "";

function write_log($file_name="", $msg="", $write_mode="a"){
	$str_txt = "";
	$str = $msg;	
	$date_now = date('Y-m-d H:i:s');
	$date_log = date('Y-m-d');
	$log_name = "./log/".$file_name."_".$date_log.".log";
	$file = fopen($log_name, $write_mode);
	$str_txt = $date_now."|".$str."\r\n";
	fwrite($file, $str_txt);
}//end func

function check_auth($api_key="", $api_user=""){
	if ( $api_key=="fauser@fa8615" && $api_user=="fauser" ) {
		return true;
	}else{
		return false;
	}//end else
}//end func

// d($_SERVER);
$request_method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : '';
$request_url = !empty($_SERVER["PATH_INFO"]) ? $_SERVER["PATH_INFO"] : '';
if (!array_key_exists('CONTENT_TYPE', $_SERVER) && array_key_exists('HTTP_CONTENT_TYPE', $_SERVER)) {
    $_SERVER['CONTENT_TYPE'] = $_SERVER['HTTP_CONTENT_TYPE'];
}
$content_type = isset($_SERVER['CONTENT_TYPE']) ? $_SERVER['CONTENT_TYPE'] : '';
// $data_values = $_GET;
if ($request_method === 'POST') {
    $data_values = $_POST;
    $auth = check_auth($data_values["api_key"], $data_values["api_user"]);
    if ( $request_url == "/register/" && $auth ) {

    	
		$str_txt = "";
	    foreach ($args as $key => $v) {
	    	$str_txt .= "{$key}={$v}|";
	    }//end loop $v
	    write_log("elearning-api-ctrl-register", $str_txt);

    }elseif ( $request_url == "/result/" ) {


		$str_txt = "";
		foreach ($args as $key => $v) {
	    	$str_txt .= "{$key}={$v}|";
	    }//end loop $v
	    $str_txt .= "status={$status}|";
	    write_log("elearning-api-ctrl-result", $str_txt);
    }//end else if
    // d($data_values);
    // d($data_values["send_date"]);
    // echo $data_values;

} elseif ($request_method === 'PUT') {
    if (strpos($content_type, 'application/x-www-form-urlencoded') === 0) {
        parse_str($http_raw_post_data, $_PUT);
        $data_values = $_PUT;
    }
} elseif ($request_method === 'PATCH') {
    if (strpos($content_type, 'application/x-www-form-urlencoded') === 0) {
        parse_str($http_raw_post_data, $_PATCH);
        $data_values = $_PATCH;
    }
} elseif ($request_method === 'DELETE') {
    if (strpos($content_type, 'application/x-www-form-urlencoded') === 0) {
        parse_str($http_raw_post_data, $_DELETE);
        $data_values = $_DELETE;
    }
} elseif ($request_method === 'GET') {
	$data_values = $_GET;
    $auth = check_auth($data_values["api_key"], $data_values["api_user"]);
/*	
	if ( $request_url=="/product/list/" ) {
		$q="SELECT * FROM course";
		$rs = $db->get($q);
		// return d($_SERVER);die();
		// $rs = json_encode($rs);
		// return json_encode($rs);
		// return var_dump($rs);
		echo json_encode($rs);
		// d($rs);
	}
*/
	if ( $auth && $request_url=="/company/all/" ) {
		// echo "string";
		$q = "SELECT
				a.company_id,
				a.`code`,
				a.`name`,
				a.name_eng,
				a.address,
				a.email,
				a.phone,
				a.website,
				a.map_img,
				a.tax_address,
				a.tax_no,
				a.branch,
				a.active,
				a.recby_id,
				a.rectime,
				a.fax,
				a.capital,
				a.ratio
			FROM company a
		";
		$rs = $db->get($q);	
		$data = array();
		if ( !empty($rs) ) {
			$data["status"] = "success";			
		}else{
			$data["status"] = "empty";			
		}//end else
		$data["size"] = count($rs);
		$data["return_date"] = $date_now;
		$data["value"] = $rs;
		echo json_encode($data);

	}else if( $auth && $request_url=="/company/id/" ){
		$company_id = (int)$data_values["id"];

		$q = "SELECT
				a.company_id,
				a.`code`,
				a.`name`,
				a.name_eng,
				a.address,
				a.email,
				a.phone,
				a.website,
				a.map_img,
				a.tax_address,
				a.tax_no,
				a.branch,
				a.active,
				a.recby_id,
				a.rectime,
				a.fax,
				a.capital,
				a.ratio
			FROM company a
			WHERE a.company_id={$company_id}
		";
		$rs = $db->rows($q);	
		$data = array();
		if ( !empty($rs) ) {
			$data["status"] = "success";			
		}else{
			$data["status"] = "empty";			
		}//end else
		$data["size"] = count($rs);
		$data["return_date"] = $date_now;
		$data["value"] = $rs;
		echo json_encode($data);
		
	}else if( $auth && $request_url=="/company/tax_no/" ){
		$tax_no = trim($data_values["tax_no"]);

		$q = "SELECT
				a.company_id,
				a.`code`,
				a.`name`,
				a.name_eng,
				a.address,
				a.email,
				a.phone,
				a.website,
				a.map_img,
				a.tax_address,
				a.tax_no,
				a.branch,
				a.active,
				a.recby_id,
				a.rectime,
				a.fax,
				a.capital,
				a.ratio
			FROM company a
			WHERE a.tax_no={$tax_no}
		";
		$rs = $db->rows($q);	
		$data = array();
		if ( !empty($rs) ) {
			$data["status"] = "success";			
		}else{
			$data["status"] = "empty";			
		}//end else
		$data["size"] = count($rs);
		$data["return_date"] = $date_now;
		$data["value"] = $rs;
		echo json_encode($data);
	}else if( $auth && $request_url=="/company/name/" ){
		$company_name = trim($data_values["name"]);

		$q = "SELECT
				a.company_id,
				a.`code`,
				a.`name`,
				a.name_eng,
				a.address,
				a.email,
				a.phone,
				a.website,
				a.map_img,
				a.tax_address,
				a.tax_no,
				a.branch,
				a.active,
				a.recby_id,
				a.rectime,
				a.fax,
				a.capital,
				a.ratio
			FROM company a
			WHERE a.name='{$company_name}'
		";
		$rs = $db->rows($q);	
		$data = array();
		if ( !empty($rs) ) {
			$data["status"] = "success";			
		}else{
			$data["status"] = "empty";			
		}//end else
		$data["size"] = count($rs);
		$data["return_date"] = $date_now;
		$data["value"] = $rs;
		echo json_encode($data);
	}//end else if
}//end else if


?>