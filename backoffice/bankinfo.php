<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/bank.php";
global $db;
$payment_bank_id = $_GET["payment_bank_id"];

$payment_bank = $db->get($q);

$apay = datatype(" and a.active='T'", "bank", true);

$q = "SELECT
		payment_bank_id,
		bank_id,
		`name`,
		name_eng,
		expire_day,
		active,
		recby_id,
		rectime,
		remark
		FROM payment_bank
		WHERE payment_bank_id=$payment_bank_id
";
$r = $db->rows($q);

$str = "";
if($r){
	$str = $r["name"];
	$bank_id = $r["bank_id"];
	$expire_day = $r["expire_day"];
	$name_th = $r["name"];
	$name_eng = $r["name_eng"];
	
}else{
	$str = "เพิ่มช่องทางบริจาค";
}
?>
<link rel="stylesheet" type="text/css" href="js/bootstrap.summernote/dist/summernote.css" />
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
				<div class="col-md-12">			      
					<div class="block-flat">
						<div class="header">							
							<ol class="breadcrumb">
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
								<li class="active"><?php echo $str; ?></li>
							</ol>
						</div>
						<div class="content">
							<form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="update-payment-bank.php">
								<input type="hidden" name="payment_bank_id" id="payment_bank_id" value="<?php echo $payment_bank_id; ?>">

								<!-- <div class="col-sm-12"> -->
								<div class="row">
									<h3 class="col-sm-2 control-label" style="text-align: left; color: #000;">ช่องทางบริจาค</h3>
								</div>
								<div class="row">
									<label class="col-sm-1 control-label" style=" padding-right:0px;">ธนาคาร </label>
									<div class="col-sm-3">
										<select name="sBank" id="sBank" class="form-control" onchange="reCall();">
											<option value="">แสดงทั้งหมด</option>

											<?php 
											foreach ($apay as $key => $value) {
												$id = $value['bank_id'];
												$s = "";

												if($bank_id==$id){
													$s = "selected";
												}

												$name = $value['name'];
												echo  "<option value='$id' {$s}>$name</option>";
												}//end loop $value 
												?>

											</select>
										</div>
										<label class="col-sm-2 control-label">บริจาคเงินภายใน (วัน)</label>
										<div class="col-sm-2">
											<input class="form-control" name="payment_expire_day" id="payment_expire_day"  placeholder="" type="number" 
											value="<?php echo $expire_day; ?>" 
											>
										</div>
										<label class="col-sm-1 control-label">สถานะ</label>
										<div class="col-sm-2">
											<select name="active" id="active" class="form-control required">
												<option selected="selected" value="T">ใช้งาน</option>
												<option value="F">ไม่ใช้งาน</option>
											</select>	                			
										</div>                
									</div>
									<!-- </div> -->
									<div class="row">
										<label class="col-sm-1 control-label">ชื่อภาษาไทย</label>
										<div class="col-sm-4">
											<input class="form-control" name="name_th" id="name_th"  placeholder="" type="text" value="<?php echo $name_th; ?>">
										</div>
										<label class="col-sm-2 control-label">ชื่อภาษาอังกฤษ</label>
										<div class="col-sm-4">
											<input class="form-control" name="name_eng" id="name_eng"  placeholder="" type="text" value="<?php echo $name_eng; ?>">
										</div>			                		            
									</div>

									<div class="clear"></div> 
									<br>
									<div class="form-group row" style="padding-left:10px;">
										<div class="col-sm-12">
											<button type="button" class="btn btn-primary" onClick="ckSave()">Save changes</button>
											<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">Cancel</button>
										</div>
									</div>
								</form>
							</div>
						</div>

					</div>
				</div>

			</div>
		</div> 
	</div>
	<?php include_once ('inc/js-script.php'); ?>

	<script type="text/javascript" src="js/bootstrap.summernote/dist/summernote.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			$("#frmMain").validate();
			var payment_bank_id = "<?php echo $_GET["payment_bank_id"]?>"; 
   //alert(payment_bank_id);
   if(payment_bank_id) viewInfo(payment_bank_id);
});
 //email_backlist_id
 function viewInfo(payment_bank_id){
 	if(typeof payment_bank_id=="undefined") return;
 	getemailInfo(payment_bank_id);
 }

 function getbankInfo(id){
 	if(typeof id=="undefined") return;
	//var url = "data/newslist.php";
	var url = "data/banklist.php";
	var param = "payment_bank_id="+id+"&single=T";
	dataUrl(url, param,"#frmMain");
}


function ckSave(id){
	onCkForm("#frmMain");
	var tmp = $('#detail').code();
	$('#detail').val(tmp);
	$("#frmMain").submit();
}	
</script>