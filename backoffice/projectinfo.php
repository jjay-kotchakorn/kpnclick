<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;
$project_id = $_GET["project_id"];

$project = $db->get($q);

$protype = datatype(" and a.active='T'", "projecttype", true);

$q = "SELECT
		project_id,
		code_project,
		name_th,
		name_eng,
		detail,
		`status`,
		projecttype_id,
		donation_min,
		donation_max,
		donation_min_receipt,
		active,
		recby_id,
		rectime,
		remark,
		createtime,
		dev_code
	FROM project
	WHERE project_id={$project_id}
";
$r = $db->rows($q);

$str = "";
if($r){
	$str = $r["name_th"];
	$name_th = $r["name_th"];
	$name_eng = $r["name_eng"];
	$donation_min = $r["donation_min"];
	$donation_max = $r["donation_max"];
	$donation_min_receipt = $r["donation_min_receipt"];
	$code_project = $r["code_project"];
	$projecttype_id = $r["projecttype_id"];
	
}else{
	$str = "เพิ่มโครงการ";
}
?>
<link rel="stylesheet" type="text/css" href="js/bootstrap.summernote/dist/summernote.css" />
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
				<div class="col-md-12">			      
					<div class="block-flat">
						<div class="header">							
							<ol class="breadcrumb">
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
								<li class="active"><?php echo $str; ?></li>
							</ol>
						</div>
						<div class="content">
							<form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="update-project.php">
								<input type="hidden" name="project_id" id="project_id" value="<?php echo $project_id; ?>">

								<div class="row">
									<h3 class="col-sm-2 control-label" style="text-align: left; color: #000;">โครงการ</h3>
								</div>

								
								<div class="row">
									<label class="col-sm-2 control-label" style=" padding-right:100px;">ชื่อภาษาไทย</label>
									<div class="col-sm-9">
										<input class="form-control" name="name_th" id="name_th"  placeholder="" type="text" value="<?php echo $name_th; ?>">
									</div>
								</div>
								<div class="row">
									<label class="col-sm-2 control-label" style=" padding-right:100px;">ชื่อภาษาอังกฤษ</label>
									<div class="col-sm-9">
										<input class="form-control" name="name_eng" id="name_eng"  placeholder="" type="text" value="<?php echo $name_eng; ?>">
									</div>			                		            
								</div>
							</div>
							<div class="row">
								<label class="col-sm-2 control-label">บริจาคขั้นต่ำ(บาท)</label>
								<div class="col-sm-2">
									<input class="form-control" name="donation_min" id="donation_min"  placeholder="" type="float" 
									value="<?php echo $donation_min; ?>" 
									>
								</div>
								<label class="col-sm-2 control-label">บริจาคขั้นสูงสุด(บาท)</label>
								<div class="col-sm-2">
									<input class="form-control" name="donation_max" id="donation_max"  placeholder="" type="float" 
									value="<?php echo $donation_max; ?>" 
									>
								</div>
							</div>
							<div class="row">
								<label class="col-sm-2 control-label">ยอดบริจาคที่ขอรับใบเสร็จ(บาท)</label>
								<div class="col-sm-2">
									<input class="form-control" name="donation_min_receipt" id="donation_min_receipt"  placeholder="" type="float" 
									value="<?php echo $donation_min_receipt; ?>" 
									>
								</div>
								<label class="col-sm-2 control-label">รหัสโครงการ(WP)</label>
								<div class="col-sm-2">
									<input class="form-control" name="code_project" id="code_project"  placeholder="" type="float" 
									value="<?php echo $code_project; ?>" 
									>
								</div>
							</div>
							<div class="row">
								<label class="col-sm-2 control-label" style="padding-left: 65px;">ประเภท </label>
								<div class="col-sm-2">
									<select name="sType" id="sType" class="form-control" onchange="reCall();">
										<option value="">แสดงทั้งหมด</option>

										<?php 
										foreach ($protype as $key => $value) {
											$id = $value['projecttype_id'];
											$s = "";

											if($projecttype_id==$id){
												$s = "selected";
											}

											$name = $value['name'];
											echo  "<option value='$id' {$s}>$name</option>";
												}//end loop $value 
												?>

											</select>
										</div>
										
										<label class="col-sm-2 control-label" style="padding-left: 100px;">สถานะ</label>
										<div class="col-sm-2">
											<select name="active" id="active" class="form-control required">
												<option selected="selected" value="T">ใช้งาน</option>
												<option value="F">ไม่ใช้งาน</option>
											</select>	                			
										</div>                
									</div>
									<!-- </div> -->

									<div class="clear"></div> 
									<br>
									<div class="form-group row" style="padding-left:10px;">
										<div class="col-sm-12">
											<button type="button" class="btn btn-primary" onClick="ckSave()">Save changes</button>
											<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">Cancel</button>
										</div>
									</div>
								</form>
							</div>
						</div>

					</div>
				</div>

			</div>
		</div> 
	</div>
	<?php include_once ('inc/js-script.php'); ?>

	<script type="text/javascript" src="js/bootstrap.summernote/dist/summernote.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			$("#frmMain").validate();
			var project_id = "<?php echo $_GET["project_id"]?>"; 
   //alert(project_id);
   if(project_id) viewInfo(project_id);
});
 
 function viewInfo(project_id){
 	if(typeof project_id=="undefined") return;
 	getprojectInfo(project_id);
 }

 function getprojectInfo(id){
 	if(typeof id=="undefined") return;
	var url = "data/projectlist.php";
	var param = "project_id="+id+"&single=T";
	dataUrl(url, param,"#frmMain");
}


function ckSave(id){
	onCkForm("#frmMain");
	var tmp = $('#detail').code();
	$('#detail').val(tmp);
	$("#frmMain").submit();
}	
</script>