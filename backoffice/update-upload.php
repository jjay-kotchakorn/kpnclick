<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include('share/class.upload.php');
global $db;

$ds          = DIRECTORY_SEPARATOR;  //1
 
$storeFolder = 'uploads/';   //2
 
if (!empty($_FILES) && $_POST["news_id"]) {
     
	$uploaddir = './uploads/'; 
	$file = $uploaddir . basename($_FILES['file']['name']); 
	$name_photo=($_FILES['file']['name']);
	$size=$_FILES['file']['size'];
	if($size>10485760000)
	{
		echo "error file size > 1 MB";
		unlink($_FILES['file']['tmp_name']);
		exit;
	}

	if (move_uploaded_file($_FILES['file']['tmp_name'], $file)) { 
	if($_POST){
		$args = array();
		$args["table"] = "news_upload";
		$args["news_id"] = $_POST["news_id"];
		$args["url"] = $uploaddir;
		$args["name"] = $name_photo;
		$args["active"] = "T";
		$args["recby_id"] = (int)$EMPID;
		$args["rectime"] = date("Y-m-d H:i:s");		
	    $db->set($args);
	}
	    //echo "success"; 
	} else {
		//echo "error ".$_FILES['file']['error']." --- ".$_FILES['file']['tmp_name']." %%% ".$file."($size)";
	}  
}else{
	$news_upload_id = $_POST["news_upload_id"];
	$q = "select a.name, a.url, a.news_upload_id from news_upload a where a.news_upload_id=".$news_upload_id;
	$r = $db->rows($q);
	if($r){		
		$q = "delete from news_upload where  news_upload_id=".$r["news_upload_id"];
		$db->query($q);
		unlink($r["url"].$r["name"]);
	}
}
?>