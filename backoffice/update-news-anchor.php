<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include('share/class.upload.php');
global $db;
if($_POST){
	$args = array();
	$args["table"] = "news_anchor";
	if($_POST["news_anchor_id"])
	   $args["id"] = $_POST["news_anchor_id"];
	$args["code"] = $_POST["code"];
	$args["name"] = $_POST["name"];
	$args["link_to"] = $_POST["link_to"];
	$args["detail"] = $_POST["detail"];
	$args["highlight"] = $_POST["highlight"];
	$args["active"] = $_POST["active"];
	$args["recby_id"] = (int)$EMPID;
	$args["rectime"] = date("Y-m-d H:i:s");
	
   $ret = $db->set($args);
   $news_anchor_id = $args["id"] ? $args["id"] : $ret;

}
$args = array();
$args["p"] = "news-anchor";
$args["news_anchor_id"] = $news_anchor_id;
$args["type"] = "info";
redirect_url($args);
?>