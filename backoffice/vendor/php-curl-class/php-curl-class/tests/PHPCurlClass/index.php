<?php
include_once "./connection/connection.php";
require "./vendor/autoload.php";
global $db;

use \Curl\Curl;

function d($data=""){
	echo "<pre>";
	print_r($data);
	echo "</pre>";
	// die();
}//end func
// curl --request GET "https://httpbin.org/get?key=value"

$curl = new Curl();
// var_dump($curl);die();


$curl->get('http://localhost/ati/test-api/api-ctrl.php', array(
    'key' => 'value',
));

if ($curl->error) {
    echo 'Error: ' . $curl->errorCode . ': ' . $curl->errorMessage . "\n";
} else {
    echo 'Response:' . "\n";
    var_dump($curl->response);
}

$curl->close();
