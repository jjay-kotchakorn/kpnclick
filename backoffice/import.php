<?php 
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
?>
<meta charset="UTF-8">
<?php 
include ('share/datatype.php');
global $db;
require_once dirname(__FILE__) . '/PHPExcel.php';
$preview = false;
$args = array();
$args[] = "code";
$args[] = "name";
$args[] = "itemtype_id";
$args[] = "remark";
$args[] = "address";
   
$data = array();
$headBar = "";
$fileName = "import.xls";
error_reporting(E_ALL);
ini_set('display_errors','off');

$objPHPExcel = PHPExcel_IOFactory::load($fileName);
					foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
						$worksheetTitle     = $worksheet->getTitle();
				      $highestRow         = $worksheet->getHighestRow(); // e.g. 10
				      $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
				      $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
				      $nrColumns = ord($highestColumn) - 64;
				      //echo"<div class='box-header well' data-original-title>
				      //<h2><i class='icon-edit'></i>&nbsp;";
				      $headBar .= "นำเข้าไฟล์ ".$worksheetTitle." ";
				      $headBar .= $nrColumns . ' คอลัมน์ (A-' . $highestColumn . ') ';
				      $headBar .= ' จำนวน ' . ($highestRow-1) . ' เร็คคอร์ด.';
				     // echo "</h2>
				      //</div>";
				       //echo '<table class="table table-striped">';
				      for ($row = 2; $row <= $highestRow; ++ $row) {
				          //echo '<tr>';
				  		$t = array();
				      	for ($col = 0; $col < $highestColumnIndex; ++ $col) {
				      		$cell = $worksheet->getCellByColumnAndRow($col, $row);
				      		$val = $cell->getValue();

				      		$dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
				            // echo '<td>' .$val.'</td>';
				      		$t[$args[$col]] = $val;
				      	}
				      	$data[] = $t;
				         //echo '</tr>';
				      }
				       //echo '</table>';
				  }				

				  foreach ($data as $key => $field) {
				  		if(trim($field["name"])=="") continue;	  		
				  		$itemtype_id = get_datatype_import($field["itemtype_id"], "itemtype", true);
				  		$room_id = get_datatype_import($field["address"], "room", true);
						$args = array('table' => 'item',
								  'code' => $field["code"],
								  'name' => $field["name"],
								  'itemtype_id' => $itemtype_id,
								  'remark' => $field["remark"],
								  'address' => $field["address"],
								  'room_id' => $room_id,
								  'recby_id' => 1
						);
						//print_r($args);
						//echo "<hr>";
						$db->set($args);
				  }
?>