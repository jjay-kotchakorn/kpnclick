<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db, $RIGHTTYPEID;

?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		
		<div class="col-md-12">  		
			<img src="images/logo-fa.png" alt="">
		</div>
	
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
					<div class="page-head">
						<?php if($RIGHTTYPEID==1 || $RIGHTTYPEID==4)
						{ ?>
							<button class="btn btn-success btn-small pull-right" onclick="addnew()" style="margin-top:10px;"><i class="fa fa-plus"></i> เพิ่มบริษัทสมาชิก</button>
						<?php } ?>
						<h3><i class="fa fa-users"></i> &nbsp;บริษัทสมาชิกชมรมวาณิชธนกิจ</h3>
					</div>
					<table id="tbcompany" class="table" style="width:100%">
						  <thead>
							  <tr>
								  <th width="5%">ลำดับ</th>
								  <th width="50">ชื่อบริษัท(ไทย)</th>
								  <th width="30%">เว็บไซต์</th>
								  <th width="15%">Manage</th>
							  </tr>
						  </thead>   
						<tbody>
						</tbody>
					</table>
					<br><br>
				</div>
			</div>

		</div>
	</div> 
</div>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
	$(document).ready(function() {
		var oTable;
		$("#frmMain").validate();
		listItem();
	});

	function listItem(){
	   var url = "data/companylist.php";
	   oTable = $("#tbcompany").dataTable({
		   "sDom": 'T<"clear">lfrtip',
		   "oLanguage": {
	   	   "sInfocompanyty": "",
	   		"sInfoFiltered": ""
							  },
			"oTableTools": {
				"aButtons":  ""
			},
			"bProcessing": true,
			"bServerSide": true,
			"sAjaxSource": url,
			"sPaginationType": "full_numbers",
			"aaSorting": [[ 0, "desc" ]],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				aoData.push({"name":"menu_id","value":$("#srchMenuId").val()});
				$.ajax( {
					"dataType": 'json', 
					"type": "POST", 
					"url": sSource, 
					"data": aoData, 
					"success": fnCallback
				});
			}
	   }); 
	}

	function editInfo(id){
		if(typeof id=="undefined") return;
	   var url = "index.php?p=<?php echo $_GET["p"];?>&company_id="+id+"&type=info";
	   redirect(url);
	}
	function viewInfo(id){
		if(typeof id=="undefined") return;
	   var url = "index.php?p=<?php echo $_GET["p"];?>&company_id="+id+"&type=view";
	   redirect(url);
	}
	function addnew(){
	   var url = "index.php?p=<?php echo $_GET["p"];?>&type=info";
	   redirect(url);
	}
	function closeInfo(id){
	   	var okToRefresh = confirm("ยืนยันการลบ");
		if (okToRefresh){
			
		   $.ajax({
		        url: 'update-company.php',
		        data: {
		            type: 'del',
		            company_id: id,
		            //person_id: id,
		        },
		        type: 'POST',
		        dataType: 'text/html',
		        success: function(data){
		            // console.log(data);
		            location.reload();
		        }
		    });
			setTimeout("location.reload(true);",500);
		}//end if
	}

	function reCall(){
		oTable.fnClearTable( 0 );
		oTable.fnDraw();
	}

</script>